echo Starte KonMaFS Model Checker
@echo off
set serverIP="127.0.0.1"
set serverPort=80
set resources=%cd%

start java -jar konMaFS.jar -ip %serverIP% -port %serverPort% -resources %resources%