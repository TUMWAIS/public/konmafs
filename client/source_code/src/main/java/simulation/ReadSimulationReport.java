package simulation;

import java.io.BufferedReader;
import java.io.FileReader;

import entities.ModelFile;
import rdf.Unit;

public class ReadSimulationReport {
	private ModelFile reportFile;
	private SimReportList simReportList;
	
	public ReadSimulationReport(ModelFile reportFile) {
		this.reportFile = reportFile;
		simReportList = new SimReportList();
		simReportList.setFileName(reportFile.getFile().getPath());
	}
	
	public SimReportList getSimReportList() {
		return simReportList;
	}
	
	public void readSimulationReportFile() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(reportFile.getFile()));;
			String currentLine = reader.readLine();
			while(currentLine.contains("Simulation")) { 
				currentLine = reader.readLine();
			}
			String entityType ="";
			while(currentLine!= null) {
				currentLine = reader.readLine();
				if(currentLine == null) {
					break;
				}
				
				if(currentLine.contains("***")) {
					entityType = currentLine.substring(4, currentLine.length()-4);
					currentLine = reader.readLine();
					currentLine = reader.readLine();
				}
				
				SimReportEntity simReportEntity = new SimReportEntity();
				if(simReportList.getSimReportEntities().get(currentLine.split("\\s+")[0])!=null) {
					simReportEntity = simReportList.getSimReportEntities().get(currentLine.split("\\s+")[0]);
				}
				simReportEntity.setSimulationItemType(entityType);
				simReportEntity.setSimulationItemName(currentLine.split("\\s+")[0]);
				simReportEntity.setUnit(Unit.valueOf(currentLine.split("\\s+")[3]));
				while(currentLine!=null && !currentLine.isEmpty()) {
					String value = currentLine.split("\\s+")[2];
					if(currentLine.contains("StateTimes[Breakdown]")) {
						simReportEntity.setTime_breakdown(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Idle]")) {
						simReportEntity.setTime_idle(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Maintenance]")) {
						simReportEntity.setTime_maintenance(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Stopped]")) {
						simReportEntity.setTime_stopped(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Working]")) {
						simReportEntity.setTime_working(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Downtime]")) {
						simReportEntity.setTime_downtime(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Closed]")) {
						simReportEntity.setTime_closed(Double.valueOf(value));
					}
					else if(currentLine.contains("StateTimes[Open]")) {
						simReportEntity.setTime_open(Double.valueOf(value));
					}
					else if(currentLine.contains("Value")) {
						String oldName = simReportEntity.getSimulationItemName();
						String newName = oldName.split("_")[0];
						
						if(simReportList.getSimReportEntities().get(newName)!=null) {
							simReportEntity = simReportList.getSimReportEntities().get(newName);
						}
						
						String subElementPropertyCustomName = oldName.split("_")[1];
						Double subElementPropertyCustomValue = Double.valueOf(value);
						SimulationReportCustomItem simulationReportCustomItem = new SimulationReportCustomItem(subElementPropertyCustomName,subElementPropertyCustomValue);
						simReportEntity.setSimulationItemName(newName);
						simReportEntity.addSimulationReportCustomItem(simulationReportCustomItem);
					}
					else if(currentLine.contains("TotalTime")) {
						simReportEntity.setTotalTime(Double.valueOf(value));
					}
					else if(currentLine.contains("Utilisation")) {
						simReportEntity.setUtilisation(Double.valueOf(value));
					}
					else if(currentLine.contains("Commitment")) {
						simReportEntity.setCommitment(Double.valueOf(value));
					}
					else if(currentLine.contains("Availability")) {
						simReportEntity.setAvailability(Double.valueOf(value));
					}
					else if(currentLine.contains("Reliability")) {
						simReportEntity.setReliability(Double.valueOf(value));
					}
					else if(currentLine.contains("NumberAdded")) {
						simReportEntity.setNumberAdded(Double.valueOf(value).longValue());
					}
					else if(currentLine.contains("NumberProcessed")) {
						simReportEntity.setNumberProcessed(Double.valueOf(value).longValue());
					}
					else if(currentLine.contains("StateTimes[None]")) {
						simReportEntity.setTime_none(Double.valueOf(value));
					}
					else if(currentLine.contains("QueueLengthMaximum")) {
						simReportEntity.setQueueLengthMaximum(Double.valueOf(value).longValue());
					}
					currentLine = reader.readLine();
				}
				simReportList.addSimReportEntity(simReportEntity.getSimulationItemName(), simReportEntity);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
}
