package simulation;

import java.util.List;
import java.util.Map.Entry;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import rdf.RDFBaseModel;

public class SimulationElementLoggerRDFWriter {

	public static final String BASE_URI = "http://example.org";
	public static final String SIM_NS = BASE_URI + "/"+ "simulation";	
	public static final String SIM_LOG_PROP_NS = SIM_NS+"/"+"simulationLog#";
	public static final String SIM_LOG_COLLECTION_NS = SIM_NS+"/"+"simulationLog";
	public static final String SIM_LOG_PROPERTY_URI_NAME = SIM_LOG_PROP_NS+ "name";	
	public static final String SIM_LOG_PROPERTY_LOG_COUNT_ITEM = SIM_LOG_PROP_NS+ "logCountItem";
	
	public static final String SIM_LOG_COUNT_ITEM_PROP_NS = SIM_NS+"/"+"simulationLogCountItem#";
	public static final String SIM_LOG_COUNT_ITEM_COLLECTION_NS = SIM_NS+"/"+"simulationLogCountItem";
	public static final String SIM_LOG_COUNT_ITEM_PROPERTY_SIM_ELEMENT_NAME =SIM_LOG_COUNT_ITEM_PROP_NS+"simElementName";
	public static final String SIM_LOG_COUNT_ITEM_PROPERTY_SIM_ELEMENT_COUNT =SIM_LOG_COUNT_ITEM_PROP_NS+"simElementCount";
	
	Property simLogPropertyName;
	Property simLogPropertyLogCountItem;
	Property simLogCountItemPropertySimElementName;
	Property simLogCountItemPropertySimElementCount;
	
	public Model model;
	
	private static int simulationLogCounter=1;
	private static int simulationLogCountItemCounter=1;
	
	public static void resetCounter() {
		simulationLogCounter=1;
		simulationLogCountItemCounter=1;
	}
	
	public SimulationElementLoggerRDFWriter(RDFBaseModel baseModel) {
		this.model = baseModel.getModel();
		init();
	}
	
	private void init() {
		model.setNsPrefix( "simLogItem", SIM_LOG_PROP_NS );
		model.setNsPrefix( "simLogCountItem", SIM_LOG_COUNT_ITEM_PROP_NS );
		
		simLogPropertyName = model.createProperty( SIM_LOG_PROPERTY_URI_NAME );
		simLogPropertyLogCountItem = model.createProperty( SIM_LOG_PROPERTY_LOG_COUNT_ITEM );
		
		simLogCountItemPropertySimElementName = model.createProperty(SIM_LOG_COUNT_ITEM_PROPERTY_SIM_ELEMENT_NAME);
		simLogCountItemPropertySimElementCount = model.createProperty(SIM_LOG_COUNT_ITEM_PROPERTY_SIM_ELEMENT_COUNT);
	}
	
	public void writeSimLogItemsToRDFModel(List<ReadSimElementLogger> readedLogElements) {
		for(ReadSimElementLogger readLog : readedLogElements) {
			Resource simLogSubject = model.createResource(SIM_LOG_COLLECTION_NS+"/"+"simulationLogItem_"+simulationLogCounter++);
			model.add(simLogSubject,simLogPropertyName,readLog.getLoggerName());
			for(Entry<String,Integer> countEntry : readLog.getEntityCountSet().entrySet()) {
				Resource simLogCountItemSubject = model.createResource(SIM_LOG_COUNT_ITEM_COLLECTION_NS+"/"+"simulationLogCountItem_"+simulationLogCountItemCounter++);
				model.add(simLogCountItemSubject,simLogCountItemPropertySimElementName,countEntry.getKey());
				Long countLong = Long.valueOf(countEntry.getValue().longValue());
				model.add(simLogCountItemSubject,simLogCountItemPropertySimElementCount,model.createTypedLiteral(countLong));
				model.add(simLogSubject,simLogPropertyLogCountItem,simLogCountItemSubject);
			}
		}
	}
	
}
