package simulation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import entities.EntityFileList;


public class SimReportList extends EntityFileList<SimReportEntity>{

	private Map<String, SimReportEntity> simReportEntities = new LinkedHashMap<>();
	
	public List<SimReportEntity> getShowableEntityList() {
		return new ArrayList<SimReportEntity>(simReportEntities.values());
	}
	
	public void addSimReportEntity(String name, SimReportEntity entity ) {
		simReportEntities.put(name, entity);
	}

	public Map<String, SimReportEntity> getSimReportEntities() {
		return simReportEntities;
	}

	public void setSimReportEntities(Map<String, SimReportEntity> simReportEntities) {
		this.simReportEntities = simReportEntities;
	}

}
