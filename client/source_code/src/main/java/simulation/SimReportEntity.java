package simulation;

import java.util.ArrayList;

import rdf.Unit;


public class SimReportEntity {

	private Double time_breakdown;
	private Double time_none;
	private Double time_idle;
	private Double time_maintenance;
	private Double time_stopped;
	private Double time_working;
	private Double time_downtime;
	private Double time_closed;
	private Double time_open;
	private Double totalTime;
	private Double utilisation;
	private Double commitment;
	private Double availability;
	private Double reliability;
	private Long numberAdded;
	private Long numberProcessed;
	private Unit unit;
	private String simulationItemType;
	private String simulationItemName;
	private ArrayList<SimulationReportCustomItem> simulationReportCustomItems = new ArrayList<>();
	private Long queueLengthMaximum;

	
	public Double getTime_breakdown() {
		return time_breakdown;
	}
	public void setTime_breakdown(Double time_breakdown) {
		this.time_breakdown = time_breakdown;
	}
	public Double getTime_idle() {
		return time_idle;
	}
	public void setTime_idle(Double time_idle) {
		this.time_idle = time_idle;
	}
	public Double getTime_maintenance() {
		return time_maintenance;
	}
	public void setTime_maintenance(Double time_maintenance) {
		this.time_maintenance = time_maintenance;
	}
	public Double getTime_stopped() {
		return time_stopped;
	}
	public void setTime_stopped(Double time_stopped) {
		this.time_stopped = time_stopped;
	}
	public Double getTime_working() {
		return time_working;
	}
	public void setTime_working(Double time_working) {
		this.time_working = time_working;
	}
	public Double getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(Double totalTime) {
		this.totalTime = totalTime;
	}
	public Double getUtilisation() {
		return utilisation;
	}
	public void setUtilisation(Double utilisation) {
		this.utilisation = utilisation;
	}
	public Double getCommitment() {
		return commitment;
	}
	public void setCommitment(Double commitment) {
		this.commitment = commitment;
	}
	public Double getAvailability() {
		return availability;
	}
	public void setAvailability(Double availability) {
		this.availability = availability;
	}
	public Double getReliability() {
		return reliability;
	}
	public void setReliability(Double reliability) {
		this.reliability = reliability;
	}
	public Long getNumberAdded() {
		return numberAdded;
	}
	public void setNumberAdded(Long numberAdded) {
		this.numberAdded = numberAdded;
	}
	public Long getNumberProcessed() {
		return numberProcessed;
	}
	public void setNumberProcessed(Long numberProcessed) {
		this.numberProcessed = numberProcessed;
	}
	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	public String getSimulationItemType() {
		return simulationItemType;
	}
	public void setSimulationItemType(String simulationItemType) {
		this.simulationItemType = simulationItemType;
	}
	public String getSimulationItemName() {
		return simulationItemName;
	}
	public void setSimulationItemName(String simulationItemName) {
		this.simulationItemName = simulationItemName;
	}
	public Double getTime_none() {
		return time_none;
	}
	public void setTime_none(Double time_none) {
		this.time_none = time_none;
	}
	public Double getTime_downtime() {
		return time_downtime;
	}
	public void setTime_downtime(Double time_downtime) {
		this.time_downtime = time_downtime;
	}
	public Double getTime_closed() {
		return time_closed;
	}
	public void setTime_closed(Double time_closed) {
		this.time_closed = time_closed;
	}
	public Double getTime_open() {
		return time_open;
	}
	public void setTime_open(Double time_open) {
		this.time_open = time_open;
	}
	
	public void addSimulationReportCustomItem(SimulationReportCustomItem simulationReportCustomItem) {
		this.simulationReportCustomItems.add(simulationReportCustomItem);
	}
	public ArrayList<SimulationReportCustomItem> getSimulationReportCustomItems() {
		return this.simulationReportCustomItems;
	}
	public void setSimulationReportCustomItems(ArrayList<SimulationReportCustomItem> simulationReportCustomItems) {
		this.simulationReportCustomItems = simulationReportCustomItems;
	}
	public Long getQueueLengthMaximum() {
		return queueLengthMaximum;
	}
	public void setQueueLengthMaximum(Long queueLengthMaximum) {
		this.queueLengthMaximum = queueLengthMaximum;
	}
	
}
