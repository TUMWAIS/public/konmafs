package simulation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import entities.ModelFile;

public class ReadSimElementLogger {

	private ModelFile logFile;
	private ModelFile jaamSimModelFile;
	private HashMap<String,Integer> entityCountSet = new HashMap<>();
	private String loggerName;
	
	public ReadSimElementLogger(ModelFile logFile, ModelFile jaamSimModelFile) {
		this.logFile = logFile;
		this.jaamSimModelFile = jaamSimModelFile;
		loggerName = getLoggerNameFromFileName(logFile.toString());
	}
	
	private String getLoggerNameFromFileName(String fileName) {
		fileName = fileName.split("\\.")[0];
		fileName = fileName.split("-")[1];
		return fileName;
	}
	
	public void readLoggerFile() {
		 BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(logFile.getFile()));
				String currentLine = reader.readLine();
			     while(currentLine!= null) {
			    	 if(currentLine.startsWith("this")) {
			    		 currentLine = reader.readLine();
			    		 break;
			    	 }
			    	 currentLine = reader.readLine();
			     }
			     
			     while(currentLine!= null) {
			    	 String entityGeneratorName = currentLine.split("\t")[1];
			    	 entityGeneratorName = entityGeneratorName.split("_")[0];
			    	 if(entityCountSet.containsKey(entityGeneratorName)) {
			    		 int currentValue = entityCountSet.get(entityGeneratorName).intValue();
			    		 currentValue++;
			    		 entityCountSet.put(entityGeneratorName, Integer.valueOf(currentValue));
			    	 }
			    	 else {
			    		 entityCountSet.put(entityGeneratorName, Integer.valueOf(1));
			    	 }
			    	 currentLine = reader.readLine();
			     }
			     
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			readEntityNameForEntityGenerator();
	}
	
	private void readEntityNameForEntityGenerator() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(jaamSimModelFile.getFile()));
			String currentLine = reader.readLine();
		     while(currentLine!= null) {
		    	 if(currentLine.contains("PrototypeEntity")) {
		    		 String entityGeneratorName = currentLine.split(" ")[0];
		    		 String entityName = currentLine.split(" ")[3];
		    		 if(entityCountSet.containsKey(entityGeneratorName)) {
			    		 int count = entityCountSet.get(entityGeneratorName);
			    		 entityCountSet.remove(entityGeneratorName);
			    		 entityCountSet.put(entityName, Integer.valueOf(count));
		    		 }
		    	 }
		    	 currentLine = reader.readLine();
		     }
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HashMap<String,Integer> getEntityCountSet() {
		return entityCountSet;
	}

	public String getLoggerName() {
		return loggerName;
	}

}
