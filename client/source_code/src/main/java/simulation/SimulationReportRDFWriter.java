package simulation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import jaamSim.SimulationItemTyp;
import rdf.RDFBaseModel;

public class SimulationReportRDFWriter {
	public static final String BASE_URI = "http://example.org";
	public static final String SIM_NS = BASE_URI + "/"+ "simulation";	
	public static final String SIM_REPORT_ITEM_PROP_NS = SIM_NS+"/"+"simulationReport#";
	public static final String SIM_REPORT_ITEM_COLLECTION_NS = SIM_NS+"/"+"simulationReport";
	public static final String UNIT_NS = "http://example.org/unit";
	public static final String SIM_REPORT_ITEM_PROPERTY_URI_NAME = SIM_REPORT_ITEM_PROP_NS+ "name";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_BREAKDOWN = SIM_REPORT_ITEM_PROP_NS+ "timeBreakdown";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_IDLE = SIM_REPORT_ITEM_PROP_NS+ "timeIdle";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_MAINTENANCE = SIM_REPORT_ITEM_PROP_NS+ "timeMaintenance";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_STOPPED = SIM_REPORT_ITEM_PROP_NS+ "timeStopped";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_WORKING = SIM_REPORT_ITEM_PROP_NS+ "timeWorking";
	public static final String SIM_REPORT_ITEM_PROPERTY_TOTAL_TIME = SIM_REPORT_ITEM_PROP_NS+ "totalTime";
	public static final String SIM_REPORT_ITEM_PROPERTY_UTILISATION = SIM_REPORT_ITEM_PROP_NS+ "utilisation";
	public static final String SIM_REPORT_ITEM_PROPERTY_COMMITMENT = SIM_REPORT_ITEM_PROP_NS+ "commitment";
	public static final String SIM_REPORT_ITEM_PROPERTY_AVAILABILITY = SIM_REPORT_ITEM_PROP_NS+ "availability";
	public static final String SIM_REPORT_ITEM_PROPERTY_RELIABILITY = SIM_REPORT_ITEM_PROP_NS+ "reliability";
	public static final String SIM_REPORT_ITEM_PROPERTY_NUMBER_ADDED = SIM_REPORT_ITEM_PROP_NS+ "numberAdded";
	public static final String SIM_REPORT_ITEM_PROPERTY_NUMBER_PROCESSED = SIM_REPORT_ITEM_PROP_NS+ "numberProcessed";
	public static final String SIM_REPORT_ITEM_PROPERTY_QUEUE_LENGTH_MAXIMUM = SIM_REPORT_ITEM_PROP_NS+ "queueLengthMaximum";
	public static final String SIM_REPORT_ITEM_SIMULATION_ITME_TYP = SIM_REPORT_ITEM_PROP_NS+ "typ";
	public static final String SIM_REPORT_ITEM_UNIT = SIM_REPORT_ITEM_PROP_NS+ "unit";
	public static final String SIM_ITEM_TYPE_COLLECTION_NS = SIM_NS+"/"+"simulationItemTyp";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_DOWNTIME = SIM_REPORT_ITEM_PROP_NS+ "timeDown";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_CLOSED = SIM_REPORT_ITEM_PROP_NS+ "timeClosed";
	public static final String SIM_REPORT_ITEM_PROPERTY_TIME_OPEN = SIM_REPORT_ITEM_PROP_NS+ "timeOpen";
	public static final String SIM_REPORT_ITEM_PROPERTY_SIMULATION_REPORT_CUSTOM_ITEM = SIM_REPORT_ITEM_PROP_NS+ "customProperty";
	public static final String SIM_REPORT_CUSTOM_ITEM_PROP_NS = SIM_NS+"/"+"simulationReportCustomItem#";
	public static final String SIM_REPORT_CUSTOM_ITEM_COLLECTION_NS = SIM_NS+"/"+"simulationReportCustomItem";
	public static final String SIM_REPORT_CUSTOM_ITEM_PROPERTY_NAME =SIM_REPORT_CUSTOM_ITEM_PROP_NS+"name";
	public static final String SIM_REPORT_CUSTOM_ITEM_PROPERTY_VALUE =SIM_REPORT_CUSTOM_ITEM_PROP_NS+"value";
	
	public static final String SIM_ITEM_TYPE_PREFIX = "simItemType_";
	
	private Map<String,Resource> resourceMap = new HashMap<>();
	
	Property simReportItemPropertyName;
	Property simReportItemPropertyTimeBreakdown;
	Property simReportItemPropertyTimeIdle;
	Property simReportItemPropertyTimeMaintenance;
	Property simReportItemPropertyTimeStopped;
	Property simReportItemPropertyTimeWorking;
	Property simReportItemPropertyTotalTime;
	Property simReportItemPropertyUtilisation;
	Property simReportItemPropertyCommitment;
	Property simReportItemPropertyAvailability;
	Property simReportItemPropertyReliability;
	Property simReportItemPropertyNumberAdded;
	Property simReportItemPropertyNumberProcessed;
	Property simReportItemPropertyQueueLengthMaximum;
	Property simReportItemPropertySimulationItemType;
	Property simReportItemPropertyUnit;
	Property simReportItemPropertyTimeDown;
	Property simReportItemPropertyTimeClosed;
	Property simReportItemPropertyTimeOpen;	
	Property simReportItemPropertySimulationReportCustomItem;
	Property simReportCustomItemPropertyName;
	Property simReportCustomItemPropertyValue;
	
	public Model model;
	private RDFBaseModel baseModel;
	
	public static void resetCounter() {
		simulationReportCounter=1;
		simulationReportCustomCounter = 1;
	}
	
	private static int simulationReportCounter=1;
	public static int simulationReportCustomCounter=1;
	
	public SimulationReportRDFWriter(RDFBaseModel baseModel) {
		this.baseModel = baseModel;
		this.model = baseModel.getModel();
		init();
	}
	
	private void init() {
		model.setNsPrefix( "simReportItem", SIM_REPORT_ITEM_PROP_NS );
		model.setNsPrefix( "simReportCustomItem", SIM_REPORT_CUSTOM_ITEM_PROP_NS );
		
		simReportItemPropertyName = model.createProperty( SIM_REPORT_ITEM_PROPERTY_URI_NAME );
		simReportItemPropertyTimeBreakdown = model.createProperty( SIM_REPORT_ITEM_PROPERTY_TIME_BREAKDOWN );
		simReportItemPropertyTimeIdle= model.createProperty( SIM_REPORT_ITEM_PROPERTY_TIME_IDLE );
		simReportItemPropertyTimeMaintenance= model.createProperty( SIM_REPORT_ITEM_PROPERTY_TIME_MAINTENANCE );
		simReportItemPropertyTimeStopped= model.createProperty( SIM_REPORT_ITEM_PROPERTY_TIME_STOPPED );
		simReportItemPropertyTimeWorking= model.createProperty( SIM_REPORT_ITEM_PROPERTY_TIME_WORKING );
		simReportItemPropertyTotalTime= model.createProperty( SIM_REPORT_ITEM_PROPERTY_TOTAL_TIME );
		simReportItemPropertyUtilisation= model.createProperty( SIM_REPORT_ITEM_PROPERTY_UTILISATION );
		simReportItemPropertyCommitment= model.createProperty( SIM_REPORT_ITEM_PROPERTY_COMMITMENT );
		simReportItemPropertyAvailability= model.createProperty( SIM_REPORT_ITEM_PROPERTY_AVAILABILITY );
		simReportItemPropertyReliability= model.createProperty( SIM_REPORT_ITEM_PROPERTY_RELIABILITY );
		simReportItemPropertyNumberAdded= model.createProperty( SIM_REPORT_ITEM_PROPERTY_NUMBER_ADDED );
		simReportItemPropertyNumberProcessed= model.createProperty( SIM_REPORT_ITEM_PROPERTY_NUMBER_PROCESSED );
		simReportItemPropertyQueueLengthMaximum= model.createProperty( SIM_REPORT_ITEM_PROPERTY_QUEUE_LENGTH_MAXIMUM );
		simReportItemPropertySimulationItemType= model.createProperty( SIM_REPORT_ITEM_SIMULATION_ITME_TYP );
		simReportItemPropertyUnit = model.createProperty(SIM_REPORT_ITEM_UNIT);
		simReportItemPropertyTimeDown = model.createProperty(SIM_REPORT_ITEM_PROPERTY_TIME_DOWNTIME);
		simReportItemPropertyTimeClosed = model.createProperty(SIM_REPORT_ITEM_PROPERTY_TIME_CLOSED);
		simReportItemPropertyTimeOpen = model.createProperty(SIM_REPORT_ITEM_PROPERTY_TIME_OPEN);
		simReportItemPropertySimulationReportCustomItem = model.createProperty(SIM_REPORT_ITEM_PROPERTY_SIMULATION_REPORT_CUSTOM_ITEM);
		simReportCustomItemPropertyName = model.createProperty(SIM_REPORT_CUSTOM_ITEM_PROPERTY_NAME);
		simReportCustomItemPropertyValue = model.createProperty(SIM_REPORT_CUSTOM_ITEM_PROPERTY_VALUE);

		for(SimulationItemTyp type : SimulationItemTyp.values()) {
			Resource nodeTypeResource = model.createResource(SIM_ITEM_TYPE_COLLECTION_NS+"/"+type.toString());
			resourceMap.put(SIM_ITEM_TYPE_PREFIX+type.toString(), nodeTypeResource);
		}
	}
	
	public void writeSimReportItemsToRDFModel(List<SimReportEntity> simReportItems, File file) {
		for(SimReportEntity simReportItem :simReportItems) {
			Resource simReportItemSubject = model.createResource(SIM_REPORT_ITEM_COLLECTION_NS+"/"+"simulationReportItem_"+simulationReportCounter++);
			resourceMap.put(simReportItem.getSimulationItemName(), simReportItemSubject);
		}
		for(SimReportEntity simReportItem :simReportItems) {
			Resource simItemSubject = resourceMap.get(simReportItem.getSimulationItemName());
			model.add(simItemSubject,simReportItemPropertyName,simReportItem.getSimulationItemName());
			Resource simTypeResource = resourceMap.get(SIM_ITEM_TYPE_PREFIX+simReportItem.getSimulationItemType());
			if(simTypeResource!=null) {
				model.add(simItemSubject,simReportItemPropertySimulationItemType,simTypeResource);
			}
			switch(simReportItem.getUnit()) {
				case ms:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_ms);break;
				case s:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_s);break;
				case h:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_h);break;
				case min:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_min);break;
				case d:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_d);break;
				case w:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_w);break;
				case y:model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_y);break;
				default:
					model.add(simItemSubject,simReportItemPropertyUnit,baseModel.unit_h);
			}
			if(simReportItem.getTime_breakdown()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeBreakdown,model.createTypedLiteral(simReportItem.getTime_breakdown()));
			if(simReportItem.getTime_idle()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeIdle,model.createTypedLiteral(simReportItem.getTime_idle()));
			if(simReportItem.getTime_maintenance()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeMaintenance,model.createTypedLiteral(simReportItem.getTime_maintenance()));
			if(simReportItem.getTime_stopped()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeStopped,model.createTypedLiteral(simReportItem.getTime_stopped()));
			if(simReportItem.getTime_working()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeWorking,model.createTypedLiteral(simReportItem.getTime_working()));
			if(simReportItem.getTotalTime()!=null)
				model.add(simItemSubject,simReportItemPropertyTotalTime,model.createTypedLiteral(simReportItem.getTotalTime()));
			if(simReportItem.getUtilisation()!=null)
				model.add(simItemSubject,simReportItemPropertyUtilisation,model.createTypedLiteral(simReportItem.getUtilisation()));
			if(simReportItem.getCommitment()!=null)
				model.add(simItemSubject,simReportItemPropertyCommitment,model.createTypedLiteral(simReportItem.getCommitment()));
			if(simReportItem.getAvailability()!=null)
				model.add(simItemSubject,simReportItemPropertyAvailability,model.createTypedLiteral(simReportItem.getAvailability()));
			if(simReportItem.getReliability()!=null)
				model.add(simItemSubject,simReportItemPropertyReliability,model.createTypedLiteral(simReportItem.getReliability()));
			if(simReportItem.getNumberAdded()!=null)
				model.add(simItemSubject,simReportItemPropertyNumberAdded,model.createTypedLiteral(simReportItem.getNumberAdded()));
			if(simReportItem.getNumberProcessed()!=null)
				model.add(simItemSubject,simReportItemPropertyNumberProcessed,model.createTypedLiteral(simReportItem.getNumberProcessed()));
			if(simReportItem.getQueueLengthMaximum()!=null)
				model.add(simItemSubject,simReportItemPropertyQueueLengthMaximum,model.createTypedLiteral(simReportItem.getQueueLengthMaximum()));
			if(simReportItem.getTime_downtime()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeDown,model.createTypedLiteral(simReportItem.getTime_downtime()));
			if(simReportItem.getTime_closed()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeClosed,model.createTypedLiteral(simReportItem.getTime_closed()));
			if(simReportItem.getTime_open()!=null)
				model.add(simItemSubject,simReportItemPropertyTimeOpen,model.createTypedLiteral(simReportItem.getTime_open()));
			if(!simReportItem.getSimulationReportCustomItems().isEmpty()) {
				for(SimulationReportCustomItem customItem : simReportItem.getSimulationReportCustomItems()) {
					Resource simReportCustomItemSubject = model.createResource(SIM_REPORT_CUSTOM_ITEM_COLLECTION_NS+"/"+"simulationReportCustomItem_"+simulationReportCustomCounter++);
					model.add(simReportCustomItemSubject,simReportCustomItemPropertyName,customItem.getName());
					model.add(simReportCustomItemSubject,simReportCustomItemPropertyValue,model.createTypedLiteral(customItem.getValue()));
					model.add(simItemSubject,simReportItemPropertySimulationReportCustomItem,simReportCustomItemSubject);
				}
			}
		}
		if(file!=null) {
			try {
				FileOutputStream fop = new FileOutputStream(file);
				RDFDataMgr.write(fop, model, Lang.RDFXML);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
