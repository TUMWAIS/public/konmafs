package freeCad;

public class FreeCadEntity {
	protected String label;
	protected String objectName;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	protected String getObjectName() {
		return objectName;
	}

	protected void setObjectName(String objectName) {
		this.objectName = objectName;
	}
}