package freeCad;

import java.util.Date;

public class DocumentProperties {
	private Date creationDate;
	private Date lastModifiedDate;

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String toString() {
		return "Created at: " + creationDate + "\nLast modified at: " + lastModifiedDate;
	}
}