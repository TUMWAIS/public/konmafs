package freeCad;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import rdf.RDFBaseModel;

public class CadRDFWriter {
	
	public static final String BASE_URI = "http://example.org";
	public static final String CAD_NS = BASE_URI+"/"+"cad";
	public static final String CAD_ITEM_NS = CAD_NS+"/"+"cadItem#";
	public static final String CAD_ITEM_COLLECTION_NS = CAD_NS+"/"+"cadItem";
	public static final String UNIT_NS = BASE_URI+"/"+"unit";
	public static final String CAD_ITEM_PROPERTY_URI_NAME = CAD_ITEM_NS+ "name";
	public static final String CAD_ITEM_PROPERTY_URI_HIGHT = CAD_ITEM_NS+ "height";
	public static final String CAD_ITEM_PROPERTY_URI_LENGTH = CAD_ITEM_NS+ "length";
	public static final String CAD_ITEM_PROPERTY_URI_WIDTH = CAD_ITEM_NS+ "width";
	public static final String CAD_ITEM_PROPERTY_URI_X_POS = CAD_ITEM_NS+ "x_position";
	public static final String CAD_ITEM_PROPERTY_URI_Y_POS = CAD_ITEM_NS+ "y_position";
	public static final String CAD_ITEM_PROPERTY_URI_Z_POS = CAD_ITEM_NS+ "z_position";
	public static final String CAD_ITEM_PROPERTY_UNIT = CAD_ITEM_NS+ "unit";
	public static final String CAD_GROUP_NS = CAD_NS+"/"+"cadGroup#";
	public static final String CAD_GROUP_COLLECTION_NS = CAD_NS+"/"+"cadGroup";
	public static final String CAD_GROUP_PROPERTY_URI_NAME = CAD_GROUP_NS+ "name";
	public static final String CAD_GROUP_PROPERTY_GROUP_CAD_ITEM = CAD_GROUP_NS+ "groupCadItem";
	public static final String CAD_GROUP_PROPERTY_INNER_GROUP_ITEM = CAD_GROUP_NS+ "groupCadGroup";
	public static final String CAD_GROUP_PROPERTY_GROUP_IS_TOP_LEVEL = CAD_GROUP_NS+ "groupIsTopLevelGroup";
	public static final String CAD_GROUP_PROPERTY_URI_HIGHT = CAD_GROUP_NS+ "height";
	public static final String CAD_GROUP_PROPERTY_URI_LENGTH = CAD_GROUP_NS+ "length";
	public static final String CAD_GROUP_PROPERTY_URI_WIDTH = CAD_GROUP_NS+ "width";
	public static final String CAD_GROUP_PROPERTY_URI_X_POS = CAD_GROUP_NS+ "x_position";
	public static final String CAD_GROUP_PROPERTY_URI_Y_POS = CAD_GROUP_NS+ "y_position";
	public static final String CAD_GROUP_PROPERTY_URI_Z_POS = CAD_GROUP_NS+ "z_position";
	
	
	Property cadItemPropertyName;
	Property cadItemPropertyHeight;
	Property cadItemPropertyLenght;
	Property cadItemPropertyWidth;
	Property cadItemPropertyXpos;
	Property cadItemPropertyYpos;
	Property cadItemPropertyZpos;
	Property cadItemPropertyUnit;
	Property cadGroupPropertyName;
	Property cadGroupPropertyGroupCADItems;
	Property cadGroupPropertyInnerGroupItem;
	Property cadGroupPropertyGroupIsTopLevel;
	Property cadGroupPropertyHeight;
	Property cadGroupPropertyLenght;
	Property cadGroupPropertyWidth;
	Property cadGroupPropertyXpos;
	Property cadGroupPropertyYpos;
	Property cadGroupPropertyZpos;
	
	public Model model;
	private RDFBaseModel baseModel;
	
	private HashMap<String,Resource> cadItemResources = new HashMap<>();
	private HashMap<String,Resource> groupResources = new HashMap<>();
	
	public static void resetCounter() {
		cadItemCounter = 1;
		cadGroupCounter = 1;
	}
	
	private static int cadItemCounter = 1;
	private static int cadGroupCounter = 1;
	
	public CadRDFWriter(RDFBaseModel baseModel) {
		this.baseModel = baseModel;
		this.model = baseModel.getModel();
		initCadRdfModel();
	}
	
	public void initCadRdfModel() {		
		model.setNsPrefix( "cadItem", CAD_ITEM_NS );
		model.setNsPrefix( "cadGroup", CAD_GROUP_NS );

		cadItemPropertyName = model.createProperty( CAD_ITEM_PROPERTY_URI_NAME );
		cadItemPropertyHeight = model.createProperty( CAD_ITEM_PROPERTY_URI_HIGHT );
		cadItemPropertyLenght = model.createProperty( CAD_ITEM_PROPERTY_URI_LENGTH );
		cadItemPropertyWidth = model.createProperty( CAD_ITEM_PROPERTY_URI_WIDTH );
		cadItemPropertyXpos = model.createProperty( CAD_ITEM_PROPERTY_URI_X_POS );
		cadItemPropertyYpos = model.createProperty( CAD_ITEM_PROPERTY_URI_Y_POS );
		cadItemPropertyZpos = model.createProperty( CAD_ITEM_PROPERTY_URI_Z_POS );		
		
		cadGroupPropertyName = model.createProperty( CAD_GROUP_PROPERTY_URI_NAME );
		cadGroupPropertyGroupCADItems = model.createProperty( CAD_GROUP_PROPERTY_GROUP_CAD_ITEM );
		cadGroupPropertyInnerGroupItem = model.createProperty(CAD_GROUP_PROPERTY_INNER_GROUP_ITEM);
		cadGroupPropertyGroupIsTopLevel = model.createProperty(CAD_GROUP_PROPERTY_GROUP_IS_TOP_LEVEL);
		
		cadGroupPropertyHeight = model.createProperty( CAD_GROUP_PROPERTY_URI_HIGHT );
		cadGroupPropertyLenght = model.createProperty( CAD_GROUP_PROPERTY_URI_LENGTH );
		cadGroupPropertyWidth = model.createProperty( CAD_GROUP_PROPERTY_URI_WIDTH );
		cadGroupPropertyXpos = model.createProperty( CAD_GROUP_PROPERTY_URI_X_POS );
		cadGroupPropertyYpos = model.createProperty( CAD_GROUP_PROPERTY_URI_Y_POS );
		cadGroupPropertyZpos = model.createProperty( CAD_GROUP_PROPERTY_URI_Z_POS );
		
		cadItemPropertyUnit = model.createProperty(CAD_ITEM_PROPERTY_UNIT);
	}
	
	private Literal getDoubleTypedLiteral(double value) {
		return model.createTypedLiteral(Double.valueOf(value));
	}
	
	public void writeCADGroupElementsToRdfModel(List<FreeCadGroup> selectedGroups) {
		for(FreeCadGroup group : selectedGroups) {
			Resource cadGroupSubject = model.createResource(CAD_GROUP_COLLECTION_NS+"/"+"cadGroup_"+cadGroupCounter++);
			groupResources.put(group.objectName, cadGroupSubject);
		}
		for(FreeCadGroup group : selectedGroups) {
			Resource groupResource = groupResources.get(group.objectName);
			model.add(groupResource,cadGroupPropertyName,group.getLabel().replace(" ","_"));
			model.add(groupResource,cadGroupPropertyGroupIsTopLevel,Boolean.toString(group.isTopLevelGroup()));
			for(String elementLabelInGroup : group.getGroupedElements()) {
				if(cadItemResources.containsKey(elementLabelInGroup)) {
					model.add(groupResource,cadGroupPropertyGroupCADItems,cadItemResources.get(elementLabelInGroup));
				}
				else {
					model.add(groupResource,cadGroupPropertyInnerGroupItem,groupResources.get(elementLabelInGroup));
				}				
			}
			
			model.add(groupResource,cadGroupPropertyHeight,getDoubleTypedLiteral(group.getHeight()));
			model.add(groupResource,cadGroupPropertyLenght,getDoubleTypedLiteral(group.getLength()));
			model.add(groupResource,cadGroupPropertyWidth,getDoubleTypedLiteral(group.getWidth()));
			if(group.getxPlace()!=null) {
				model.add(groupResource,cadGroupPropertyXpos,getDoubleTypedLiteral(group.getxPlace().doubleValue()));
				model.add(groupResource,cadGroupPropertyYpos,getDoubleTypedLiteral(group.getyPlace().doubleValue()));
				model.add(groupResource,cadGroupPropertyZpos,getDoubleTypedLiteral(group.getzPlace().doubleValue()));
			}
		}		
	}
	
	public void writeCADElementsToRdfModel(List<FreeCadObject> selectedCADElements, File file) {
		for(FreeCadObject cadObject : selectedCADElements) {
			Resource cadSubject = model.createResource(CAD_ITEM_COLLECTION_NS+"/"+"cadItem_"+cadItemCounter++);
			model.add(cadSubject,cadItemPropertyName,cadObject.getLabel().replace(" ","_"));
			
			model.add(cadSubject,cadItemPropertyUnit,baseModel.unit_mm);
			
			model.add(cadSubject,cadItemPropertyHeight,getDoubleTypedLiteral(cadObject.getHeight()));
			model.add(cadSubject,cadItemPropertyLenght,getDoubleTypedLiteral(cadObject.getLength()));
			model.add(cadSubject,cadItemPropertyWidth,getDoubleTypedLiteral(cadObject.getWidth()));
			model.add(cadSubject,cadItemPropertyXpos,getDoubleTypedLiteral(cadObject.getxPlace()));
			model.add(cadSubject,cadItemPropertyYpos,getDoubleTypedLiteral(cadObject.getyPlace()));
			model.add(cadSubject,cadItemPropertyZpos,getDoubleTypedLiteral(cadObject.getzPlace()));
			
			cadItemResources.put(cadObject.objectName, cadSubject);
		}
		try {
			if(file!=null) {
				FileOutputStream fop = new FileOutputStream(file);
				RDFDataMgr.write(fop, model, Lang.RDFXML);
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

	}



}
