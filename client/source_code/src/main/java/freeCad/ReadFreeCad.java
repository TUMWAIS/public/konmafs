package freeCad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import entities.ModelFile;

public class ReadFreeCad {
	
	public static String IGNORE_ELEMENT_ORIGIN = "Origin";
	public static String IGNORE_ELEMENT_X_AXIS = "X_Axis";
	public static String IGNORE_ELEMENT_Y_AXIS = "Y_Axis";
	public static String IGNORE_ELEMENT_Z_AXIS = "Z_Axis";
	public static String IGNORE_ELEMENT_XY_PLANE = "XY_Plane";
	public static String IGNORE_ELEMENT_XZ_PLANE = "XZ_Plane";
	public static String IGNORE_ELEMENT_YZ_PLANE = "YZ_Plane";
	
	List<String>ignoreElements = new ArrayList<>();
	private DocumentContent documentContent = new DocumentContent();
	private FreeCadObject currentFreeCadObject;
	private ModelFile freeCadModelFile;

	public ReadFreeCad(ModelFile freeCadModelFile) {
		this.freeCadModelFile = freeCadModelFile;
		ignoreElements.add(IGNORE_ELEMENT_ORIGIN);
		ignoreElements.add(IGNORE_ELEMENT_X_AXIS);
		ignoreElements.add(IGNORE_ELEMENT_Y_AXIS);
		ignoreElements.add(IGNORE_ELEMENT_Z_AXIS);
		ignoreElements.add(IGNORE_ELEMENT_XY_PLANE);
		ignoreElements.add(IGNORE_ELEMENT_XZ_PLANE);
		ignoreElements.add(IGNORE_ELEMENT_YZ_PLANE);
	}

	public File readZip(File zip) {
		String[] zipSplit = zip.getName().split("\\.");
		if (!zipSplit[zipSplit.length - 1].equals("FCStd")) {
			throw new IllegalArgumentException("A FCStd file is expected. Got " + zip.getName());
		}
		try {
			ZipInputStream zin = new ZipInputStream(new FileInputStream(zip));
			ZipEntry entry = null;
			File file = new File("Document.xml");
			while ((entry = zin.getNextEntry()) != null) {
				if (!entry.getName().equals("Document.xml")) {
					continue;
				}
				FileOutputStream os = new FileOutputStream(file);
				for (int c = zin.read(); c != -1; c = zin.read()) {
					os.write(c);
				}
				os.close();
				
				return file;
			}
			zin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void readFreeCadEntity() {
		readFreeCadFile(freeCadModelFile.getFile());
	}

	public void readFreeCadFile(File freeCadFile) {
		freeCadFile = readZip(freeCadFile);
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(freeCadFile);
			doc.getDocumentElement().normalize();
			NodeList rootChildren = doc.getDocumentElement().getChildNodes();
			for (int i = 0; i < rootChildren.getLength(); i++) {
				Node temp = rootChildren.item(i);
				if (temp.getNodeType() == Node.ELEMENT_NODE) {
					Element rootChild = (Element) temp;
					String tagName = rootChild.getTagName();
					switch (tagName) {
					case "Properties":
						NodeList properties = rootChild.getChildNodes();
						for (int j = 0; j < properties.getLength(); j++) {
							Node propertiesChild = properties.item(j);
							if (propertiesChild.getNodeType() == Node.ELEMENT_NODE) {
								Element propertiesChildElement = (Element) propertiesChild;
								if (propertiesChildElement.getTagName().equals("Property")) {
									parseDocumentPropertyElement(propertiesChildElement);
								}
							}
						}
						break;
					case "ObjectData":
						NodeList objectData = rootChild.getChildNodes();
						for (int j = 0; j < objectData.getLength(); j++) {
							Node objectChild = objectData.item(j);
							if (objectChild.getNodeType() == Node.ELEMENT_NODE) {
								Element objectDataChild = (Element) objectChild;
								if (objectDataChild.getTagName().equals("Object")) {
									String objectName = objectDataChild.getAttribute("name");
									boolean ignore = false;
									for(String ignoreElment : ignoreElements) {
										if(objectName.contains(ignoreElment)) {
											ignore = true;
										}
									}
									if(!ignore) {
										parseObjectElement(objectDataChild);
									}
								}
							}
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if(freeCadFile.exists()) {
				freeCadFile.delete();
			}
		}
		markTopLevelGroups();
		eliminateElementsFromFoldersInOtherFolders();
	}
	
	private void eliminateElementsFromFoldersInOtherFolders() {
		for(FreeCadGroup group : documentContent.getGroups()) {
			
			List<String> allElementsInGroup = group.getGroupedElements();
			List<String> groupsInGroup = new ArrayList<>();
			List<String> otherElementsInGroup = new ArrayList<>();
			for(String elementOfGroup : allElementsInGroup) {
				if(elementOfGroup.contains("Group")) {
					groupsInGroup.add(elementOfGroup);
				}
				else {
					otherElementsInGroup.add(elementOfGroup);
				}
			}
			List<String> elementsToRemoveFromOtherElementsOfGroup = new ArrayList<>();
			for(String groupInGroup : groupsInGroup) {
				for(FreeCadGroup subGroup : documentContent.getGroups()) {
					if(subGroup.getObjectName().equals(groupInGroup)) {
						elementsToRemoveFromOtherElementsOfGroup.addAll(subGroup.getGroupedElements());
					}
				}
			}
			group.getGroupedElements().removeAll(elementsToRemoveFromOtherElementsOfGroup);
		}
	}
	
	private void markTopLevelGroups() {
		for(FreeCadGroup group : documentContent.getGroups()) {
			String groupInternalName = group.objectName;
			Optional<FreeCadGroup> groupContainingThisGroup = documentContent.getGroups().stream().filter(g -> g.getGroupedElements().contains(groupInternalName)).findFirst();
			if(groupContainingThisGroup.isEmpty()) {
				group.setTopLevelGroup(true);
			}
		}
	}
	
	private void parseObjectElement(Element objectElement) {
		String objectName = objectElement.getAttribute("name");
		
		String objectLabel = getObjectLabel(objectElement);
		
		
		
		FreeCadGroup group = new FreeCadGroup(objectName,objectLabel);
		group = getGroupIfElementIsGroup(objectElement,group);
		
		
		if (group==null) {
			currentFreeCadObject = new FreeCadObject(objectName);
			currentFreeCadObject.setLabel(objectLabel);
			NodeList objectChildren = objectElement.getChildNodes();
			parseObjectChildren(objectChildren);
			documentContent.getObjects().add(currentFreeCadObject);
		}
		else {
			documentContent.getGroups().add(group);
		}
	}
	
	private void parseObjectChildren(NodeList objectChildren) {
		Element objectProperties = findFirstChildElement(objectChildren, "Properties");
		if (objectProperties != null) {
			parseObjectProperties(objectProperties.getChildNodes());
		} else {
			System.err.println("Found an Object without properties");
		}
	}
	
	private String getObjectLabel(Element object) {
		Element properties = findFirstChildElement(object.getChildNodes(), "Properties");
		List<Element> propertiesChildren = findAllChildElements(properties.getChildNodes(), "Property");
		for (Element property : propertiesChildren) {
			if(property.getAttribute("name").equals("Label")) {
				return findFirstChildElement(property.getChildNodes(), "String").getAttribute("value");
			}
		}
		return null;
	}

	private void parseDocumentPropertyElement(Element propertyElement) {
		DocumentProperties properties = documentContent.getDocumentProperties();
		Element child = findFirstChildElement(propertyElement.getChildNodes(), "String");
		String propertyName = propertyElement.getAttribute("name");
		switch (propertyName) {
		case "CreationDate":
			properties.setCreationDate(parseDateElement(child));
			break;
		case "LastModifiedDate":
			properties.setLastModifiedDate(parseDateElement(child));
			break;
		case "Label":
			documentContent.setFileName(child.getAttribute("value"));
		}
	}

	private void parseObjectProperties(NodeList objectProperties) {
		List<Element> propertyElements = findAllChildElements(objectProperties, "Property");
		for (Element propertyElement : propertyElements) {
			String nameValue = propertyElement.getAttribute("name");
			switch (nameValue) {
			case "Height":
			case "Plane_Height":
				currentFreeCadObject.setHeight(extractChildFloatValueFromProperty(propertyElement));
				break;
			case "Length":
			case "Plane_Length":
				currentFreeCadObject.setLength(extractChildFloatValueFromProperty(propertyElement));
				break;
			case "Width":
			case "Plane_Width":
				currentFreeCadObject.setWidth(extractChildFloatValueFromProperty(propertyElement));
				break;
			case "Placement":
				writePlacement(propertyElement);
				break;
			default:
			}
		}
	}

	private void writePlacement(Element element) {
		Element propertyPlacement = findFirstChildElement(element.getChildNodes(),"PropertyPlacement");
		currentFreeCadObject.setxPlace(Double.parseDouble(propertyPlacement.getAttribute("Px")));
		currentFreeCadObject.setyPlace(Double.parseDouble(propertyPlacement.getAttribute("Py")));
		currentFreeCadObject.setzPlace(Double.parseDouble(propertyPlacement.getAttribute("Pz")));
	}
	
	private Double extractChildFloatValueFromProperty(Element element) {
		return Double.parseDouble(findFirstChildElement(element.getChildNodes(), "Float").getAttribute("value"));
	}

	private List<String> extractStringValuesFromLinkList(Element element) {
		if (!element.getTagName().equals("LinkList")) {
			System.err.println("Attempting to extract Links from an Element that is not LinkList but "
					+ element.getTagName() + ". This may go wrong.");
		}
		List<String> linkListElements = new ArrayList<String>();
		for (Element e : findAllChildElements(element.getChildNodes(), "Link")) {
			linkListElements.add(e.getAttribute("value"));
		}
		return linkListElements;
	}

	private Element findFirstChildElement(NodeList nodes, String tagName) {
		for (int i = 0; i < nodes.getLength(); i++) {
			Node currentNode = nodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element) currentNode;
				if (currentElement.getTagName().equals(tagName)) {
					return currentElement;
				}
			}
		}
		return null;
	}

	private List<Element> findAllChildElements(NodeList nodes, String tagName) {
		List<Element> foundElements = new ArrayList<Element>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node currentNode = nodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element) currentNode;
				if (currentElement.getTagName().equals(tagName)) {
					foundElements.add(currentElement);
				}
			}
		}
		return foundElements;
	}

	private FreeCadGroup getGroupIfElementIsGroup(Element e,FreeCadGroup group) {
		
		boolean type_App__GroupExtension = false;
		boolean type_App__OriginGroupExtension = false;
		Element extensionsElement = findFirstChildElement(e.getChildNodes(), "Extensions");
		if(extensionsElement!=null) {
			Element extensionElement = findFirstChildElement(extensionsElement.getChildNodes(), "Extension");
			if(extensionElement!=null && extensionElement.hasAttribute("type")) {
				if(extensionElement.getAttributes().getNamedItem("type").getNodeValue().equals("App::GroupExtension")) {
					type_App__GroupExtension = true;
				}
				if(extensionElement.getAttributes().getNamedItem("type").getNodeValue().equals("App::OriginGroupExtension")) {
					type_App__OriginGroupExtension = true;
				}
				type_App__GroupExtension =extensionElement.getAttributes().getNamedItem("type").getNodeValue().equals("App::GroupExtension");
			}
		}
		if(type_App__GroupExtension == false && type_App__OriginGroupExtension==false) {
			return null;
		}
		else {
			List<String> groupElmentsWithInternalNameList = getGroupedElements(e);
			group.setGroupedElements(groupElmentsWithInternalNameList);
			
		}
		Element objectProperties = findFirstChildElement(e.getChildNodes(), "Properties");
		List<Element> propertyElements = findAllChildElements(objectProperties.getChildNodes(), "Property");
		for (Element propertyElement : propertyElements) {
			String nameValue = propertyElement.getAttribute("name");
			switch (nameValue) {
			case "Height":
			case "Plane_Height":
				group.setHeight(extractChildFloatValueFromProperty(propertyElement));
				break;
			case "Length":
			case "Plane_Length":
				group.setLength(extractChildFloatValueFromProperty(propertyElement));
				break;
			case "Width":
			case "Plane_Width":
				group.setWidth(extractChildFloatValueFromProperty(propertyElement));
				break;
			case "Placement":
				Element propertyPlacement = findFirstChildElement(propertyElement.getChildNodes(),"PropertyPlacement");
				group.setxPlace(Double.valueOf(propertyPlacement.getAttribute("Px")));
				group.setyPlace(Double.valueOf(propertyPlacement.getAttribute("Py")));
				group.setzPlace(Double.valueOf(propertyPlacement.getAttribute("Pz")));
				break;
			default:
			}
		}
		return group;
	}

	private List<String> getGroupedElements(Element e) {
		List<String> group = new ArrayList<String>();
		Element propertiesElement = findFirstChildElement(e.getChildNodes(), "Properties");
		List<Element> propertiesElementChildren = findAllChildElements(propertiesElement.getChildNodes(), "Property");
		for (Element childElement : propertiesElementChildren) {
			if (childElement.hasAttribute("name")) {
				if (childElement.getAttribute("name").equals("Group")) {
					return extractStringValuesFromLinkList(
							findFirstChildElement(childElement.getChildNodes(), "LinkList"));
				}
			}
		}
		return group;
	}

	private Date parseDateElement(Element element) {
		if (element.getTagName().equals("String") & element.hasAttribute("value")) {
			return parseDate(element.getAttribute("value"));
		} else {
			return null;
		}

	}

	private Date parseDate(String str) {
		try {
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			return dateFormatter.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public DocumentContent getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(DocumentContent documentContent) {
		this.documentContent = documentContent;
	}
}