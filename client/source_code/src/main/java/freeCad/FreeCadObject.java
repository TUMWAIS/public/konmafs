package freeCad;

public class FreeCadObject extends FreeCadEntity{
	private double height, length, width;
	private double xPlace, yPlace, zPlace;
	
	public FreeCadObject(String objectName) {
		super.objectName = objectName;
	}
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getxPlace() {
		return xPlace;
	}

	public void setxPlace(double xPlace) {
		this.xPlace = xPlace;
	}

	public double getyPlace() {
		return yPlace;
	}

	public void setyPlace(double yPlace) {
		this.yPlace = yPlace;
	}

	public double getzPlace() {
		return zPlace;
	}

	public void setzPlace(double zplace) {
		this.zPlace = zplace;
	}

	public String toString() {
		return label;
	}
}