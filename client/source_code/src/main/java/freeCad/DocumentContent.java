package freeCad;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DocumentContent {

	private DocumentProperties documentProperties;
	private String fileName;
	private List<FreeCadObject> objects;
	private List<FreeCadGroup> groups = new ArrayList<FreeCadGroup>();
	private List<FreeCadGroup> topLevelGroupsWithBaseElements = new ArrayList<FreeCadGroup>();
	static List<String> namesNotVisible ;
	
	static {
		namesNotVisible = new ArrayList<>();
		namesNotVisible.add("Origin");
		namesNotVisible.add("X_Axis");
		namesNotVisible.add("Y_Axis");
		namesNotVisible.add("Z_Axis");
		namesNotVisible.add("XY_Plane");
		namesNotVisible.add("XZ_Plane");
		namesNotVisible.add("YZ_Plane");
		namesNotVisible.add("Body");
	}

	public DocumentContent() {
		this.documentProperties = new DocumentProperties();
		this.objects = new ArrayList<>();
	}

	public DocumentContent(DocumentProperties documentProperties) {
		this.documentProperties = documentProperties;
	}

	public DocumentProperties getDocumentProperties() {
		return documentProperties;
	}

	public void setDocumentProperties(DocumentProperties documentProperties) {
		this.documentProperties = documentProperties;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public List<FreeCadObject> getVisibleObjects() {
		return objects.stream().filter(o -> !namesNotVisible.contains(o.getLabel())).collect(Collectors.toList());
	}

	public List<FreeCadObject> getObjects() {
		return objects;
	}

	public void setObjects(List<FreeCadObject> objects) {
		this.objects = objects;
	}

	public List<FreeCadGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<FreeCadGroup> groupedObjects) {
		this.groups = groupedObjects;
	}

	public List<FreeCadEntity> getAllContent() {
		List<FreeCadEntity> allContent = new ArrayList<FreeCadEntity>();
		allContent.addAll(objects);
		allContent.addAll(groups);
		return allContent;
	}

	public String toString() {
		String objectsString = "";
		for (FreeCadObject obj : objects) {
			objectsString += obj.toString() + "\n";
		}
		String groupsString = "";
		for (FreeCadGroup group : groups) {
			groupsString += "Gruppe " + group.getGroupName() + ": ";
			for (String itemName : group.getGroupedElements()) {
				groupsString += itemName + ",";
			}
			groupsString = groupsString.substring(0, groupsString.length() - 1);
			groupsString += "\n";
		}
		groupsString = groupsString.substring(0, groupsString.length() - 1);
		return fileName + "\nProperties:\n" + this.documentProperties.toString() + "\nObjects:\n" + objectsString
				+ "\nGroups:\n" + groupsString;
	}

	public List<FreeCadGroup> getTopLevelGroupsWithBaseElements() {
		return topLevelGroupsWithBaseElements;
	}

	public void setTopLevelGroupsWithBaseElements(List<FreeCadGroup> topLevelGroupsWithBaseElements) {
		this.topLevelGroupsWithBaseElements = topLevelGroupsWithBaseElements;
	}

}