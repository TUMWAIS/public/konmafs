package freeCad;

import java.util.ArrayList;
import java.util.List;

public class FreeCadGroup extends FreeCadEntity{
	private boolean topLevelGroup = false;
	private List<String> groupedElements;
	
	private double height, length, width;
	private Double xPlace, yPlace, zPlace;
	
	public FreeCadGroup(String objectName,String groupName) {
		super.objectName=objectName;
		super.label = groupName;
		this.groupedElements = new ArrayList<String>();
	}

	public String getGroupName() {
		return super.label;
	}

	public void setGroupName(String groupName) {
		super.label = groupName;
	}

	public List<String> getGroupedElements() {
		return groupedElements;
	}

	public void setGroupedElements(List<String> groupedElements) {
		this.groupedElements = groupedElements;
	}
	
	public void addGroupElement(String elementLabel) {
		this.groupedElements.add(elementLabel);
	}
	
	public String toString() {
		return label;
	}

	public boolean isTopLevelGroup() {
		return topLevelGroup;
	}

	public void setTopLevelGroup(boolean topLevelGroup) {
		this.topLevelGroup = topLevelGroup;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public Double getxPlace() {
		return xPlace;
	}

	public void setxPlace(Double xPlace) {
		this.xPlace = xPlace;
	}

	public Double getyPlace() {
		return yPlace;
	}

	public void setyPlace(Double yPlace) {
		this.yPlace = yPlace;
	}

	public Double getzPlace() {
		return zPlace;
	}

	public void setzPlace(Double zPlace) {
		this.zPlace = zPlace;
	}
}