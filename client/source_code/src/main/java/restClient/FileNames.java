package restClient;

public class FileNames {
	private String[] rdfModels;
	private String jsonFile;
	
	public String[] getRdfModels() {
		return rdfModels;
	}
	public void setRdfModels(String[] rdfModels) {
		this.rdfModels = rdfModels;
	}
	public String getJsonFiles() {
		return jsonFile;
	}
	public void setJsonFiles(String jsonFile) {
		this.jsonFile = jsonFile;
	}
}
