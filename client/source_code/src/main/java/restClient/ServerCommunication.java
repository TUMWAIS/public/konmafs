package restClient;

import java.io.StringWriter;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import gui.ResultMode;
import rdf.RDFBaseModel;
import resultProcessing.ResultObject;

public class ServerCommunication {
	
	private int serverPort;
	private String serverIpAdress;
	
	public ServerCommunication(String serverIpAdress, int serverPort) {
		this.serverPort = serverPort;
		this.serverIpAdress = serverIpAdress;
	}
	
	public boolean checkConnection() {
		boolean connectionOk = false;
		try {
			HttpRequest request = HttpRequest.newBuilder()
					  .uri(new URI("http://"+serverIpAdress+":"+serverPort+"/testConnection"))
					  .GET()
					  .build();
			HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());
			if(response.body().toString().equals("KonMaFs server works") ) {
				connectionOk = true;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return connectionOk;
	}
	
	public String sendJSON(String jsonAsString) {
		String fileNameOnServer = "noFileName";
		try {
		
			HttpRequest request = HttpRequest.newBuilder()
			  .uri(new URI("http://"+serverIpAdress+":"+serverPort+"/sendFile"))
			  .header("filename", "manualModel.json")
			  .header("Content-Type", "text/plain;charset=UTF-8")
			  .POST(HttpRequest.BodyPublishers.ofString(jsonAsString))
			  .build();
		
		HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());
		fileNameOnServer = response.body().toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return fileNameOnServer;
	}

	public String sendRDF(RDFBaseModel rdfModel,String fileName) {
		String fileNameOnServer = "noFileName";
		try {
		StringWriter strwriter = new StringWriter();
		RDFDataMgr.write(strwriter,rdfModel.getModel(),Lang.RDFXML);
		
		HttpRequest request = HttpRequest.newBuilder()
				  .uri(new URI("http://"+serverIpAdress+":"+serverPort+"/sendFile"))
				  .header("filename", fileName)
				  .header("Content-Type", "text/plain;charset=UTF-8")
				  .POST(HttpRequest.BodyPublishers.ofString(strwriter.toString()))
				  .build();
		
		HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());
		fileNameOnServer = response.body().toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return fileNameOnServer;
		
	}
	
	public List<ResultObject> sendFileListAndGetResults(FileNames fileNames,ResultMode resultMode){
		String methodToCallOnServer = "";
		if(resultMode== ResultMode.INCONSISTENCIES) {
			methodToCallOnServer = "checkConsistence";
		}
		if(resultMode == ResultMode.UPDATE) {
			methodToCallOnServer = "checkUpdate";
		}
		
		List<ResultObject> results = null;
		try {
			ObjectMapper objectMapper = new ObjectMapper();		
			String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(fileNames);
			HttpRequest request = HttpRequest.newBuilder()
					  .uri(new URI("http://"+serverIpAdress+":"+serverPort+"/"+methodToCallOnServer))
					  .header("Content-Type", "application/json;charset=UTF-8")
					  .POST(HttpRequest.BodyPublishers.ofString(jsonString))
					  .build();
			HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());
			String responseText = response.body();
			if(!responseText.startsWith("[")) {
				responseText = "["+responseText+"]";
			}
			
			results = objectMapper.readValue(responseText, new TypeReference<List<ResultObject>>() {});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}
}
