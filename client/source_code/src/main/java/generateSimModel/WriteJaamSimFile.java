package generateSimModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import bpmn.bpmnEntities.BPMNNodeTyp;

public class WriteJaamSimFile {
	
	Map<BPMNNodeTyp, Set<String>> bpmnTypes = new HashMap<>();
	public static final String BRANCH = "Branch";
	public static final String SERVER = "Server";
	public static final String ENTITY_GENERATOR = "EntityGenerator";
	public static final String ENTITY_SINK = "EntitySink";
	public static final String QUEUE = "Queue";
	public static final String SIM_ENTITY = "SimEntity";
	public static final String ENTITY_CONVEYOR = "EntityConveyor";
	public static final String NEXT_COMPONENT = "NextComponent";
	public static final String POSITION = "Position";
	
	StringBuilder simulationFile = new StringBuilder();
	String pathToDirectory = null;

	List<BPMNQueryResult> results;
	LinkedHashMap<String,Conveyor> conveyors = new LinkedHashMap<>();
	LinkedHashMap<String,Integer> simEntitys = new LinkedHashMap<>();
	TreeMap<String,String> serverToQueueMap = new TreeMap<>();
	TreeSet<String> entityGeneratorNames = new TreeSet<>();
	LinkedHashMap<String,Point2D.Double> positionMap = new LinkedHashMap<>();
	
	public void readElements() {
		readBPMNTypes();
		readSimEntity();	
		readEntityGenerators();
		readQueuesNamesForServer();
		readConveyors();
	}
	
	public void writeBPMNResultToJaamSim(Map<String,Double> travelTimeForConveyors) {
		writeDefineSection();
		simulationFile.append(getEntityGenerator());
		simulationFile.append(getEntityConveyor(travelTimeForConveyors));
		simulationFile.append(getBranchSection());
		simulationFile.append(getSimEntitySection());
		simulationFile.append(getServerSection());
		simulationFile.append(getEntitySinkSection());
		simulationFile.append(getQueueSection());		
		simulationFile.append(getPositionSection());	
		simulationFile.append(getDisplayStaticText());
		writeToFile();
	}

	private String getRemainingPositionSection() {
		StringBuilder positionSection = new StringBuilder();
		positionMap.put(entityGeneratorNames.first().split("__")[0], new Point2D.Double(0.0d,0.5d));
		String nextPredecessorName = entityGeneratorNames.first().split("__")[0];
		ArrayList<Conveyor> remainingStartConveyor = new ArrayList<>();

		for(int i=0;i<conveyors.size();i++) {
			final String nextName = nextPredecessorName;
			List<Conveyor> conveyorsForCurrentElement = conveyors.values().stream()
														 .filter(conv -> conv.getPredecessorName().equals(nextName))
														 .collect(Collectors.toList());
			Conveyor currentConveyor = null;
			double correctionFactorHeight = 0.0;
			double correctionFactorLength = 0.0;
			if(conveyorsForCurrentElement.size()>1) {
				for(int j=1;j<conveyorsForCurrentElement.size();j++) {
					remainingStartConveyor.add(conveyorsForCurrentElement.get(j));
				}
				correctionFactorHeight = 1.0;
				correctionFactorLength = -2.0;
				currentConveyor = conveyorsForCurrentElement.get(0);
			}
			if(conveyorsForCurrentElement.size()==1) {
				currentConveyor = conveyorsForCurrentElement.get(0);
			}
			if(conveyorsForCurrentElement.size()==0) {
				currentConveyor = remainingStartConveyor.get(0);
				remainingStartConveyor.remove(0);
				correctionFactorHeight = 1.0;
				correctionFactorLength = -2.0;
			}
			Point2D.Double startPoint = positionMap.get(currentConveyor.getPredecessorName());
			Point2D.Double endPoint = new Point2D.Double(startPoint.getX()+6.0d+correctionFactorLength,startPoint.getY()+correctionFactorHeight);
			if(positionMap.containsKey(currentConveyor.getSuccessorName())) {
				endPoint = positionMap.get(currentConveyor.getSuccessorName());
			}
			positionSection.append(currentConveyor.getConveyorName());
			positionSection.append(" "+"Points"+" ");
			positionSection.append("{ { "+startPoint.getX()+" "+startPoint.getY()+" 0.0 m } ");
			positionSection.append("{ "+endPoint.getX()+" "+endPoint.getY()+" 0.0 m } }\n\n");

			if(!positionMap.containsKey(currentConveyor.getSuccessorName())) {
				positionSection.append(currentConveyor.getSuccessorName());
				positionSection.append(" "+"Position"+" ");
				positionSection.append("{ "+endPoint.getX()+" "+endPoint.getY()+" 0.0 m }\n\n");
				nextPredecessorName = currentConveyor.getSuccessorName();
			}
			
			if(currentConveyor.getSuccessorTyp()==BPMNNodeTyp.serviceTask) {
				String serverName = "";
				for(Entry<String,String> serverToQueueEntry : serverToQueueMap.entrySet()) {
					if(serverToQueueEntry.getValue().equals(currentConveyor.getSuccessorName())) {
						serverName = serverToQueueEntry.getKey();
					}
				}
				positionSection.append(serverName);
				positionSection.append(" "+"Position"+" ");
				endPoint = new Point2D.Double(endPoint.getX(),endPoint.getY()+1.0);
				positionSection.append("{ "+endPoint.getX()+" "+endPoint.getY()+" 0.0 m }\n\n");
				nextPredecessorName = serverName;
			}
			
			positionMap.put(nextPredecessorName,endPoint);
		}
		return positionSection.toString();
	}
	
	
	public WriteJaamSimFile(List<BPMNQueryResult> results,String pathToDirectory) {
		this.results = results;
		this.pathToDirectory = pathToDirectory;
	}
	
	private void readBPMNTypes() {
		for(BPMNQueryResult result : results) {
			addItemToBpmnTypes(result.getPredecessorTyp(),bpmnTypes.get(result.getPredecessorTyp()),result.getPredecessorName());
			addItemToBpmnTypes(result.getSuccessorTyp(),bpmnTypes.get(result.getSuccessorTyp()),result.getSuccessorName());
		}
	}
	
	private void addItemToBpmnTypes(BPMNNodeTyp bpmnType, Set<String> namesForBPMNType,String name) {		
		if(namesForBPMNType == null) {
			Set<String> newSet = new TreeSet<String>();
			newSet.add(name);
			bpmnTypes.put(bpmnType, newSet);
		}
		else {
			namesForBPMNType.add(name);
		}
	}
	
	private void readSimEntity(){
		Set<String> simEntityNames = new TreeSet<String>();
		results.stream().filter(a-> a.getFlowItem()!=null).forEach(c-> simEntityNames.add(c.getFlowItem()));
		Integer simEntityCounter = Integer.valueOf(1);
		for(String simEntityName : simEntityNames) {
			simEntitys.put(simEntityName, simEntityCounter);
			simEntityCounter = Integer.valueOf(simEntityCounter.intValue() + 1);
		}
	}
	
	private void readQueuesNamesForServer() {
		if(bpmnTypes.get(BPMNNodeTyp.serviceTask)!=null) {
			Set<String> serviceTaskNames = bpmnTypes.get(BPMNNodeTyp.serviceTask);
			for(String serviceTaskName :serviceTaskNames) {
				serverToQueueMap.put(serviceTaskName, serviceTaskName+"_"+QUEUE);
			}
		}
	}

	
	private void readConveyors() {
		for(BPMNQueryResult result : results) {
			Conveyor conveyor = new Conveyor();
			conveyor.setPredecessorName(result.getPredecessorName());
			
			conveyor.setPredecessorTyp(result.getPredecessorTyp());
			conveyor.setSuccessorTyp(result.getSuccessorTyp());
			
			if(result.getPredecessorTyp()==BPMNNodeTyp.startEvent) {
				conveyor.setPredecessorName(entityGeneratorNames.first().split("__")[0]);
			}
			
			if(result.getSuccessorTyp()==BPMNNodeTyp.serviceTask) {
				conveyor.setSuccessorName(serverToQueueMap.get(result.getSuccessorName()));
			}
			else {
				conveyor.setSuccessorName(result.getSuccessorName());
			}
			if(result.getFlowItem()!=null) {
				conveyor.getFlowItems().add(result.getFlowItem());
			}
			
			if(conveyors.get(conveyor.getConveyorName())==null) {
				conveyors.put(conveyor.getConveyorName(), conveyor);
			}
			else {
				if(result.getFlowItem()!=null) {
					conveyors.get(conveyor.getConveyorName()).getFlowItems().add(result.getFlowItem());
				}
			}
			
		}
	}
	
	public Set<String> getConveyorNames(){
		Set<String> conveyorNames = new TreeSet<String>();
		conveyorNames.addAll(conveyors.keySet());
		return conveyorNames;
	}
	
	
	
	private void readEntityGenerators(){
		Set<String> entityGeneratorNameSet = bpmnTypes.get(BPMNNodeTyp.startEvent);
		Set<String> entityNames = simEntitys.keySet();
		for(String generatorName :entityGeneratorNameSet) {
			for(String entityName : entityNames) {
				entityGeneratorNames.add(generatorName+"__"+entityName);
			}
		}
	}
	
	private void writeDefineSection() {
		StringBuilder defineSection = new StringBuilder();
		for(Entry<BPMNNodeTyp,Set<String>> bpmnEntry : bpmnTypes.entrySet()) {
			switch(bpmnEntry.getKey()) {
				case exclusiveGateway : defineSection.append(getDefineString(BRANCH,bpmnEntry.getValue()));break;
				case serviceTask :  defineSection.append(getDefineString(SERVER,bpmnEntry.getValue()));
									defineSection.append(getDefineString(QUEUE,new TreeSet<String>(serverToQueueMap.values())));break;
				case startEvent : 	defineSection.append(getDefineString(ENTITY_GENERATOR,entityGeneratorNames));
									defineSection.append(getDefineString(SIM_ENTITY,simEntitys.keySet()));break;
				case endEvent : defineSection.append(getDefineString(ENTITY_SINK,bpmnEntry.getValue()));break;
				default:
			}
		}
		
		defineSection.append(getDefineString(ENTITY_CONVEYOR,getConveyorNames()));
		simulationFile.append(getWholeDefineSectionString(defineSection.toString()));	
	}
	
	private String getEntitySinkSection() {
		StringBuilder entitySinkSection = new StringBuilder();
		entitySinkSection.append("# *** EntitySink ***\n\n");
		return entitySinkSection.toString();
	}
	
	private String getQueueSection() {
		StringBuilder queueSection = new StringBuilder();
		queueSection.append("# *** Queue ***\n\n");
		return queueSection.toString();
	}
	
	private String getDefineString(String defineType, Set<String> typeNames) {
		List<String> sortedNames = typeNames.stream().sorted().collect(Collectors.toList());     
		String nameList = "{ "+String.join("  ", sortedNames)+ " }";
		String defineLine = "Define "+defineType+" "+nameList+"\n";
		return defineLine;
	}
	
	private String getWholeDefineSectionString(String dynamicDefineSectionPart) {
		StringBuilder defineSection = new StringBuilder();
		defineSection.append("\n").append("RecordEdits\n\n");
		defineSection.append(dynamicDefineSectionPart);
		
		defineSection.append("Define ColladaModel { Axis  Grid100x100 }\n");
		defineSection.append("Define DisplayEntity { XY-Grid  XYZ-Axis }\n");
		defineSection.append("Define OverlayClock { Clock }\n");
		defineSection.append("Define OverlayText { Title }\n");
		defineSection.append("Define View { View1 }\n");
		defineSection.append("\n");
		return defineSection.toString();
	}
	
	private String getEntityConveyor(Map<String,Double> travelTimeForConveyors) {
		StringBuilder entityConveyorSection = new StringBuilder();
		entityConveyorSection.append("# *** EntityConveyor ***\n\n");
		
		for(Conveyor conveyor : conveyors.values()) {
			entityConveyorSection.append(conveyor.getConveyorName());
			entityConveyorSection.append(" "+NEXT_COMPONENT+" ");
			entityConveyorSection.append("{ "+conveyor.getSuccessorName()+" }\n");
			entityConveyorSection.append(conveyor.getConveyorName());
			entityConveyorSection.append(" "+"TravelTime"+" ");
			if(travelTimeForConveyors.get(conveyor.getConveyorName())!=null) {
				entityConveyorSection.append("{ "+travelTimeForConveyors.get(conveyor.getConveyorName()).toString()+" s"+" }\n\n");	
			}
			else {
				entityConveyorSection.append("{ "+"1.0 s"+" }\n\n");	
			}
		}
		return entityConveyorSection.toString();
	}
	
	private String getEntityGenerator() {
		StringBuilder entityGeneratorSection = new StringBuilder();
		entityGeneratorSection.append("# *** EntityGenerator ***\n\n");
		
		int firstTimeArrivial =1;
		for(String generatorName : entityGeneratorNames) {
			entityGeneratorSection.append(generatorName);
			entityGeneratorSection.append(" "+NEXT_COMPONENT+" ");
			entityGeneratorSection.append("{ "+getNextConveyorNameForPredecessorName(generatorName.split("__")[0])+" }\n");
			
			entityGeneratorSection.append(generatorName);
			entityGeneratorSection.append(" FirstArrivalTime ");
			entityGeneratorSection.append("{ "+firstTimeArrivial+"  s }\n");
			
			entityGeneratorSection.append(generatorName);
			entityGeneratorSection.append(" InterArrivalTime ");
			entityGeneratorSection.append("{ 3  s }\n");
			firstTimeArrivial +=2;
			
			entityGeneratorSection.append(generatorName);
			entityGeneratorSection.append(" "+"PrototypeEntity"+" ");
			String entityNameForGenerator = generatorName.split("__")[1];
			entityGeneratorSection.append("{ "+entityNameForGenerator+" }"+"\n\n");
			
		}
		return entityGeneratorSection.toString();
	}
	
	private String getSimEntitySection() {
		StringBuilder branchSection = new StringBuilder();
		branchSection.append("# *** SimEntity ***\n\n");
		
		for(Entry<String, Integer> simEntry : simEntitys.entrySet()) {
			branchSection.append(simEntry.getKey());
			branchSection.append(" "+"InitialState"+" ");
			branchSection.append("{ "+simEntry.getValue().toString()+" }\n\n");
		}		
		
		return branchSection.toString();
	}
	
	private String getServerSection() {
		StringBuilder serverSection = new StringBuilder();
		serverSection.append("# *** Server ***\n\n");
		
		for(Entry<String,String> serverQueueEntry : serverToQueueMap.entrySet()) {
			serverSection.append(serverQueueEntry.getKey());
			serverSection.append(" "+NEXT_COMPONENT+" ");
			serverSection.append("{ "+getNextConveyorNameForPredecessorName(serverQueueEntry.getKey())+" }\n");
			serverSection.append(serverQueueEntry.getKey());
			serverSection.append(" "+"WaitQueue"+" ");
			serverSection.append("{ "+serverQueueEntry.getValue()+" }\n\n");
			serverSection.append(serverQueueEntry.getKey());
			serverSection.append(" "+"ServiceTime"+" ");
			serverSection.append("{ 1 s }\n\n");
		}
		return serverSection.toString();
	}
	
	private String getBranchSection() {
		StringBuilder branchSection = new StringBuilder();
		branchSection.append("# *** Branch ***\n\n");
		
		Set<String> branchNames = bpmnTypes.get(BPMNNodeTyp.exclusiveGateway);
		
		for(String branchName : branchNames) {
			ArrayList<Conveyor> conveyorsForBranch = new ArrayList<>();
			for(Conveyor conveyor : conveyors.values()) {
				
				if(conveyor.getPredecessorName().equals(branchName)) {
					conveyorsForBranch.add(conveyor);
				}
			}
			
			if(conveyorsForBranch.size() == 1) {
				branchSection.append(branchName);
				branchSection.append(" NextComponentList ");
				branchSection.append("{ "+conveyorsForBranch.get(0).getConveyorName()+" }\n");
				branchSection.append(branchName);
				branchSection.append(" Choice { 1 }\n\n");
			}
			if(conveyorsForBranch.size()>1) {
				branchSection.append(branchName);
				branchSection.append(" NextComponentList ");
				branchSection.append("{ "+getNextComponentListForBranch(conveyorsForBranch)+" }\n");
				branchSection.append(branchName);
				branchSection.append(" Choice { parseNumber([Switch_01].obj.State) }\n\n");				
			}
			
		}
		
		
		return branchSection.toString();
	}
	
	private String getNextComponentListForBranch(ArrayList<Conveyor> conveyorsForBranch) {
		TreeMap<Integer, String> sortedConveyors = new TreeMap<>();
		for(int i=0;i<conveyorsForBranch.size();i++) {
			Conveyor currentConveyor = conveyorsForBranch.get(i);
			Integer entityNumber = simEntitys.get(currentConveyor.getFlowItems().get(0));
			sortedConveyors.put(entityNumber, currentConveyor.getConveyorName());
		}
		String output = "";
		
		for(String conveyorName : sortedConveyors.values()) {
			output += (conveyorName+" ");
		}
		
		return output;
	}
	
	private String getNextConveyorNameForPredecessorName(String predecessorName) {
		 Optional<String> nextComponentName = conveyors.values().stream().filter(conveyor -> conveyor.getPredecessorName()
																.contains(predecessorName))
																.map(conveyor -> conveyor.getConveyorName())
																.findAny();
		 if(nextComponentName.isPresent()) {
			 return nextComponentName.get();
		 }
		 else {
			 return null;
		 }
	}
	
	private String getPositionSection() {
		StringBuilder fullPositionSection = new StringBuilder();
		fullPositionSection.append(getGraphicsInputsSection());
		fullPositionSection.append(getEntityPositionSection());
		fullPositionSection.append(getGeneratorPositionSection());
		fullPositionSection.append(getRemainingPositionSection());
		return fullPositionSection.toString();
	}
	
	private String getGraphicsInputsSection() {
		StringBuilder graphicsInputsSection = new StringBuilder();
		
		graphicsInputsSection.append("# *** GRAPHICS INPUTS ***\n\n");
		graphicsInputsSection.append("Simulation RealTime { TRUE }\n");
		graphicsInputsSection.append("Simulation SnapToGrid { TRUE }\n");
		graphicsInputsSection.append("Simulation RealTimeFactor { 1 }\n");
		graphicsInputsSection.append("Simulation ShowLabels { TRUE }\n");
		graphicsInputsSection.append("Simulation ShowSubModels { TRUE }\n");
		graphicsInputsSection.append("Simulation PresentationMode { FALSE }\n");
		graphicsInputsSection.append("Simulation ShowModelBuilder { TRUE }\n");
		graphicsInputsSection.append("Simulation ShowObjectSelector { TRUE }\n");
		graphicsInputsSection.append("Simulation ShowInputEditor { TRUE }\n");
		graphicsInputsSection.append("Simulation ShowOutputViewer { TRUE }\n");
		graphicsInputsSection.append("Simulation ShowPropertyViewer { FALSE }\n");
		graphicsInputsSection.append("Simulation ShowLogViewer { FALSE }\n");
		graphicsInputsSection.append("Simulation ObjectSelectorPos { 46  289 }\n");
		graphicsInputsSection.append("Simulation ObjectSelectorSize { 289  413 }\n");
		graphicsInputsSection.append("Simulation ControlPanelWidth { 1707 }\n\n");
		
		
		return graphicsInputsSection.toString();
	}
	
	private String getEntityPositionSection() {
		StringBuilder entityPostionSection = new StringBuilder();
		
		double yPositionEntity = 0.5;
		for(String entityName : simEntitys.keySet()) {
			entityPostionSection.append(entityName);
			entityPostionSection.append(" "+POSITION+" ");
			entityPostionSection.append("{ -2.0 ");
			entityPostionSection.append(yPositionEntity+" ");
			entityPostionSection.append("0.0 m }\n\n");
			yPositionEntity -=2.0;
		}
		
		return entityPostionSection.toString();
	}
	
	private String getGeneratorPositionSection() {
		StringBuilder generatorPostionSection = new StringBuilder();
		
		double yPositionEntity = 0.5;
		for(String entityName : entityGeneratorNames) {
			generatorPostionSection.append(entityName);
			generatorPostionSection.append(" "+POSITION+" ");
			generatorPostionSection.append("{ 0.0 ");
			generatorPostionSection.append(yPositionEntity+" ");
			generatorPostionSection.append("0.0 m }\n\n");
			yPositionEntity -=2.0;
		}
		
		return generatorPostionSection.toString();
	}
	
	private String getDisplayStaticText() {
		StringBuilder displayText = new StringBuilder();
		
		displayText.append("# *** ColladaModel ***\n\n");
		displayText.append("Axis ColladaFile { <res>/shapes/axis_text.dae }\n\n");
		displayText.append("Grid100x100 ColladaFile { <res>/shapes/grid100x100.dae }\n\n");
		
		displayText.append("# *** DisplayEntity ***\n\n");
		displayText.append("XY-Grid Description { 'Grid for the X-Y plane (100 m x 100 m)' }\n");
		displayText.append("XY-Grid Size { 100  100  m }\n");
		displayText.append("XY-Grid DisplayModel { Grid100x100 }\n");
		displayText.append("XY-Grid Movable { FALSE }\n\n");
		
		displayText.append("XYZ-Axis Description { 'Unit vectors' }\n");
		displayText.append("XYZ-Axis Alignment { -0.4393409  -0.4410096  -0.4394292 }\n");
		displayText.append("XYZ-Axis Size { 1.125000  1.1568242  1.1266404  m }\n");
		displayText.append("XYZ-Axis DisplayModel { Axis }\n");
		displayText.append("XYZ-Axis Movable { FALSE }\n\n");
		
		
		displayText.append("# *** View ***\n\n");
		displayText.append("View1 Description { 'Default view window' }\n");
		displayText.append("View1 ViewCenter { 6.239961  0.775792  0.0  m }\n");
		displayText.append("View1 ViewPosition { 6.239961  0.775792  17.320508  m }\n");
		displayText.append("View1 WindowSize { 1365  576 }\n");
		displayText.append("View1 WindowPosition { 342  103 }\n");
		displayText.append("View1 ShowWindow { TRUE }\n");
		displayText.append("View1 Lock2D { TRUE }\n");
		displayText.append("View1 SkyboxImage { <res>/images/sky_map_2048x1024.jpg }\n");
		return displayText.toString();
	}
	
	public String getSumulationFileContent() {
		return simulationFile.toString();
	}
	
	private void writeToFile() {
		try {
			File file = new File(pathToDirectory+"\\generated.cfg");
			FileWriter fileWriter = null;
			fileWriter = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(simulationFile);	   
			printWriter.close();
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
	}
}
