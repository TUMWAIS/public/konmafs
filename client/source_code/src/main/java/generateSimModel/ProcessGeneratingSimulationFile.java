package generateSimModel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bpmn.bpmnEntities.BPMNNodeTyp;
import entities.ModelFile;
import freeCad.FreeCadObject;
import freeCad.ReadFreeCad;
import generateSimModel.serverCommunication.ConveyorLengthJson;
import generateSimModel.serverCommunication.GenerateSimModelServerCommunication;
import generateSimModel.serverCommunication.ResultConveyorTravelTime;
import gui.GuiConsistencRules;
import javafx.concurrent.Task;
import restClient.ServerCommunication;
import resultProcessing.ReadAndUploadModels;

public class ProcessGeneratingSimulationFile extends Task<Void> {

	List<ModelFile> modelFiles;
	ServerCommunication sendToServer;
	GuiConsistencRules gui;

	private File directory;

	public ProcessGeneratingSimulationFile(List<ModelFile> modelFiles, ServerCommunication sendToServer,
			GuiConsistencRules gui,File directory) {
		this.modelFiles = modelFiles;
		this.sendToServer = sendToServer;
		this.gui = gui;
		this.directory = directory;
	}
	
	private String giveAllBPMNConnectionsSPARQL() {
		String query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+ "PREFIX simItem: <http://example.org/simulation/simulationItem#> "
				+ "PREFIX simLogItem: <http://example.org/simulation/simulationLog#> "
				+ "PREFIX simLogCountItem: <http://example.org/simulation/simulationLogCountItem#> "
				+ "PREFIX bpmnItem: <http://example.org/bpmn/bpmnItem#> "
				+ "SELECT (?predecessorBPMNItemName AS ?predecessorName) ?predecessorBPMNItemTyp (?nextBPMNItemName AS ?successorName) ?successorBPMNItemTyp ?flowItem WHERE "
				+ "{ "
				+ "  ?predecessorBPMNItem ^bpmnItem:sourceNode ?flowWithCondition ."
				+ "  ?predecessorBPMNItem bpmnItem:name ?predecessorBPMNItemName ."
				+ "  ?predecessorBPMNItem bpmnItem:itemTyp ?predecessorBPMNItemTyp . "
				+ "  OPTIONAL { ?flowWithCondition bpmnItem:conditionExpression ?flowItem } ."
				+ "  ?flowWithCondition bpmnItem:targetNode ?nextBPMNNode ."
				+ "  ?nextBPMNNode bpmnItem:name ?nextBPMNItemName .  "
				+ "  ?nextBPMNNode bpmnItem:itemTyp ?successorBPMNItemTyp ."
				+ "} ";
		return query;
	}
	
	@Override
	protected Void call() throws Exception {
		processGerationOfSimulationFile();
		return null;
	}
	
	private Map<String,Double> getTravelTimeForConveyors(Set<String> conveyorNames){
		Map<String,Double> cadNameToLengthMap = new HashMap<>();
		String pathToPLCOnServer = "";
		for(ModelFile modelFile : modelFiles) {			
			if(modelFile.isFileOfType(".FCStd")) {
				ReadFreeCad readFreeCad = new ReadFreeCad(modelFile);
				readFreeCad.readFreeCadEntity();
				for(FreeCadObject cadObject : readFreeCad.getDocumentContent().getVisibleObjects()) {
					if(conveyorNames.contains(cadObject.getLabel())) {
						cadNameToLengthMap.put(cadObject.getLabel(), Double.valueOf(cadObject.getLength()));
					}
				}
			}
			if(modelFile.isFileOfType(".xml") || modelFile.isFileOfType(".txt")) {
				List<ModelFile> plcModelFile = new ArrayList<>();
				plcModelFile.add(modelFile);
				List<String> plcServerPath = new ArrayList<>();
				ReadAndUploadModels readModelsFromList = new ReadAndUploadModels(sendToServer);
				readModelsFromList.readAndUploadModels(plcModelFile,plcServerPath);
				pathToPLCOnServer = plcServerPath.get(0);
			}
		}
		ArrayList<ConveyorLengthJson> conveyorsLengthObejectList = new ArrayList<>();
		for(Entry<String,Double> conveyorEntry : cadNameToLengthMap.entrySet()) {
			ConveyorLengthJson conveyorLenthJsonObject = new ConveyorLengthJson();
			conveyorLenthJsonObject.setConveyorName(conveyorEntry.getKey());
			conveyorLenthJsonObject.setConveyorLength(conveyorEntry.getValue());
			conveyorLenthJsonObject.setUnit("mm");
			conveyorLenthJsonObject.setPathToPLCRDF(pathToPLCOnServer);
			conveyorsLengthObejectList.add(conveyorLenthJsonObject);
		}
		
		ConveyorLengthJson[] conveyorLengthJsonArray = new ConveyorLengthJson[conveyorsLengthObejectList.size()];
		conveyorsLengthObejectList.toArray(conveyorLengthJsonArray);
		
		ObjectMapper objectMapper = new ObjectMapper();

		
		String jsonString = null;
		try {
			jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(conveyorLengthJsonArray);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		List<ResultConveyorTravelTime> results = GenerateSimModelServerCommunication.sendConveyorLengthsAndGetTravelTime(jsonString);
		Map<String,Double> conveyorNameToTravelTimeMap = new HashMap<>();
		for(ResultConveyorTravelTime timeObject : results) {
			conveyorNameToTravelTimeMap.put(timeObject.getConveyorName(), timeObject.getConveyorTravelTime());
		}
		
		return conveyorNameToTravelTimeMap;
	}

	private void processGerationOfSimulationFile() {

		String sparqlQuery = giveAllBPMNConnectionsSPARQL();
		updateMessage("Reading BPMN Model");
		Model bpmnModel =null;
		for (ModelFile modelFile : modelFiles) {
			if (modelFile.isFileOfType(".bpmn")) {
				List<ModelFile> bpmnModelList = new ArrayList<>();
				bpmnModelList.add(modelFile);			
				ReadAndUploadModels readModelsFromList = new ReadAndUploadModels(null);
				readModelsFromList.readAndUploadModels(bpmnModelList,null);
				bpmnModel =  readModelsFromList.getClientModel();
			}
		}
		updateMessage("Execute BPMN Sparql");
		List<BPMNQueryResult> bpmnQueryResults = getBPMNSparqlResult(bpmnModel,sparqlQuery);
		updateMessage("Generation of Simulation Model");
		WriteJaamSimFile jaamSimFileWriter = new WriteJaamSimFile(bpmnQueryResults,directory.getAbsolutePath());
		jaamSimFileWriter.readElements();

		Map<String,Double> travelTimeForConveyors = getTravelTimeForConveyors(jaamSimFileWriter.getConveyorNames());
		
		jaamSimFileWriter.writeBPMNResultToJaamSim(travelTimeForConveyors);
		updateMessage("Simulation File generated");
	}

	public List<BPMNQueryResult> getBPMNSparqlResult(Model model, String queryString) {
		List<BPMNQueryResult> bpmnQueryResults = new ArrayList<>();
		Query query = QueryFactory.create(queryString) ;
		  try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
		    ResultSet results = qexec.execSelect() ;
		    List<QuerySolution> queries =  ResultSetFormatter.toList(results);
		    for(QuerySolution qr : queries) {
		    	
		    	BPMNQueryResult resultObject = new BPMNQueryResult();
		    	resultObject.setPredecessorName(qr.getLiteral("predecessorName").toString());
		    	String[] predesssorResourceSplit = qr.getResource("predecessorBPMNItemTyp").toString().split("/");
		    	resultObject.setPredecessorTyp(BPMNNodeTyp.valueOf(predesssorResourceSplit[predesssorResourceSplit.length-1]));
		    	resultObject.setSuccessorName(qr.getLiteral("successorName").toString());
		    	String[] successorResourceSplit = qr.getResource("successorBPMNItemTyp").toString().split("/");
		    	resultObject.setSuccessorTyp(BPMNNodeTyp.valueOf(successorResourceSplit[successorResourceSplit.length-1]));
		    	if(qr.getLiteral("flowItem")!=null) {
		    		resultObject.setFlowItem(qr.getLiteral("flowItem").toString());
		    	}
		    	bpmnQueryResults.add(resultObject);
		    }
		  }
		  catch (Exception e) {
			  e.printStackTrace();
		  }
		  return bpmnQueryResults;
	}

}
