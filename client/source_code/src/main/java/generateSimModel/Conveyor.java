package generateSimModel;

import java.util.ArrayList;
import java.util.List;

import bpmn.bpmnEntities.BPMNNodeTyp;

public class Conveyor {

	private String predecessorName;
	private String successorName;
	private BPMNNodeTyp predecessorTyp;
	public BPMNNodeTyp getPredecessorTyp() {
		return predecessorTyp;
	}
	public void setPredecessorTyp(BPMNNodeTyp predecessorTyp) {
		this.predecessorTyp = predecessorTyp;
	}
	public BPMNNodeTyp getSuccessorTyp() {
		return successorTyp;
	}
	public void setSuccessorTyp(BPMNNodeTyp successorTyp) {
		this.successorTyp = successorTyp;
	}

	private BPMNNodeTyp successorTyp;
	
	
	private List<String> flowItems = new ArrayList<>();
	public String getPredecessorName() {
		return predecessorName;
	}
	public void setPredecessorName(String predecessorName) {
		this.predecessorName = predecessorName;
	}
	public String getSuccessorName() {
		return successorName;
	}
	public void setSuccessorName(String successorName) {
		this.successorName = successorName;
	}
	public List<String> getFlowItems() {
		return flowItems;
	}
	public void setFlowItems(List<String> flowItems) {
		this.flowItems = flowItems;
	}
	
	public String getConveyorName() {
		return "conveyor_"+predecessorName+"_"+successorName;
	}
}
