package generateSimModel.serverCommunication;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GenerateSimModelServerCommunication {

	
	public static List<ResultConveyorTravelTime> sendConveyorLengthsAndGetTravelTime(String jsonString){
		List<ResultConveyorTravelTime> results = null;
		try {
			HttpRequest request = HttpRequest.newBuilder()
					  .uri(new URI("http://"+"127.0.0.1"+":"+"80"+"/getConveyorTravelTime"))
					  .header("Content-Type", "application/json;charset=UTF-8")
					  .POST(HttpRequest.BodyPublishers.ofString(jsonString))
					  .build();
			HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());
			ObjectMapper objectMapper = new ObjectMapper();
			results = objectMapper.readValue(response.body(), new TypeReference<List<ResultConveyorTravelTime>>() {});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}
}
