package generateSimModel.serverCommunication;

import java.io.Serializable;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "conveyorName", "conveyorTravelTime", "unit" })
@Generated("jsonschema2pojo")
public class ResultConveyorTravelTime implements Serializable {

	@JsonProperty("conveyorName")
	private String conveyorName;
	@JsonProperty("conveyorTravelTime")
	private Double conveyorTravelTime;
	@JsonProperty("unit")
	private String unit;
	private final static long serialVersionUID = 841623550512907598L;

	@JsonProperty("conveyorName")
	public String getConveyorName() {
		return conveyorName;
	}

	@JsonProperty("conveyorName")
	public void setConveyorName(String conveyorName) {
		this.conveyorName = conveyorName;
	}

	@JsonProperty("conveyorTravelTime")
	public Double getConveyorTravelTime() {
		return conveyorTravelTime;
	}

	@JsonProperty("conveyorTravelTime")
	public void setConveyorTravelTime(Double conveyorTravelTime) {
		this.conveyorTravelTime = conveyorTravelTime;
	}

	@JsonProperty("unit")
	public String getUnit() {
		return unit;
	}

	@JsonProperty("unit")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(ResultConveyorTravelTime.class.getName()).append('@')
				.append(Integer.toHexString(System.identityHashCode(this))).append('[');
		sb.append("conveyorName");
		sb.append('=');
		sb.append(((this.conveyorName == null) ? "<null>" : this.conveyorName));
		sb.append(',');
		sb.append("conveyorTravelTime");
		sb.append('=');
		sb.append(((this.conveyorTravelTime == null) ? "<null>" : this.conveyorTravelTime));
		sb.append(',');
		sb.append("unit");
		sb.append('=');
		sb.append(((this.unit == null) ? "<null>" : this.unit));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = ((result * 31) + ((this.conveyorName == null) ? 0 : this.conveyorName.hashCode()));
		result = ((result * 31) + ((this.unit == null) ? 0 : this.unit.hashCode()));
		result = ((result * 31) + ((this.conveyorTravelTime == null) ? 0 : this.conveyorTravelTime.hashCode()));
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ResultConveyorTravelTime) == false) {
			return false;
		}
		ResultConveyorTravelTime rhs = ((ResultConveyorTravelTime) other);
		return ((((this.conveyorName == rhs.conveyorName)
				|| ((this.conveyorName != null) && this.conveyorName.equals(rhs.conveyorName)))
				&& ((this.unit == rhs.unit) || ((this.unit != null) && this.unit.equals(rhs.unit))))
				&& ((this.conveyorTravelTime == rhs.conveyorTravelTime) || ((this.conveyorTravelTime != null)
						&& this.conveyorTravelTime.equals(rhs.conveyorTravelTime))));
	}

}