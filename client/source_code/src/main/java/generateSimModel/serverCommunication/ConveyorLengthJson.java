package generateSimModel.serverCommunication;

public class ConveyorLengthJson {

	private String conveyorName;
	private Double conveyorLength;
	private String unit;
	private String pathToPLCRDF;
	
	public String getConveyorName() {
		return conveyorName;
	}
	public void setConveyorName(String conveyorName) {
		this.conveyorName = conveyorName;
	}
	public Double getConveyorLength() {
		return conveyorLength;
	}
	public void setConveyorLength(Double conveyorLength) {
		this.conveyorLength = conveyorLength;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getPathToPLCRDF() {
		return pathToPLCRDF;
	}
	public void setPathToPLCRDF(String pathToPLCRDF) {
		this.pathToPLCRDF = pathToPLCRDF;
	}
}
