package generateSimModel;

import bpmn.bpmnEntities.BPMNNodeTyp;

public class BPMNQueryResult {
	private String predecessorName;
	private BPMNNodeTyp predecessorTyp;
	private String successorName;
	private BPMNNodeTyp successorTyp;
	private String flowItem;
	public String getFlowItem() {
		return flowItem;
	}
	public void setFlowItem(String flowItem) {
		this.flowItem = flowItem;
	}
	public String getPredecessorName() {
		return predecessorName;
	}
	public void setPredecessorName(String predecessorName) {
		this.predecessorName = predecessorName;
	}
	public BPMNNodeTyp getPredecessorTyp() {
		return predecessorTyp;
	}
	public void setPredecessorTyp(BPMNNodeTyp predecessorTyp) {
		this.predecessorTyp = predecessorTyp;
	}
	public String getSuccessorName() {
		return successorName;
	}
	public void setSuccessorName(String successorName) {
		this.successorName = successorName;
	}
	public BPMNNodeTyp getSuccessorTyp() {
		return successorTyp;
	}
	public void setSuccessorTyp(BPMNNodeTyp successorTyp) {
		this.successorTyp = successorTyp;
	}
}
