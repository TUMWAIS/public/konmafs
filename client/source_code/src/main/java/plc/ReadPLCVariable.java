package plc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entities.ModelFile;

public class ReadPLCVariable {
	
	private ModelFile plcFile;
	private List<ConveyorProperies> conveyorPropertyList = new ArrayList<>();
	
	public List<ConveyorProperies> getConveyorPropertyList() {
		return conveyorPropertyList;
	}

	public final String TRANSPORT_DIRECTION_BELT_CONVEYOR_VAR_NAME = "C_move";
	public final String TRANSPORT_ACCELERATION_BELT_CONVEYOR_VAR_NAME = "Acc_soll";
	public final String TRANSPORT_SPEED_BELT_CONVEYOR_VAR_NAME = "Speed_soll";
	public final String CONDITION_BELT_CONVEYOR_DIRECTION = "IF";
	public final String PLC_VAR_CHECK_SYMBOL = "=";
	
	Pattern p = Pattern.compile("-?\\d+");
	
	
	public ReadPLCVariable(ModelFile plcFile){
		this.plcFile = plcFile;
	}
	
	public void readPLCFileForConveyorProperties() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(plcFile.getFile()));
			String currentLine = reader.readLine();
		     while(currentLine!= null) {
		    	 if(currentLine.contains(TRANSPORT_DIRECTION_BELT_CONVEYOR_VAR_NAME)) {
		    		 if(currentLine.contains(CONDITION_BELT_CONVEYOR_DIRECTION)) {		 
		    			 
		    			 ConveyorProperies conveyorProperies = new ConveyorProperies();
		    			 conveyorProperies.setC_move(getValueAftereEqualSymbol(currentLine));
		    			 
		    			 currentLine = reader.readLine();
		    		     while(currentLine!= null) {
		    		    	 if(currentLine.contains(TRANSPORT_ACCELERATION_BELT_CONVEYOR_VAR_NAME)) {		    		    		 
		    		    		 conveyorProperies.setAcc_soll(getValueAftereEqualSymbol(currentLine));
		    		    	 } else if(currentLine.contains(TRANSPORT_SPEED_BELT_CONVEYOR_VAR_NAME)) {
		    		    		 conveyorProperies.setSpeed_soll(getValueAftereEqualSymbol(currentLine));
		    		    	 }
		    		    	 
		    		    	 if(conveyorProperies.getAcc_soll()!=null && conveyorProperies.getSpeed_soll()!=null) {
		    		    		 conveyorPropertyList.add(conveyorProperies);
		    		    		 break;
		    		    	 }
		    		    	 currentLine = reader.readLine();
		    		     }
		    		 }
		    	 }
		    	 currentLine = reader.readLine();
		     }
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Integer getValueAftereEqualSymbol(String plcCodeLine) {
		String stringAfterEqualSymbol = plcCodeLine.split(PLC_VAR_CHECK_SYMBOL)[1];
		 Matcher matcher = p.matcher(stringAfterEqualSymbol.trim());
		 if(matcher.find()) {
			 return Integer.valueOf(matcher.group());
		 }
		 return Integer.valueOf(0);	 
	}
	
}
