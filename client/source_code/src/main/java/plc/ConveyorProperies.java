package plc;

public class ConveyorProperies {

	Integer c_move;
	Integer acc_soll;
	Integer speed_soll;

	public ConveyorProperies() {
	}
	
	public ConveyorProperies(Integer c_move, Integer acc_soll, Integer speed_soll) {
		this.c_move = c_move;
		this.acc_soll = acc_soll;
		this.speed_soll = speed_soll;
	}
	
	public Integer getC_move() {
		return c_move;
	}

	public void setC_move(Integer c_move) {
		this.c_move = c_move;
	}

	public Integer getAcc_soll() {
		return acc_soll;
	}

	public void setAcc_soll(Integer acc_soll) {
		this.acc_soll = acc_soll;
	}

	public Integer getSpeed_soll() {
		return speed_soll;
	}

	public void setSpeed_soll(Integer speed_soll) {
		this.speed_soll = speed_soll;
	}

}
