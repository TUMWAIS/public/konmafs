package plc;

import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import rdf.RDFBaseModel;

public class PLCVariableRDFWriter {
	
	public static final String BASE_URI = "http://example.org";
	public static final String PLC_NS = BASE_URI + "/"+ "plc";	
	public static final String PLC_VARIABLE_PROP_NS = PLC_NS+"/"+"plcVariable#";
	public static final String PLC_VARIABLE_COLLECTION_NS = PLC_NS+"/"+"plcVariable";
	
	public static final String PLC_VARIABLE_PROPERTY_C_MOVE = PLC_VARIABLE_PROP_NS+ "C_move";
	public static final String PLC_VARIABLE_PROPERTY_ACC_SOLL = PLC_VARIABLE_PROP_NS+ "Acc_soll";
	public static final String PLC_VARIABLE_PROPERTY_SPEED_SOLL = PLC_VARIABLE_PROP_NS+ "Speed_soll";
	
	Property plcVariablePropertyC_Move;
	Property plcVariablePropertyAcc_Soll;
	Property plcVariablePropertySpeed_Soll;
	
	public Model model;
	private static int plcVariableConveyorPropertiesObject=1;
	
	public static void resetCounter() {
		plcVariableConveyorPropertiesObject = 1;
	}
	
	public PLCVariableRDFWriter(RDFBaseModel baseModel) {
		this.model = baseModel.getModel();
		init();
	}
	
	private void init() {
		model.setNsPrefix( "plcVar", PLC_VARIABLE_PROP_NS );
		plcVariablePropertyC_Move = model.createProperty( PLC_VARIABLE_PROPERTY_C_MOVE );
		plcVariablePropertyAcc_Soll = model.createProperty( PLC_VARIABLE_PROPERTY_ACC_SOLL );
		plcVariablePropertySpeed_Soll = model.createProperty( PLC_VARIABLE_PROPERTY_SPEED_SOLL );
	}
	
	public void writePLCVariableOfConvyeorPropertiesToRDFModel(List<ConveyorProperies> conveyorProperiesList) {
		for(ConveyorProperies item : conveyorProperiesList) {
			Resource plcConveyorVariablesSubject = model.createResource(PLC_VARIABLE_COLLECTION_NS+"/"+"plcVariable_"+plcVariableConveyorPropertiesObject++);
			model.add(plcConveyorVariablesSubject,plcVariablePropertyC_Move,model.createTypedLiteral(item.getC_move()));
			model.add(plcConveyorVariablesSubject,plcVariablePropertyAcc_Soll,model.createTypedLiteral(item.getAcc_soll()));
			model.add(plcConveyorVariablesSubject,plcVariablePropertySpeed_Soll,model.createTypedLiteral(item.getSpeed_soll()));
		}
		
	}
}
