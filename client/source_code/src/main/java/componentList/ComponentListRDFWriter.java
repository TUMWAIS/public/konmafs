package componentList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import rdf.RDFBaseModel;

public class ComponentListRDFWriter {
	public static final String BASE_URI = "http://example.org";
	public static final String COMPONENT_LIST_NS = BASE_URI+"/"+"componentList";
	public static final String COMPONENT_LIST_PROPERTY_NS = COMPONENT_LIST_NS+"/"+"componentListItem#";
	public static final String COMPONENT_LIST_COLLECTION_NS = COMPONENT_LIST_NS+"/"+"componentListItem";
	public static final String COMPONENT_LIST_PROPERTY_PLACE = COMPONENT_LIST_PROPERTY_NS+"place";
	public static final String COMPONENT_LIST_PROPERTY_COMPONENT_NAME = COMPONENT_LIST_PROPERTY_NS+"componentName";
	public static final String COMPONENT_LIST_PROPERTY_COMPONENT_LABEL = COMPONENT_LIST_PROPERTY_NS+"componentLabel";
	public static final String COMPONENT_LIST_PROPERTY_QUANTITY = COMPONENT_LIST_PROPERTY_NS+"quantity";
	public static final String COMPONENT_LIST_PROPERTY_TYP = COMPONENT_LIST_PROPERTY_NS+"typ";
	
	
	Property componentListPropertyPlace;
	Property componentListPropertyComponentName;
	Property componentListPropertyComponentLabel;
	Property componentListPropertyQuantity;
	Property componentListPropertyTyp;
	
	public Model model;
	
	public static void resetCounter() {
		componentListItemCounter = 1;
	}
	
	private static int componentListItemCounter=1;
	
	public ComponentListRDFWriter(RDFBaseModel baseModel) {
		this.model = baseModel.getModel();
		init();
	}
	
	private void init() {
		model.setNsPrefix( "componentListItem", COMPONENT_LIST_PROPERTY_NS );
		
		componentListPropertyPlace = model.createProperty(COMPONENT_LIST_PROPERTY_PLACE);
		componentListPropertyComponentName = model.createProperty(COMPONENT_LIST_PROPERTY_COMPONENT_NAME);
		componentListPropertyComponentLabel = model.createProperty(COMPONENT_LIST_PROPERTY_COMPONENT_LABEL);
		componentListPropertyQuantity = model.createProperty(COMPONENT_LIST_PROPERTY_QUANTITY);
	}
	
	public void writeComponentListItemsToRDFModel(List<ComponentItem> componentListItems, File file) {
		
		for(ComponentItem componentItem : componentListItems) {
			try {
			Resource componentItemSubject = model.createResource(COMPONENT_LIST_COLLECTION_NS+"/"+"componentListItem_"+componentListItemCounter++);
			model.add(componentItemSubject,componentListPropertyPlace,componentItem.getPlace());
			model.add(componentItemSubject,componentListPropertyComponentName,componentItem.getComponentName());
			if(componentItem.getComponentLabel()!=null) {
				model.add(componentItemSubject,componentListPropertyComponentLabel,componentItem.getComponentLabel());
			}
			if(componentItem.getQuantity()!=null) {
				model.add(componentItemSubject,componentListPropertyQuantity,model.createTypedLiteral(componentItem.getQuantity()));
			}
			}		catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(file!=null) {
			try {
				FileOutputStream fop = new FileOutputStream(file);
				RDFDataMgr.write(fop, model, Lang.RDFXML);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
