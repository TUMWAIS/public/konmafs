package componentList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import entities.EntityFileList;

public class ComponentList extends EntityFileList<ComponentItem>{

	
	private String componenteNameList;
	private int namePosition;
	private int labelPosition;
	private int typPosition;
	private int quantityPosition;
	private int headerPostion;
	private HashMap<String,Integer> componentNameToRowNumber = new HashMap<>();
	
	private List<ComponentItem> componentItems = new ArrayList<>();
	
	public ComponentList(String componenteNameList) {
		this.componenteNameList = componenteNameList;
	}
	
	public String getComponenteNameList() {
		return componenteNameList;
	}
	
	public void setComponenteNameList(String componenteNameList) {
		this.componenteNameList = componenteNameList;
	}
	public int getNamePosition() {
		return namePosition;
	}
	public void setNamePosition(int namePosition) {
		this.namePosition = namePosition;
	}
	public int getLabelPosition() {
		return labelPosition;
	}
	public void setLabelPosition(int labelPosition) {
		this.labelPosition = labelPosition;
	}
	public int getHeaderPostion() {
		return headerPostion;
	}
	public void setHeaderPostion(int headerPostion) {
		this.headerPostion = headerPostion;
	}

	public int getTypPosition() {
		return typPosition;
	}

	public void setTypPosition(int typPosition) {
		this.typPosition = typPosition;
	}

	public int getQuantityPosition() {
		return quantityPosition;
	}

	public void setQuantityPosition(int quantityPosition) {
		this.quantityPosition = quantityPosition;
	}

	public List<ComponentItem> getComponents() {
		return componentItems;
	}
	public void addComponent(ComponentItem component) {
		componentItems.add(component);
	}

	@Override
	public List<ComponentItem> getShowableEntityList() {
		return componentItems;
	}
	
	public HashMap<String, Integer> getComponentNameToRowNumber() {
		return componentNameToRowNumber;
	}
}
