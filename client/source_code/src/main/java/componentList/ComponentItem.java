package componentList;

public class ComponentItem {

	
	private String place;
	private String componentName;
	private String componentLabel;
	private String componentTyp;
	private Integer quantity = null;
	private ComponentList componentList;
	
	public ComponentItem(ComponentList componentList) {
		this.componentList = componentList;
	}
	
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public String getComponentLabel() {
		return componentLabel;
	}
	public void setComponentLabel(String componentLabel) {
		this.componentLabel = componentLabel;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String toString() {
		return componentLabel+"("+componentName+")";
	}
	public ComponentList getComponentList() {
		return componentList;
	}

	public String getComponentTyp() {
		return componentTyp;
	}

	public void setComponentTyp(String componentTyp) {
		this.componentTyp = componentTyp;
	}
}
