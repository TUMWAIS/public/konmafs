package componentList;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import entities.ModelFile;


public class ReadComponentList {
	
	private static String  COLUMN_NAME_COMPONENT_NAME = "Components";
	private static String  COLUMN_NAME_LABEL = "Identification";
	private static String  COLUMN_NAME_QUANTITY = "Quantity";
	private ModelFile componentListModelFile;
	private ComponentList currentComponentList;

		
		public ReadComponentList(ModelFile componentListModelFile) {
			this.componentListModelFile = componentListModelFile;
			currentComponentList = new ComponentList(componentListModelFile.getFile().getPath());
		}
		
		public ComponentList getComponentList() {
			return currentComponentList;
		}
		
	
		public void readCompontenListFile() {
		File excelFile = componentListModelFile.getFile();
	    Workbook workbook = null;
		try {
			workbook = new XSSFWorkbook(excelFile);
		} catch (InvalidFormatException | IOException e) {
				e.printStackTrace();
				
		}
	     Sheet datatypeSheet = workbook.getSheetAt(0);
         Iterator<Row> iterator = datatypeSheet.iterator();

         while (iterator.hasNext()) {

             Row currentRow = iterator.next();
             Iterator<Cell> cellIterator = currentRow.iterator();
             
             while (cellIterator.hasNext()) {

                 Cell currentCell = cellIterator.next();
                 
                 
                 if (currentCell.getCellType() == CellType.STRING) {
                     if(currentCell.getStringCellValue().equals(COLUMN_NAME_COMPONENT_NAME)) {
                    	 currentComponentList.setNamePosition(currentCell.getColumnIndex());
                    	 currentComponentList.setHeaderPostion(currentCell.getRowIndex());
                     }
                     else  if(currentCell.getStringCellValue().equals(COLUMN_NAME_LABEL)) {
                    	 currentComponentList.setLabelPosition(currentCell.getColumnIndex());
                     }
                     else  if(currentCell.getStringCellValue().equals(COLUMN_NAME_QUANTITY)) {
                    	 currentComponentList.setQuantityPosition(currentCell.getColumnIndex());
                     }
                     
                 } 

             }
             if(currentComponentList.getLabelPosition()!=0) {
            	 break;
             }

         }
         DataFormatter formatter = new DataFormatter();
         while (iterator.hasNext()) {

             Row currentRow = iterator.next();
             ComponentItem componentItem = new ComponentItem(currentComponentList);
             
             if(currentRow.getCell(0)==null) {
            	 continue;
             }
             
             CellType cellTypePlace = currentRow.getCell(0).getCellType();
             if(cellTypePlace==CellType.BLANK) {
            	 continue;
             }
             if(cellTypePlace==CellType.NUMERIC) {            	 
            	 String valueAsString = formatter.formatCellValue(currentRow.getCell(0));
            	 componentItem.setPlace(valueAsString);
             }
             else {
            	 componentItem.setPlace(currentRow.getCell(0).getStringCellValue());
             }
 
             
             componentItem.setComponentName(currentRow.getCell(currentComponentList.getNamePosition()).getStringCellValue());
             currentComponentList.getComponentNameToRowNumber().put(componentItem.getComponentName(), currentRow.getRowNum());
             if(currentRow.getCell(currentComponentList.getLabelPosition())!=null) {
            	 componentItem.setComponentLabel(currentRow.getCell(currentComponentList.getLabelPosition()).getStringCellValue());
             }
             
             if((currentRow.getCell(currentComponentList.getQuantityPosition())!=null)) {
            	 try {
            		 String value = currentRow.getCell(currentComponentList.getQuantityPosition()).getStringCellValue();
            		 if(!value.equals("")) {
            			 componentItem.setQuantity(Double.valueOf(currentRow.getCell(currentComponentList.getQuantityPosition()).getNumericCellValue()).intValue());
            			 }
            		 }
            	 catch(Exception e) {
            		 componentItem.setQuantity(Double.valueOf(currentRow.getCell(currentComponentList.getQuantityPosition()).getNumericCellValue()).intValue());
            	 }
             }
             currentComponentList.addComponent(componentItem);
         }
         
         try {
				workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	public HashMap<String, Integer> getComponentNameToRowNumber() {
		return currentComponentList.getComponentNameToRowNumber();
	}
	
	public int getColumnNumberForQuantityPosition() {
		return currentComponentList.getQuantityPosition();
	}
}
