package gui;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Gui extends Application {
	
	public static final double WIDTH_MAIN_WINDOW = 1380;
	public static final double HEIGHT_MAIN_WINDOW = 249;
	
	public static void startGui(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("KonMaFs");

		final GridPane inputGridPane = new GridPane();
 
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        
 
        final ListView<String> listView = new ListView<>();
        ObservableList<String> items = FXCollections.observableArrayList (
        	    "Single", "Double", "Suite", "Family App");
        
        listView.setItems(items);
        listView.setPrefSize(100, 75);
        
        Label labelElement = new Label("Element");
        Label dynamicElement = new Label("");
 
        
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                dynamicElement.setText(newValue);
            }
        });

        
        inputGridPane.add(listView,0,0,1,3);
        inputGridPane.add(labelElement,1,0,1,1);
        inputGridPane.add(dynamicElement,2,0,1,1);
        
        GuiConsistencRules guiConsistencRules = new GuiConsistencRules(primaryStage);
        
        primaryStage.setScene(new Scene(guiConsistencRules.getConsistencRulesGui(),WIDTH_MAIN_WINDOW,HEIGHT_MAIN_WINDOW));
        
        if(Main.logo!=null) {
	        Image logo = new Image("file:"+Main.logo);
	        primaryStage.getIcons().add(logo);
        }
        
        primaryStage.show();
        
        if(!Main.toolModelsFiles.isEmpty()) {
        	guiConsistencRules.preLoadToolModels(Main.toolModelsFiles);
        }
	}	
	
}
