package gui;

import java.io.File;
import java.util.List;

import entities.ModelFile;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Window;

public class GuiElements {

	private GuiElements() {}
	
	public static Button getFileSelectButton(String buttonName,Window ownerWindow,ListView<ModelFile> fileList) {
		final Button selectFileButton = new Button(buttonName);
		
		final FileChooser fileChooser = new FileChooser();
	     
		selectFileButton.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                    List<File> list =
                        fileChooser.showOpenMultipleDialog(ownerWindow);
                    if (list != null) {
                        for (File file : list) {
                        	ModelFile mf = new ModelFile(file.getPath());
                        	fileList.getItems().add(mf);
                        	
                        }
                        fileList.refresh();
                    }
                }
            });
		return selectFileButton;
	}
	
	public static Button getDeleteButton(String buttonName,Window ownerWindow,ListView<ModelFile> fileList) {
		final Button deleteFileButton = new Button(buttonName);
		
		deleteFileButton.setOnAction(
	            new EventHandler<ActionEvent>() {
	                @Override
	                public void handle(final ActionEvent e) {
	            		ObservableList<ModelFile> obFileList = fileList.getItems();
	            		obFileList.clear();
	                }
	            });
		
		return deleteFileButton;
	}
	
	public static VBox getSpacingBox(int width) {
		VBox spacingBox = new VBox();
		spacingBox.setMinWidth(width);
		return spacingBox;
	}
	
	public static VBox getFileLoadingGui(String title,ListView<ModelFile> listView, Window ownerWindow ) {

		Label labelTitle = new Label(title);
		labelTitle.setStyle("-fx-font-weight: bold");

		listView.setPrefSize(200, 210);

		Button fileSelectButton = GuiElements.getFileSelectButton("Add tool file", ownerWindow, listView);

		Button fileDeleteButton = GuiElements.getDeleteButton("Delete list", ownerWindow, listView);
		
		HBox buttons = new HBox(fileSelectButton,fileDeleteButton);
		
		VBox fileLoadingBox = new VBox(labelTitle,listView,buttons);
		fileLoadingBox.setMinWidth(300);
		
		return fileLoadingBox;
	}	
}
