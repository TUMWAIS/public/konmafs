package gui;

import java.io.File;
import java.util.List;

import corectBom.ProcessCorectBomFile;
import entities.ModelFile;
import generateSimModel.ProcessGeneratingSimulationFile;
import generateViewModel.ProcessGenerateViewModelFile;
import generateViewModel.ProcessReadElementsFromToolFiles;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import restClient.ServerCommunication;
import resultProcessing.ProcessConsistencCheck;
import resultProcessing.ProcessUpdateCheck;
import resultProcessing.ResultObject;

public class GuiConsistencRules {
	
	public final static double RESULT_TABLE_WIDTH = 1044.0;
	
	GridPane consistenceRulesPane = null;
	Stage primaryStage;	
	ServerCommunication sendToServer = new ServerCommunication(Main.ip,Main.port);	
	ListView<ModelFile> fileViewToolModels;	
	ResultMode resultMode = ResultMode.NONE;
	
	Button readConsistenceRulesButton = null;
	Button createRDFModelFile = null;
	Button createSparqFile = null;
	Button checkConsistencyButton = null;
	Button generateSimulationFile = null;
	Button corectBomButton = null;
	Button generateViewButton=null;
	
	Button checkUpdateButton = null;
	Button checkConnectionButton = null;
	public Alert alert = new Alert(AlertType.INFORMATION);
	Alert connectionTest = new Alert(AlertType.INFORMATION);
	
	Label labelFoundInconsistencies  = new Label("");
	TableView<ObservableList<String>> inconsitencCheckResult;
	TableView<ResultObject> resultTableView;
	ObservableList<ObservableList<String>> queryData = FXCollections.observableArrayList();
	
	
	public GuiConsistencRules(Stage primaryStage) {
		this.primaryStage = primaryStage;
		initConsistencRules();
	}
	
	public void preLoadToolModels(List<File> toolModelFiles) {
		for(File file : toolModelFiles) {
			ModelFile mf = new ModelFile(file.getPath());
			fileViewToolModels.getItems().add(mf);
			fileViewToolModels.getSelectionModel().clearSelection();
			fileViewToolModels.setStyle("-fx-background-insets: 0 ;");
		}
	}
	
	private void checkConsistencyAction(GuiConsistencRules gui) {
		checkConsistencyButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(final ActionEvent e) {
				ProcessConsistencCheck checkTask = new ProcessConsistencCheck(fileViewToolModels.getItems(), sendToServer,gui);
				if(fileViewToolModels.getItems()==null && fileViewToolModels.getItems().size()==0) {
					return;
				}
				if(resultTableView!=null) {
					resultTableView.getItems().clear();
				}
				resultMode = ResultMode.INCONSISTENCIES;
		        alert = new Alert(AlertType.INFORMATION);   
		        alert.setHeaderText("Processing Consistency Check");
		        alert.getDialogPane().lookupButton(ButtonType.OK).setVisible(false);
		        alert.contentTextProperty().bind(checkTask.messageProperty());
		        alert.show();
		        
		        Thread resultProcessingThread = new Thread(checkTask);
		        resultProcessingThread.setDaemon(true);
		        resultProcessingThread.start();
		        
		        checkTask.setOnSucceeded(event -> {
		        	gui.showResults(checkTask.getResults());
			    	gui.alert.close();
			    	
			    	labelFoundInconsistencies.setText("Inconsistencies in models:");
			    	labelFoundInconsistencies.setStyle("-fx-font-weight: bold");
		        });
			}
		});
	}
	
	private void checkUpdateAction(GuiConsistencRules gui) {
		checkUpdateButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				resultMode = ResultMode.UPDATE;
				
				ProcessUpdateCheck checkUpdateTask = new ProcessUpdateCheck(fileViewToolModels.getItems(), sendToServer,gui);
				if(fileViewToolModels.getItems()==null && fileViewToolModels.getItems().size()==0) {
					return;
				}
				if(resultTableView!=null) {
					resultTableView.getItems().clear();
				}
				resultMode = ResultMode.UPDATE;
		        alert = new Alert(AlertType.INFORMATION);   
		        alert.setHeaderText("Processing update check");
		        alert.getDialogPane().lookupButton(ButtonType.OK).setVisible(false);
		        alert.contentTextProperty().bind(checkUpdateTask.messageProperty());
		        alert.show();
		        
		        Thread updateCheckThread = new Thread(checkUpdateTask);
		        updateCheckThread.setDaemon(true);
		        updateCheckThread.start();
		        
		        checkUpdateTask.setOnSucceeded(event -> {
		        	gui.showResults(checkUpdateTask.getResults());
			    	gui.alert.close();
			    	
					labelFoundInconsistencies.setText("To be checked model elements due to update:");
					labelFoundInconsistencies.setStyle("-fx-font-weight: bold");
		        });
	
			}
		});
	}
	
	private void generateSimulationAction(GuiConsistencRules gui) {
		generateSimulationFile.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				
				boolean bpmnFound = false;
				for(ModelFile modelFile : fileViewToolModels.getItems()) {
					if(modelFile.isFileOfType(".bpmn")) {
						bpmnFound = true;
					}
				}
				if(bpmnFound == false) {
					alert = new Alert(AlertType.INFORMATION);   
			        alert.setHeaderText("No BPMN File found");
			        alert.getDialogPane().lookupButton(ButtonType.OK).setVisible(false);
			        alert.show();
			        return;
				}
				else {
					alert = new Alert(AlertType.INFORMATION);   
			        alert.setHeaderText("Generating Simulation File");
			        alert.getDialogPane().lookupButton(ButtonType.OK).setVisible(false);
			        
				}
				File directoryToExport = null;
				if(Main.EXPORT_PATH!=null) {
					directoryToExport = Main.EXPORT_PATH;
				}else {
					DirectoryChooser directoryChooser = new DirectoryChooser();
					directoryChooser.setTitle("Choose directory to save gerated file");
					directoryToExport = directoryChooser.showDialog(primaryStage);
				}
				ProcessGeneratingSimulationFile generateSimulationTask = new ProcessGeneratingSimulationFile(fileViewToolModels.getItems(), sendToServer,gui,directoryToExport);
				alert.contentTextProperty().bind(generateSimulationTask.messageProperty());
		        alert.show();
		        
		        Thread generatingSimulationThread = new Thread(generateSimulationTask);
		        generatingSimulationThread.setDaemon(true);
		        generatingSimulationThread.start();
			}
		});
	}
	
	private void generateViewAction(GuiConsistencRules gui) {
		generateViewButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
							
				ProcessReadElementsFromToolFiles generateViewModelTask = new ProcessReadElementsFromToolFiles(fileViewToolModels.getItems(), gui);
				alert.contentTextProperty().bind(generateViewModelTask.messageProperty());
		        alert.show();
		        Thread generatingSimulationThread = new Thread(generateViewModelTask);
		        generatingSimulationThread.setDaemon(true);
		        generatingSimulationThread.start();
		        Stage selectElementsAndMainViewWindow = new Stage();
		        ListView<String> elementNameList = new ListView<>();
		        
		        generateViewModelTask.setOnSucceeded(event -> {
		        	alert.close();
		        	Button generateViewFileButton = new Button("Generate view for selected Elements");
					selectElementsAndMainViewWindow.setTitle("Select Elements");
					Label selectMainView = new Label("Select elements:");
					
					elementNameList.setPrefSize(250, 350);
					elementNameList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
					ObservableList<String> subentries = FXCollections.observableArrayList();
					subentries.addAll(generateViewModelTask.getElementNames());
					elementNameList.setItems(subentries);		
					elementNameList.getSelectionModel().selectAll();
					
					VBox container = new VBox(selectMainView,elementNameList,generateViewFileButton);
					selectElementsAndMainViewWindow.setScene(new Scene(container,350,400));
					selectElementsAndMainViewWindow.show();
					
					generateViewFileButton.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(final ActionEvent e) {
							List<String> selectedItems = elementNameList.getSelectionModel().getSelectedItems();
							
							File directoryToExport = null;
							if(Main.EXPORT_PATH!=null) {
								directoryToExport = Main.EXPORT_PATH;
							}else {
								DirectoryChooser directoryChooser = new DirectoryChooser();
								directoryChooser.setTitle("Choose directory to save gerated file");
								directoryToExport = directoryChooser.showDialog(primaryStage);
							}

							ProcessGenerateViewModelFile processGenerateViewModelFileTask = 
									new ProcessGenerateViewModelFile(selectedItems,generateViewModelTask.getModelWithData(), gui,directoryToExport);
							Thread processGenerateViewModelFileThread = new Thread(processGenerateViewModelFileTask);
							processGenerateViewModelFileThread.setDaemon(true);
							alert = new Alert(AlertType.INFORMATION);
							alert.contentTextProperty().bind(processGenerateViewModelFileTask.messageProperty());
					        alert.show();
					        selectElementsAndMainViewWindow.close();
							processGenerateViewModelFileThread.start();
						}
					});
		        });
		     }
		});
	}
	
	private void corectInconsistenciesAction(GuiConsistencRules gui) {
		corectBomButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				boolean excelFound = false;
				boolean cadFound = false;
				for(ModelFile modelFile : fileViewToolModels.getItems()) {
					if(modelFile.isFileOfType(".FCStd")) {
						cadFound = true;
					}
					if(modelFile.isFileOfType(".xlsx")) {
						excelFound = true;
					}
				}
				if((excelFound&&cadFound) == false) {
			        alert.setHeaderText("No Excel and Cad File found");
			        alert.getDialogPane().lookupButton(ButtonType.OK).setVisible(false);
			        alert.show();
			        return;
				}
				else {
					alert = new Alert(AlertType.INFORMATION);   
			        alert.setHeaderText("Correct bill of materials file");
			        alert.getDialogPane().lookupButton(ButtonType.OK).setVisible(false);
			        
				}
				
				ProcessCorectBomFile corectBomFileTask = new ProcessCorectBomFile(fileViewToolModels.getItems());
				alert = new Alert(AlertType.INFORMATION);
				alert.contentTextProperty().bind(corectBomFileTask.messageProperty());
		        alert.show();
		        
		        Thread corectBomFileThread = new Thread(corectBomFileTask);
		        corectBomFileThread.setDaemon(true);
		        corectBomFileThread.start();
			}
		});		
	}
	
	private void testNetworkConnectionAction(GuiConsistencRules gui) {
		checkConnectionButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(final ActionEvent e) {
				if(sendToServer.checkConnection()) {
					connectionTest.setContentText("Connection OK");
				}
				else {
					connectionTest.setContentText("No connection");
				}
				connectionTest.show();
			}
		});		
	}

	public void initConsistencRules() {
		
		fileViewToolModels = new ListView<>();
		fileViewToolModels.setPlaceholder(new Label(" "));
		
		consistenceRulesPane = new GridPane();
		
		consistenceRulesPane.add(GuiElements.getFileLoadingGui("Tool models",fileViewToolModels,primaryStage),0,0,1,4);
		consistenceRulesPane.add(GuiElements.getSpacingBox(5), 2, 1, 1, 1);
		GuiConsistencRules gui = this;

		checkConsistencyButton = new Button("Check Consistency");
		checkConsistencyAction(gui);

		
		checkUpdateButton = new Button("Check update");
		checkUpdateAction(gui);
		
		generateSimulationFile = new Button("Generate simulation");
		generateSimulationAction(gui);
		
		generateViewButton = new Button("Generate view");
		generateViewAction(gui);
		
		corectBomButton = new Button("Correct inconsistencies");
		corectInconsistenciesAction(gui);
				
		checkConnectionButton = new Button("Test network connection");
		testNetworkConnectionAction(gui);		
		
		labelFoundInconsistencies.setStyle("-fx-font-weight: bold");
		consistenceRulesPane.add(labelFoundInconsistencies, 5, 0, 1, 1);
		
		inconsitencCheckResult = new TableView<>();
		inconsitencCheckResult.setMinWidth(RESULT_TABLE_WIDTH);
		inconsitencCheckResult.setPlaceholder(new Label(" "));

		consistenceRulesPane.add(inconsitencCheckResult, 5, 1, 1, 1);
		HBox buttonBox = new HBox(checkConsistencyButton,checkUpdateButton,corectBomButton,generateSimulationFile,generateViewButton,checkConnectionButton);
		consistenceRulesPane.add(buttonBox, 5, 2, 1, 1);
	}

	public Pane getConsistencRulesGui() {
		VBox outerBox = new VBox(consistenceRulesPane);
		outerBox.setPadding(new Insets(10, 12, 12, 12));
		return outerBox;
	}
	
	public void showResults(List<ResultObject> results) {
		try {
				resultTableView = new TableView<>();
				TableColumn<ResultObject,String> columnModels = new TableColumn<>("models");

				TableColumn<ResultObject,String> subColumnModelElementView = new TableColumn<>("view");
				TableColumn<ResultObject,String> subColumnModelElementName = new TableColumn<>("element name");
				TableColumn<ResultObject,String> subColumnModelElementProperty = new TableColumn<>("property");
				TableColumn<ResultObject,String> subColumnModelElementValue = null;
				TableColumn<ResultObject,String> subColumnModelElementNewValue = null;
				TableColumn<ResultObject,String> columnMessage = new TableColumn<>("message");
				columnModels.getColumns().clear();
				columnModels.getColumns().add(subColumnModelElementView);
				columnModels.getColumns().add(subColumnModelElementName);
				columnModels.getColumns().add(subColumnModelElementProperty);
				
				if(resultMode == ResultMode.INCONSISTENCIES) {
					subColumnModelElementValue = new TableColumn<>("value");
					columnModels.getColumns().add(subColumnModelElementValue);
					
				}
				else if(resultMode == ResultMode.UPDATE) {
					subColumnModelElementValue = new TableColumn<>("old value");
					columnModels.getColumns().add(subColumnModelElementValue);
					subColumnModelElementNewValue = new TableColumn<>("new value");
					columnModels.getColumns().add(subColumnModelElementNewValue);
				}
				resultTableView.getColumns().add(columnModels);
				resultTableView.getColumns().add(columnMessage);
				
				subColumnModelElementView.setCellValueFactory(new Callback<CellDataFeatures<ResultObject, String>, ObservableValue<String>>() {
				     public ObservableValue<String> call(CellDataFeatures<ResultObject, String> p) {
				    	 StringBuilder str = new StringBuilder();
				    	 for(resultProcessing.Model m : p.getValue().getModels()) {

				    		 str.append(m.getView());

				    		 str.append("\n");
				    	 }
				    	 return new ReadOnlyObjectWrapper<String>(str.toString());
				     }
				  });
				
				subColumnModelElementName.setCellValueFactory(new Callback<CellDataFeatures<ResultObject, String>, ObservableValue<String>>() {
				     public ObservableValue<String> call(CellDataFeatures<ResultObject, String> p) {
				    	 StringBuilder str = new StringBuilder();
				    	 for(resultProcessing.Model m : p.getValue().getModels()) {
				    		 str.append(m.getElementName());
				    		 str.append("\n");
				    	 }
				    	 return new ReadOnlyObjectWrapper<String>(str.toString());
				     }
				  });
				
				subColumnModelElementProperty.setCellValueFactory(new Callback<CellDataFeatures<ResultObject, String>, ObservableValue<String>>() {
				     public ObservableValue<String> call(CellDataFeatures<ResultObject, String> p) {
				    	 StringBuilder str = new StringBuilder();
				    	 for(resultProcessing.Model m : p.getValue().getModels()) {

				    		 str.append(m.getProperty());
				    		 str.append("\n");
				    	 }
				    	 return new ReadOnlyObjectWrapper<String>(str.toString());
				     }
				  });
				
				subColumnModelElementValue.setCellValueFactory(new Callback<CellDataFeatures<ResultObject, String>, ObservableValue<String>>() {
				     public ObservableValue<String> call(CellDataFeatures<ResultObject, String> p) {
				    	 
				    	 StringBuilder str = new StringBuilder();
				    	 for(resultProcessing.Model m : p.getValue().getModels()) {
				    		 
				    		 str.append(m.getOldValue());
				    		 str.append("\n");
				    	 }
				    	 return new ReadOnlyObjectWrapper<String>(str.toString());
				     }
				  });
				
				if(resultMode==ResultMode.UPDATE) {
					subColumnModelElementNewValue.setCellValueFactory(new Callback<CellDataFeatures<ResultObject, String>, ObservableValue<String>>() {
					     public ObservableValue<String> call(CellDataFeatures<ResultObject, String> p) {
					    	 
					    	 StringBuilder str = new StringBuilder();
					    	 for(resultProcessing.Model m : p.getValue().getModels()) {
					    		 
					    		 str.append(m.getNewValue());
					    		 str.append("\n");
					    	 }
					    	 return new ReadOnlyObjectWrapper<String>(str.toString());
					     }
					  });
				}
				
				columnMessage.setCellValueFactory(new Callback<CellDataFeatures<ResultObject, String>, ObservableValue<String>>() {
				     public ObservableValue<String> call(CellDataFeatures<ResultObject, String> p) {
		    	 
				         return new ReadOnlyObjectWrapper<String>(p.getValue().getMessage());
				     }
				  });
				for(ResultObject resultObject : results) {
					resultTableView.getItems().add(resultObject);
				}

			consistenceRulesPane.add(resultTableView, 5, 1, 1, 1);
			
		}
		catch(Exception expResult) {
			expResult.printStackTrace();
		}
		finally {
			checkConsistencyButton.setDisable(false);
		}
	}
}
