package gui;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static List<File> toolModelsFiles = new ArrayList<>();
	public static File EXPORT_PATH = null;
	public static int port = 80;
	public static String ip = "127.0.0.1";
	public static String logo = null;
	public static String resources = null;
	
	public static void main(String[] args) throws IllegalArgumentException, IOException {
    	Main main = new Main();
    	main.init(args);
    	Gui.startGui(args);
    }

	public void init(String[] args) {
		
		for(int i=0;i<args.length;) {
			String startArgummentType = args[i];

			if(startArgummentType.contains("-toolModel")) {
				toolModelsFiles.add(new File(args[i+1]));
			}
			else if(startArgummentType.contains("-port")) {
				port = Integer.parseInt(args[i+1]);
			}
			else if(startArgummentType.contains("-ip")) {
				ip = args[i+1];
			}
			else if(startArgummentType.contains("-resources")) {
				String resourcesFilePath = args[i+1];
				resources = resourcesFilePath;
				logo = resourcesFilePath+"\\KonMaFS_Logo.png";
			}
			else if(startArgummentType.contains("-exportPath")) {
				EXPORT_PATH = new File(args[i+1]);
			}			
			i=i+2;
		}
	}    
}