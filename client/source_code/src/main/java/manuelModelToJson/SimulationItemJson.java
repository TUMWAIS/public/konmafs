package manuelModelToJson;

public class SimulationItemJson {

	private String[] elementNames;
	private String typ;
	private String[] nextSimulationItemNames;
	private String propertyName;
	
	public String[] getElementNames() {
		return elementNames;
	}
	public void setElementNames(String[] elementNames) {
		this.elementNames = elementNames;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String[] getNextSimulationItemNames() {
		return nextSimulationItemNames;
	}
	public void setNextSimulationItemNames(String[] nextSimulationItemNames) {
		this.nextSimulationItemNames = nextSimulationItemNames;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}	
}
