package manuelModelToJson;

public class BpmnItemJson {
	private String[] elementNames;
	private String itemTyp;
	private String[] incommingFlowItemNames;
	private String[] outgoingFlowItemNames;
	
	public String[] getElementNames() {
		return elementNames;
	}
	public void setElementNames(String[] elementNames) {
		this.elementNames = elementNames;
	}
	public String getItemTyp() {
		return itemTyp;
	}
	public void setItemTyp(String itemTyp) {
		this.itemTyp = itemTyp;
	}
	public String[] getIncommingFlowItemNames() {
		return incommingFlowItemNames;
	}
	public void setIncommingFlowItemNames(String[] incommingFlowItemNames) {
		this.incommingFlowItemNames = incommingFlowItemNames;
	}
	public String[] getOutgoingFlowItemNames() {
		return outgoingFlowItemNames;
	}
	public void setOutgoingFlowItemNames(String[] outgoingFlowItemNames) {
		this.outgoingFlowItemNames = outgoingFlowItemNames;
	}
	
}
