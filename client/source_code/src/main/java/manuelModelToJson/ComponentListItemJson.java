package manuelModelToJson;

public class ComponentListItemJson {
	private String[] elementNames;
	private String propertyName;
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String[] getElementNames() {
		return elementNames;
	}
	public void setElementNames(String[] elementNames) {
		this.elementNames = elementNames;
	}
}
