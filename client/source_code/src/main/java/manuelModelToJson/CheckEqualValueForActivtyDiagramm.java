package manuelModelToJson;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;

import emf.EmfFile;
import emf.EmfNode;
import emf.TagValue;

public class CheckEqualValueForActivtyDiagramm {

	EmfFile emfFile;
	Model rdfModel;
	List<EmfNode> testIdentityActionNodes;
	public static final String EQUALS_PROPERTY_NAME = "EigenschaftName";
	public static final String EQUALS_PROPERTY_EQUAL_TYPE = "Vergleich";
	public static final String WHITESPACE =" \n";
	int cadItemCounter =1;
	int bpmnNodeItemCounter =1;
	int bpmnFlowItemCounter =1;
	public List<String> sparqlQueries = new ArrayList<>();

	
	public CheckEqualValueForActivtyDiagramm(Model rdfModel,EmfFile emfFile ) {
		this.rdfModel = rdfModel;
		this.emfFile = emfFile;
	}
	
	public ComparisonContainer readAppliedValuesIntoJson() {
		ComparisonContainer compContainer = new ComparisonContainer();
		for(EmfNode node :emfFile.allNodes) {
			if(node.type.equals("uml:TestIdentityAction")) {				
				EmfNode first = emfFile.allNodes.stream().filter(n->n.exceptionInput.contains(node.first)).findFirst().get();
				EmfNode second = emfFile.allNodes.stream().filter(n->n.exceptionInput.contains(node.second)).findFirst().get();
				EqualPropertyCheckJson checkJson = new EqualPropertyCheckJson();
				compContainer.setEqualPropertyCheckJson(checkJson);
				checkJson.setFileName(emfFile.name);
				String firstView = first.appliedProfils.get(0).split(":")[1];
				String secondView = second.appliedProfils.get(0).split(":")[1];
				EmfNode[] appliedViews = new EmfNode[2];
				appliedViews[0] = first;
				appliedViews[1] = second;
				checkJson.setFirstViewElemet(firstView);
				checkJson.setSecondViewElement(secondView);
				for(TagValue item : node.appliedValues) {
					if(item.tag.equals("comparison")) {
						checkJson.setComparison(getCompareTypeInLogicRepresentation(item.value));
					}
					if(item.tag.equals("severtiy")) {
						checkJson.setSeverity(item.value);
					}
					if(item.tag.equals("message")) {
						checkJson.setMessage(item.value);
					}
				}
				for(EmfNode view : appliedViews) {
					if(view.appliedProfils.get(0).split(":")[1].equals("simulationReport")) {
						compContainer.setSimulationReport(getSimulationReport(view.appliedValues));
					}
					if(view.appliedProfils.get(0).split(":")[1].equals("requirementListItem")) {
						compContainer.setRequirementListItem(getRequirementListItem(view.appliedValues));
					}
					if(view.appliedProfils.get(0).split(":")[1].equals("simulationItem")) {
						compContainer.setSimulationItem(getSimulationItemJson(view.appliedValues));
					}
					if(view.appliedProfils.get(0).split(":")[1].equals("cadItem")) {
						compContainer.setCadItem(getCadItemJson(view.appliedValues));
					}
					if(view.appliedProfils.get(0).split(":")[1].equals("componentListItem")) {
						compContainer.setComponentListItem(getComponentListItemJson(view.appliedValues));
					}
					if(view.appliedProfils.get(0).split(":")[1].equals("bpmnItem")) {
						compContainer.setBpmnItem(getBpmnItemJson(view.appliedValues));
					}
				}	
			}
		}
		return compContainer;
	}
	
	private ComponentListItemJson getComponentListItemJson(List<TagValue> appliedValues) {
		ComponentListItemJson componentListItemJson = new ComponentListItemJson();
		for(TagValue item: appliedValues) {
			if(item.tag.equals("elementNames")) {
				componentListItemJson.setElementNames(item.value.split(","));
			}
			if(item.tag.equals("property")) {
				componentListItemJson.setPropertyName(item.value);
			}
		}
		return componentListItemJson;
	}
	
	private BpmnItemJson getBpmnItemJson(List<TagValue> appliedValues) {
		BpmnItemJson bpmnItemJson = new BpmnItemJson();
		for(TagValue item: appliedValues) {
			if(item.tag.equals("elementNames")) {
				bpmnItemJson.setElementNames(item.value.split(","));
			}
			if(item.tag.equals("incomingNodeNames")) {
				bpmnItemJson.setIncommingFlowItemNames(item.value.split(","));
			}
			if(item.tag.equals("outgoingNodeNames")) {
				bpmnItemJson.setOutgoingFlowItemNames(item.value.split(","));
			}
			if(item.tag.equals("itemTyp")) {
				bpmnItemJson.setItemTyp(item.value);
			}
		}		
		return bpmnItemJson;
	}
	
	private CadItemJson getCadItemJson(List<TagValue> appliedValues) {
		CadItemJson cadItemJson = new CadItemJson();
		for(TagValue item: appliedValues) {
			if(item.tag.equals("elementNames")) {
				cadItemJson.setElementNames(item.value.split(","));
			}
			if(item.tag.equals("property")) {
				cadItemJson.setPropertyName(item.value);
			}
		}		
		return cadItemJson;
	}
	
	private SimulationItemJson getSimulationItemJson(List<TagValue> appliedValues) {
		SimulationItemJson simulationItemJson = new SimulationItemJson();
		for(TagValue item: appliedValues) {
			if(item.tag.equals("elementNames")) {
				simulationItemJson.setElementNames(item.value.split(","));
			}
			if(item.tag.equals("property")) {
				simulationItemJson.setPropertyName(item.value);
			}
			if(item.tag.equals("nextSimulationItemNames")) {
				simulationItemJson.setNextSimulationItemNames(item.value.split(","));
			}
			if(item.tag.equals("simulationItemTyp")) {
				if(!item.value.equals("noType")) {
					simulationItemJson.setTyp(item.value);
				}
			}
		}
		return simulationItemJson;
	}
	
	private RequirementListItemJson getRequirementListItem(List<TagValue> appliedValues) {
		RequirementListItemJson requirementListItem = new RequirementListItemJson();
		for(TagValue item: appliedValues) {
			if(item.tag.equals("property")) {
				requirementListItem.setPropertyName(item.value);
			}
			if(item.tag.equals("requirement")) {
				requirementListItem.setRequirement(item.value);
			}
		}
		return requirementListItem;
	}
	
	
	private SimulationReportJson getSimulationReport(List<TagValue> appliedValues) {
		SimulationReportJson simReport = new SimulationReportJson();
		for(TagValue item: appliedValues) {
			if(item.tag.equals("elementNames")) {
				simReport.setElementNames(item.value.split(","));
			}
			if(item.tag.equals("property")) {
				simReport.setPropertyName(item.value);
			}
			if(item.tag.equals("customPropertyName")) {
				simReport.setCustomPropertyName(item.value);
			}
			if(item.tag.equals("simulationItemTyp")) {
				if(!item.value.equals("noType")) {
					simReport.setSimulationItemTyp(item.value);
				}
			}
		}
		return simReport;
	}
	
	public static String getCompareTypeInLogicRepresentation(String compareTypeAsName) {
		String logicRepresentation = compareTypeAsName;
		switch(compareTypeAsName) {
		case "nichtGleich" : logicRepresentation = "!=";break;
		case "kleinerGleich": logicRepresentation = "<=";break;
		case "kleiner": logicRepresentation = "<";break;
		case "gleich": logicRepresentation = "=";break;
		case "größerGleich": logicRepresentation = "=>";break;
		case "größer": logicRepresentation = ">";break;
		}
		return logicRepresentation;
	}
}
