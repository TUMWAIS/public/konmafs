package manuelModelToJson;

public class EqualPropertyCheckJson {
	
	private String fileName;
	private String comparison;
	private String severity;
	private String message;	
	private String firstViewElemet;
	private String secondViewElement;

	public String getComparison() {
		return comparison;
	}
	public void setComparison(String comparison) {
		this.comparison = comparison;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getSecondViewElement() {
		return secondViewElement;
	}
	public void setSecondViewElement(String secondViewElement) {
		this.secondViewElement = secondViewElement;
	}
	public String getFirstViewElemet() {
		return firstViewElemet;
	}
	public void setFirstViewElemet(String firstViewElemet) {
		this.firstViewElemet = firstViewElemet;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
