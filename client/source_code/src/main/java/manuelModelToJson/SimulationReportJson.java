package manuelModelToJson;

public class SimulationReportJson {
	private String[] elementNames;
	private String propertyName;
	private String customPropertyName;
	private String simulationItemTyp;
	
	public String[] getElementNames() {
		return elementNames;
	}
	public void setElementNames(String[] elementNames) {
		this.elementNames = elementNames;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getCustomPropertyName() {
		return customPropertyName;
	}
	public void setCustomPropertyName(String customPropertyName) {
		this.customPropertyName = customPropertyName;
	}
	public String getSimulationItemTyp() {
		return simulationItemTyp;
	}
	public void setSimulationItemTyp(String simulationItemTyp) {
		this.simulationItemTyp = simulationItemTyp;
	}
}
