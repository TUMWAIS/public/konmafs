package manuelModelToJson;

public class ComparisonContainer {
	private EqualPropertyCheckJson equalPropertyCheckJson;
	private SimulationItemJson simulationItem;
	private SimulationReportJson simulationReport;
	private RequirementListItemJson requirementListItem;
	private BpmnItemJson bpmnItem;
	private CadItemJson cadItem;
	private ComponentListItemJson componentListItem;
	

	public SimulationItemJson getSimulationItem() {
		return simulationItem;
	}
	public void setSimulationItem(SimulationItemJson simulationItem) {
		this.simulationItem = simulationItem;
	}
	public SimulationReportJson getSimulationReport() {
		return simulationReport;
	}
	public void setSimulationReport(SimulationReportJson simulationReport) {
		this.simulationReport = simulationReport;
	}
	public RequirementListItemJson getRequirementListItem() {
		return requirementListItem;
	}
	public void setRequirementListItem(RequirementListItemJson requirementListItem) {
		this.requirementListItem = requirementListItem;
	}
	public EqualPropertyCheckJson getEqualPropertyCheckJson() {
		return equalPropertyCheckJson;
	}
	public void setEqualPropertyCheckJson(EqualPropertyCheckJson equalPropertyCheckJson) {
		this.equalPropertyCheckJson = equalPropertyCheckJson;
	}
	public BpmnItemJson getBpmnItem() {
		return bpmnItem;
	}
	public void setBpmnItem(BpmnItemJson bpmnItem) {
		this.bpmnItem = bpmnItem;
	}
	public CadItemJson getCadItem() {
		return cadItem;
	}
	public void setCadItem(CadItemJson cadItem) {
		this.cadItem = cadItem;
	}
	public ComponentListItemJson getComponentListItem() {
		return componentListItem;
	}
	public void setComponentListItem(ComponentListItemJson componentListItem) {
		this.componentListItem = componentListItem;
	}
}
