package entities;

import java.util.List;

public abstract class EntityFileList<T> {
	
	private String fileName;
	
	public EntityFileList() {
		
	}
	
	public String getShortFileName() {
		String[] fileParts = fileName.split("\\\\");
		return fileParts[fileParts.length-1];
	}
	
	public abstract List<T> getShowableEntityList();

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public EntityFileList(String fileName) {
		this.fileName = fileName;
	}

}
