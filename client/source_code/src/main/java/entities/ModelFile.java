package entities;

import java.io.File;

public class ModelFile {

	public String fileWithFullPath;
	
	public ModelFile(String fileWithFullPath) {
		this.fileWithFullPath = fileWithFullPath;
	}
	
	public File getFile() {
		File currentComponentFile = new File(fileWithFullPath);
		return currentComponentFile;
	}
	
	public boolean isFileOfType(String typeSuffix) {
		return fileWithFullPath.endsWith(typeSuffix);
	}
	
	public String toString() {
		if(fileWithFullPath.contains("/")) {
			String[] fileParts = fileWithFullPath.split("/");
			return fileParts[fileParts.length-1];
		} else {
			String[] fileParts = fileWithFullPath.split("\\\\");
			return fileParts[fileParts.length-1];
		}
	}
}
