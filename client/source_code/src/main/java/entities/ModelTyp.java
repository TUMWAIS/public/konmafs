package entities;

public enum ModelTyp {
	COMP("Komponentenliste"),
	SIM("Simulation"),
	BPMN("BPMN"),
	FREECAD("FreeCad"),
	CONSISTNECE_RULES("Konsistenz Regeln"),
	PAPYRUS_MODEL("Modelle");
	
	private final String text;
	
	ModelTyp(final String text) {
        this.text = text;
    }
	
	@Override
    public String toString() {
        return text;
    }
}
