package entities;

public class ModelElementContainer<T> {
	
	private T modelElement;
	private ModelTyp modelType;
	
	public ModelElementContainer(T modelElement, ModelTyp modelType) {
		super();
		this.modelElement = modelElement;
		this.modelType = modelType;
	}

	public T getModelElement() {
		return modelElement;
	}

	public ModelTyp getModelType() {
		return modelType;
	}
	
	
	
}
