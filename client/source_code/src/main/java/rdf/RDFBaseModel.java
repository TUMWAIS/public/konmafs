package rdf;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

public class RDFBaseModel {
	
	public static final String BASE_URI = "http://example.org";
	public static final String UNITS_NS = BASE_URI+"/"+"units";
	public static final String UNIT_COLLECTION_NS = UNITS_NS+"/"+"unit";

	public Model model;
	
	public Resource unit_ms;
	public Resource unit_s;
	public Resource unit_h;
	public Resource unit_mm;
	public Resource unit_m;
	public Resource unit_cm;
	public Resource unit_min;
	public Resource unit_m_div_s;
	public Resource unit_one_div_s;
	public Resource unit_noUnit;
	public Resource unit_d;
	public Resource unit_w;
	public Resource unit_y;
	
	public RDFBaseModel() {
		init();
	}
	
	private void init() {
		model = ModelFactory.createDefaultModel();

		unit_ms = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.ms.toString());
		unit_s = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.s.toString());
		unit_h = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.h.toString());
		unit_mm = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.mm.toString());
		unit_m = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.m.toString());
		unit_cm = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.cm.toString());
		unit_min = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.min.toString());
		unit_m_div_s = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.m_div_s.toString());
		unit_one_div_s = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.one_div_s.toString());
		unit_noUnit = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.noUnit.toString());
		unit_d = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.d.toString());
		unit_w = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.w.toString());
		unit_y = model.createResource(UNIT_COLLECTION_NS+"/"+Unit.y.toString());
	}
	
	public Model getModel() {
		return model;
	}
}
