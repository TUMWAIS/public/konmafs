package rdf;

import java.util.Arrays;
import java.util.Optional;

public enum Unit {
	mm("mm"),
	m("m"),
	cm("cm"),
	ms("ms"),
	s("s"),
	h("h"),
	min("min"),
	m_div_s("m/s"),
	one_div_s("1/s"),
	noUnit("-"),
	d("d"),
	w("w"),
	y("y");

	private String unitString;

	Unit(String unitString) {
		this.unitString = unitString;
	}

	public String getUnitString() {
		return unitString;
	}

	public static Optional<Unit> get(String unitString) {
		return Arrays.stream(Unit.values()).filter(unit -> unit.unitString.equals(unitString)).findFirst();
	}
	
	public String toString() {
		return unitString;
	}
}
