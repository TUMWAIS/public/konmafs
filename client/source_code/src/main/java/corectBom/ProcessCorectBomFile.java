package corectBom;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;

import componentList.ComponentListRDFWriter;
import componentList.ReadComponentList;
import entities.ModelFile;
import freeCad.CadRDFWriter;
import freeCad.ReadFreeCad;
import javafx.concurrent.Task;
import rdf.RDFBaseModel;

public class ProcessCorectBomFile  extends Task<Void> {

	private List<ModelFile> modelFiles;
	private ModelFile excelFile;
	
	public ProcessCorectBomFile(List<ModelFile> modelFiles) {
		this.modelFiles = modelFiles;
	}
	
	private String getCheckCountOfElementsSPARQL() {
		String query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+ "PREFIX cadItem: <http://example.org/cad/cadItem#>"
				+ "PREFIX componentListItem: <http://example.org/componentList/componentListItem#> "
				+ "SELECT ?compName  (STR(?compNumber) AS ?ComponentListNumber) ?cadName (STR(?cadNumber) AS ?CAD_Number) ?difNumber WHERE "
				+ "{  "
				+ "		{ SELECT ?compName   ?compNumber WHERE "
				+ "		{  "
				+ "			?s componentListItem:componentName ?compName . "
				+ "			?s componentListItem:quantity ?compNumber }  "
				+ "		} "
				+ "	{ SELECT (?name2 AS ?cadName) (?number AS ?cadNumber) WHERE "
				+ "	{  "
				+ "		{ SELECT ?name2 (COUNT(?name2) AS ?number) "
				+ "			WHERE "
				+ "			{ "
				+ "				[] cadItem:name ?name . "
				+ "				FILTER(regex(?name,'[0-9]{2}$','i') ) "
				+ "				BIND(SUBSTR(?name, 0, STRLEN(?name)-1) AS ?name2) "
				+ "			} GROUP BY ?name2 "
				+ "		}  "
				+ "		UNION  "
				+ "		{ SELECT (?name AS ?name2) (COUNT(?name) AS ?number) WHERE "
				+ "		{ "
				+ "			?s cadItem:name ?name . "
				+ "			FILTER(regex(?name,'[^0-9]{2}$','i') ) "
				+ "		} "
				+ "		GROUP BY ?name "
				+ "		}  "
				+ "	}  "
				+ "	}  "
				+ "	FILTER( ?compName = ?cadName && ?compNumber != ?cadNumber)  "
				+ "	BIND(?compNumber - ?cadNumber AS ?difNumber ). "
				+ "} "
				+ "ORDER BY ?compName ?cadName";
		return query;
	}
	
	@Override
	protected Void call() throws Exception {
		processCorectBomFile();
		return null;
	}
	
	private void processCorectBomFile() {
		String sparqlQuery = getCheckCountOfElementsSPARQL();
		updateMessage("Reading Model Files, please wait");
		Model bpmnModel = readCADandExcelFile();
		updateMessage("Searching unequal values");
		List<CorectBomQueryResult> queryResults = getCountOfElementsSparqlResult(bpmnModel,sparqlQuery);
		ChangeExcelEntries changeExcelEntries = new ChangeExcelEntries(queryResults, excelFile);
		changeExcelEntries.correctBom();
		updateMessage("Corrected File created");
	}
	
	private Model readCADandExcelFile() {
		RDFBaseModel baseModel = new RDFBaseModel();
		for(ModelFile modelFile : modelFiles) {			
			if(modelFile.isFileOfType(".FCStd")) {
				ReadFreeCad readFreeCad = new ReadFreeCad(modelFile);
				readFreeCad.readFreeCadEntity();
				CadRDFWriter cadRdfWirter = new CadRDFWriter(baseModel);
				cadRdfWirter.writeCADElementsToRdfModel(readFreeCad.getDocumentContent().getVisibleObjects(),null);
				cadRdfWirter.writeCADGroupElementsToRdfModel(readFreeCad.getDocumentContent().getGroups());
			}
			if(modelFile.isFileOfType(".xlsx")) {
				excelFile = modelFile;
				ReadComponentList readComponentList = new ReadComponentList(modelFile);
				readComponentList.readCompontenListFile();
				ComponentListRDFWriter componentListRDFWriter = new ComponentListRDFWriter(baseModel);
				componentListRDFWriter.writeComponentListItemsToRDFModel(readComponentList.getComponentList().getComponents(),null);
			}
			if(modelFile.isFileOfType(".uml")) {
				ReadCorrectionConfig readCorrectionConfig = new ReadCorrectionConfig();
				readCorrectionConfig.readConfigurationFile(modelFile.getFile());
			}
		}
		return baseModel.getModel();
	}
	
	private List<CorectBomQueryResult> getCountOfElementsSparqlResult(Model model, String queryString) {
		List<CorectBomQueryResult> bomQueryResults = new ArrayList<>();
		Query query = QueryFactory.create(queryString) ;
		  try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
		    ResultSet results = qexec.execSelect() ;

		    List<QuerySolution> querieResult = ResultSetFormatter.toList(results);
		    for(QuerySolution result : querieResult) {
		    	CorectBomQueryResult queryResult = new CorectBomQueryResult();
		    	queryResult.setName(result.getLiteral("compName").toString());
		    	Integer componentListNumber = Integer.valueOf(result.getLiteral("ComponentListNumber").toString());
		    	queryResult.setComponentListNumber(componentListNumber.intValue());
		    	Integer cadNumber = Integer.valueOf(result.getLiteral("CAD_Number").toString());
		    	queryResult.setCadNumber(cadNumber.intValue());
		    	Integer differenceNumber = Integer.valueOf(componentListNumber.intValue()-cadNumber.intValue());
		    	queryResult.setComponentListNumber(differenceNumber.intValue());
		    	bomQueryResults.add(queryResult);
		    }
		  }catch (Exception e) {
			  e.printStackTrace();
		}
		return bomQueryResults;
	}
	
	
}
