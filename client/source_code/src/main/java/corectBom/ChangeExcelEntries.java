package corectBom;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import componentList.ReadComponentList;
import entities.ModelFile;

public class ChangeExcelEntries {
	
	private List<CorectBomQueryResult> queryResults;
	private ModelFile excelFile;

	ChangeExcelEntries(List<CorectBomQueryResult> queryResults, ModelFile excelFile){
		this.queryResults = queryResults;
		this.excelFile = excelFile;
	}
	
	public void correctBom() {
		ReadComponentList readComponentList = new ReadComponentList(excelFile);
		readComponentList.readCompontenListFile();
		
		
	    Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(new FileInputStream(excelFile.fileWithFullPath));
		} catch (IOException e) {
				e.printStackTrace();
				
		}
	    Sheet sheet = workbook.getSheetAt(0);
		
		
		

		HashMap<String, Integer> componentNameToRowNumber = readComponentList.getComponentNameToRowNumber();
		int colNumber = readComponentList.getColumnNumberForQuantityPosition();
		for(CorectBomQueryResult result : queryResults) {
			int rowNumber = componentNameToRowNumber.get(result.getName());
			double valueOfCadElements = Double.valueOf(result.getCadNumber());
			Row row = sheet.getRow(rowNumber);
			Cell cell = row.getCell(colNumber);
			cell.setCellValue(valueOfCadElements);
		}
		
		try {
			String fileName = excelFile.fileWithFullPath.substring(0, excelFile.fileWithFullPath.length()-5);
			fileName = fileName +"_Corrected.xlsx";
		    FileOutputStream out = new FileOutputStream(fileName);
		    workbook.write(out);
		    out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
