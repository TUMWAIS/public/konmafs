package corectBom;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ReadCorrectionConfig {
	
	String correct_source;
	String target_to_correct;
	String correction_mode;

	public void readConfigurationFile(File configFile) {
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(configFile);
		}
		catch (Exception e) {
			return;
		}
		doc.getDocumentElement().normalize();
		
		Node first = doc.getFirstChild();
		Node correctInconsistenceNode = first.getChildNodes().item(first.getChildNodes().getLength()-2);
		
		correct_source = correctInconsistenceNode.getAttributes().getNamedItem("correct_source").getNodeValue();
		target_to_correct = correctInconsistenceNode.getAttributes().getNamedItem("correct_source").getNodeValue();
		correction_mode = correctInconsistenceNode.getAttributes().getNamedItem("correct_source").getNodeValue();
	}
	
	public String getCorrect_source() {
		return correct_source;
	}

	public void setCorrect_source(String correct_source) {
		this.correct_source = correct_source;
	}

	public String getTarget_to_correct() {
		return target_to_correct;
	}

	public void setTarget_to_correct(String target_to_correct) {
		this.target_to_correct = target_to_correct;
	}

	public String getCorrection_mode() {
		return correction_mode;
	}

	public void setCorrection_mode(String correction_mode) {
		this.correction_mode = correction_mode;
	}
}
