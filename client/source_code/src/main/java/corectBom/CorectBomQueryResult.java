package corectBom;

public class CorectBomQueryResult {

	private String name;
	private int componentListNumber;
	private int cadNumber;
	private int difference;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getComponentListNumber() {
		return componentListNumber;
	}
	public void setComponentListNumber(int componentListNumber) {
		this.componentListNumber = componentListNumber;
	}
	public int getCadNumber() {
		return cadNumber;
	}
	public void setCadNumber(int cadNumber) {
		this.cadNumber = cadNumber;
	}
	public int getDifference() {
		return difference;
	}
	public void setDifference(int difference) {
		this.difference = difference;
	}
	 
}
