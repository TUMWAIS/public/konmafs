package bpmn.bpmnEntities;

import java.util.ArrayList;
import java.util.List;

public class BPMNNodeEntity {

	private String id;
	private BPMNNodeTyp nodeTyp;
	private String name;

	private BPMNEntity bpmnEntity;
	private List<String> incommingFlows = new ArrayList<>();
	private List<String> outgoingFlows = new ArrayList<>();
	
	public void addIncomingFlowId(String incommingFlowId) {
		incommingFlows.add(incommingFlowId);
	}
	
	public void addOutgoingFlowId(String outgoingFlowId) {
		outgoingFlows.add(outgoingFlowId);
	}
	
	public List<String> getIncommingFlows(){
		return incommingFlows;
	}
	
	public List<String> getOutgoingFlows(){
		return outgoingFlows;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BPMNNodeTyp getNodeTyp() {
		return nodeTyp;
	}

	public void setNodeTyp(BPMNNodeTyp nodeTyp) {
		this.nodeTyp = nodeTyp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name+"("+nodeTyp.toString()+")";
	}

	public BPMNEntity getBpmnEntity() {
		return bpmnEntity;
	}

	public void setBpmnEntity(BPMNEntity bpmnEntity) {
		this.bpmnEntity = bpmnEntity;
	}
}
