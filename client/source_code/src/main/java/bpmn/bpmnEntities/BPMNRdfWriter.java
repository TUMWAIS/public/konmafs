package bpmn.bpmnEntities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import rdf.RDFBaseModel;

public class BPMNRdfWriter {
	
	public static final String BASE_URI = "http://example.org";
	public static final String BPMN_NS = BASE_URI+"/"+"bpmn";
	public static final String BPMN_NODE_NS = BPMN_NS+"/"+"bpmnItem#";
	public static final String BPMN_NODE_COLLECTION_NS = BPMN_NS+"/"+"bpmnItem";
	public static final String BPMN_NODE_TYP_NS = BPMN_NS+"/bpmnItemTyp";
	
	public static final String BPMN_NODE_PROPERTY_NAME = BPMN_NODE_NS+ "name";
	public static final String BPMN_NODE_PROPERTY_INCOMING_FLOWS = BPMN_NODE_NS+ "incomingFlow";
	public static final String BPMN_NODE_PROPERTY_OUTGOING_FLOWS = BPMN_NODE_NS+ "outgoingFlow";
	public static final String BPMN_NODE_PROPERTY_NODE_TYP = BPMN_NODE_NS+ "itemTyp";
	public static final String BPMN_NODE_PROPERTY_CONDITION_EXPRESSION = BPMN_NODE_NS+ "conditionExpression";
	public static final String BPMN_NODE_PROPERTY_TARGET_NODE = BPMN_NODE_NS+ "targetNode";
	public static final String BPMN_NODE_PROPERTY_SOURCE_NODE = BPMN_NODE_NS+ "sourceNode";
	
	Property bpmnNodeName;
	Property bpmnNodeIncomingFlows;
	Property bpmnNodeOutgoingFlows;
	Property bpmnNodeTyp;
	Property bpmnNodeConditionFlow;
	Property bpmnNodeTargetNode;
	Property bpmnNodeSourceNode;
	
	public Model model;

	
	public static void resetCounter() {
		bpmnNodeCounter = 1;
	}
	
	private static int bpmnNodeCounter = 1;
	
	private Map<String,Resource> resourceMap = new HashMap<>();
	
	public BPMNRdfWriter(RDFBaseModel baseModel) {
		this.model = baseModel.getModel();
		initBPMNWrite();
	}
	
	private void initBPMNWrite() {
		model.setNsPrefix("bpmnItem",BPMN_NODE_NS);
		
		bpmnNodeName = model.createProperty( BPMN_NODE_PROPERTY_NAME);
		bpmnNodeIncomingFlows = model.createProperty( BPMN_NODE_PROPERTY_INCOMING_FLOWS);
		bpmnNodeOutgoingFlows = model.createProperty(BPMN_NODE_PROPERTY_OUTGOING_FLOWS);
		bpmnNodeTyp = model.createProperty(BPMN_NODE_PROPERTY_NODE_TYP);
		
		bpmnNodeConditionFlow = model.createProperty(BPMN_NODE_PROPERTY_CONDITION_EXPRESSION);
		bpmnNodeTargetNode = model.createProperty(BPMN_NODE_PROPERTY_TARGET_NODE);
		bpmnNodeSourceNode = model.createProperty(BPMN_NODE_PROPERTY_SOURCE_NODE);
	}
	
	
	
	public void writeBPMNodesToRDF(List<BPMNNodeEntity> selectedBpmnNodeList, File file) {
		if(selectedBpmnNodeList.isEmpty()) {
			return;
		}
		buildResourceMap(selectedBpmnNodeList.get(0).getBpmnEntity());

		Set<String> flowIds = new HashSet<>();
		for(BPMNNodeEntity item : selectedBpmnNodeList) {
			Resource nodeResource = resourceMap.get(item.getId());
			for(String incomingFlow : item.getIncommingFlows()) {
				flowIds.add(incomingFlow);
				model.add(nodeResource, bpmnNodeIncomingFlows, resourceMap.get(incomingFlow));
			}
			for(String outgoingFlow : item.getOutgoingFlows()) {
				flowIds.add(outgoingFlow);
				model.add(nodeResource, bpmnNodeOutgoingFlows, resourceMap.get(outgoingFlow));
			}
			model.add(nodeResource, bpmnNodeTyp, resourceMap.get(item.getNodeTyp().toString()));
			model.add(nodeResource, bpmnNodeName, item.getName());
			
		}
		Map<String, BPMNFlowEntity> flowMap = selectedBpmnNodeList.get(0).getBpmnEntity().getBPMNFlows();
		for(String flowId : flowIds) {
			BPMNFlowEntity flow = flowMap.get(flowId);
			if(flow.getName()!=null && !flow.getName().isEmpty()) {
				model.add(resourceMap.get(flowId),bpmnNodeName,flow.getName());
			}
			if(flow.getConditionExpression()!=null && !flow.getConditionExpression().isEmpty()) {
				String[] conditionExpressions = new String[1];
				conditionExpressions[0] = flow.getConditionExpression();
				if(flow.getConditionExpression().contains(",")) {
					conditionExpressions = flow.getConditionExpression().split(",");
				}
				for(String expression : conditionExpressions) {
					model.add(resourceMap.get(flowId),bpmnNodeConditionFlow,expression);
				}				
			}
			model.add(resourceMap.get(flowId),bpmnNodeSourceNode,resourceMap.get(flow.getSourceId()));
			model.add(resourceMap.get(flowId),bpmnNodeTargetNode,resourceMap.get(flow.getTargetId()));
			model.add(resourceMap.get(flowId), bpmnNodeTyp, resourceMap.get(BPMNNodeTyp.flow.toString()));
		}
		try {
			if(file!=null) {
				FileOutputStream fop = new FileOutputStream(file);
				RDFDataMgr.write(fop, model, Lang.RDFXML);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	private void buildResourceMap(BPMNEntity bpmnEntity) {
		for(BPMNFlowEntity flow : bpmnEntity.getBPMNFlows().values()) {
			Resource nodeResource = model.createResource(BPMN_NODE_COLLECTION_NS+"/"+"bpmnItem_"+bpmnNodeCounter++);
			resourceMap.put(flow.getId(), nodeResource);

		}
		for(BPMNNodeEntity node : bpmnEntity.getBPMNNodes().values()) {
			Resource nodeResource = model.createResource(BPMN_NODE_COLLECTION_NS+"/"+"bpmnNode_"+bpmnNodeCounter++);
			resourceMap.put(node.getId(), nodeResource);
		}
		for(BPMNNodeTyp type : BPMNNodeTyp.values()) {
			Resource nodeTypeResource = model.createResource(BPMN_NODE_TYP_NS+"/"+type.toString());
			resourceMap.put(type.toString(), nodeTypeResource);
		}
	}

}
