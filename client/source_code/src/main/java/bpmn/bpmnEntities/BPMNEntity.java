package bpmn.bpmnEntities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.EntityFileList;

public class BPMNEntity extends EntityFileList<BPMNNodeEntity>{

	private Map<String, BPMNNodeEntity> nodes = new HashMap<>();
	private Map<String, BPMNFlowEntity> flows = new HashMap<>();
	
	public void addBPMNNodeToMap(BPMNNodeEntity node) {
		nodes.put(node.getId(),node);
	}
	
	public void addBPMNFlowEntity(BPMNFlowEntity flow) {
		flows.put(flow.getId(), flow);
	}
	
	public Map<String, BPMNNodeEntity> getBPMNNodes(){
		return nodes;
	}
	
	public Map<String, BPMNFlowEntity> getBPMNFlows(){
		return flows;
	}
	
	@Override
	public List<BPMNNodeEntity> getShowableEntityList() {
		return List.copyOf(nodes.values());
	}

}
