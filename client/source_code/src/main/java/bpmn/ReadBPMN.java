package bpmn;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import bpmn.bpmnEntities.BPMNEntity;
import bpmn.bpmnEntities.BPMNFlowEntity;
import bpmn.bpmnEntities.BPMNNodeEntity;
import bpmn.bpmnEntities.BPMNNodeTyp;
import entities.ModelFile;

public class ReadBPMN {
	private ModelFile bpmnFile;
	private BPMNEntity currentbpmnEntity;

	public ReadBPMN(ModelFile bpmnFile) {
		this.bpmnFile = bpmnFile;
		currentbpmnEntity = new BPMNEntity();
		currentbpmnEntity.setFileName(bpmnFile.getFile().getPath());
	}

	public void readBPMNFile() {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document doc = db.parse(bpmnFile.getFile());

			readBPMNEntities(doc);
			
			readBPMNFlowEntities(doc);
		} catch (Exception e) {

		}
	}
	
	public BPMNEntity getBPMNEntity() {
		return currentbpmnEntity;
	}
	
	private void readBPMNEntities(Document doc) {
		for (BPMNNodeTyp nodeTyp : BPMNNodeTyp.values()) {

			NodeList list = doc.getElementsByTagName(nodeTyp.toString());

			for (int listIndex = 0; listIndex < list.getLength(); listIndex++) {

				Node node = list.item(listIndex);
				NamedNodeMap nodeAttributes = node.getAttributes();

				BPMNNodeEntity bpmnNodeEntity = new BPMNNodeEntity();
				bpmnNodeEntity.setBpmnEntity(currentbpmnEntity);
				bpmnNodeEntity.setId(nodeAttributes.getNamedItem("id").getNodeValue());
				bpmnNodeEntity.setName(nodeAttributes.getNamedItem("name").getNodeValue());
				bpmnNodeEntity.setNodeTyp(BPMNNodeTyp.valueOf(node.getNodeName()));

				NodeList listOfSubnodes = node.getChildNodes();
				for (int i = 0; i < listOfSubnodes.getLength(); i++) {
					Node subnode = listOfSubnodes.item(i);
					if (subnode.getNodeName().contains("outgoing")) {
						bpmnNodeEntity.addOutgoingFlowId(subnode.getFirstChild().getNodeValue());
					}
					if (subnode.getNodeName().contains("incoming")) {
						bpmnNodeEntity.addIncomingFlowId(subnode.getFirstChild().getNodeValue());
					}
				}
				
				currentbpmnEntity.addBPMNNodeToMap(bpmnNodeEntity);
			}
		}
	}
	
	private void readBPMNFlowEntities(Document doc) {
		NodeList list = doc.getElementsByTagName("sequenceFlow");

		for (int listIndex = 0; listIndex < list.getLength(); listIndex++) {

			Node flowNode = list.item(listIndex);
			NamedNodeMap nodeAttributes = flowNode.getAttributes();
			BPMNFlowEntity flowEntity = new BPMNFlowEntity();
			flowEntity.setId(nodeAttributes.getNamedItem("id").getNodeValue());
			flowEntity.setName(nodeAttributes.getNamedItem("name").getNodeValue());
			flowEntity.setSourceId(nodeAttributes.getNamedItem("sourceRef").getNodeValue());
			flowEntity.setTargetId(nodeAttributes.getNamedItem("targetRef").getNodeValue());
			
			NodeList listConditions = flowNode.getChildNodes();
			for (int i = 0; i < listConditions.getLength(); i++) {
				Node conditionNode = listConditions.item(i);
				if (conditionNode.getNodeName().contains("conditionExpression")) {
					String conditionExpression = conditionNode.getFirstChild().getNodeValue();
					if(conditionExpression.contains(":")) {
						conditionExpression = conditionExpression.split(":")[1];
					}
					conditionExpression = conditionExpression.trim();
					flowEntity.setConditionExpression(conditionExpression);
				}
			}
			currentbpmnEntity.addBPMNFlowEntity(flowEntity);
		}
	}

}
