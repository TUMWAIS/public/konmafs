package requirementList;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import entities.EntityFileList;

public class RequirementList extends EntityFileList<RequirementItem>{
	private Map<String, RequirementItem> requirementList = new LinkedHashMap<>();

	public void addItem(RequirementItem requirementItem) {
		requirementList.put(requirementItem.getPropertyName(), requirementItem);
	}
	
	public Map<String, RequirementItem> getRequirementList() {
		return requirementList;
	}

	@Override
	public List<RequirementItem> getShowableEntityList() {
		return new ArrayList<RequirementItem>(requirementList.values());
	}
	
}
