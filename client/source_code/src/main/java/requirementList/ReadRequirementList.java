package requirementList;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import entities.ModelFile;
import rdf.Unit;

public class ReadRequirementList {
	private ModelFile requirementFile;
	private RequirementList requirementList;
	
	public ReadRequirementList(ModelFile requirementFile) {
		this.requirementFile = requirementFile;
		requirementList = new RequirementList();
		requirementList.setFileName(requirementFile.getFile().getPath());
	}
	
	public RequirementList getRequirementList() {
		return requirementList;
	}
	
	public void readRequirementListFile() {
		File excelFile = requirementFile.getFile();
	    Workbook workbook = null;
		try {
			workbook = new XSSFWorkbook(excelFile);
		} catch (InvalidFormatException | IOException e) {
				e.printStackTrace();
				
		}
	    Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = datatypeSheet.iterator();
        iterator.next();
        iterator.next();
        DataFormatter formatter = new DataFormatter();
        
        while (iterator.hasNext()) {
        	

            Row currentRow = iterator.next();
            if(currentRow.getCell(4).getCellType()==CellType.BLANK) {
            	break;
            }
            	
            RequirementItem requirementItem = new RequirementItem();
            requirementItem.setNumber(formatter.formatCellValue(currentRow.getCell(1)));
            requirementItem.setImportance(currentRow.getCell(2).getStringCellValue());
            requirementItem.setTyp(currentRow.getCell(3).getStringCellValue());
            requirementItem.setPropertyName(currentRow.getCell(4).getStringCellValue());
            if(currentRow.getCell(5).getCellType()!=CellType.BLANK) {
            	try {
            	requirementItem.setMinValue(Double.valueOf(currentRow.getCell(5).getNumericCellValue()));
            	} catch (Exception e) {
            		
            	}
            }
            if(currentRow.getCell(6).getCellType()!=CellType.BLANK) {
            	requirementItem.setEqualValue(currentRow.getCell(6).getStringCellValue());
            }
            if(currentRow.getCell(7).getCellType()!=CellType.BLANK) {
            	requirementItem.setMaxValue(Double.valueOf(currentRow.getCell(7).getNumericCellValue()));
            }
            Optional<Unit> unit = Unit.get(currentRow.getCell(8).getStringCellValue());
            if(unit.isPresent()) {
            	requirementItem.setUnit(unit.get());
            }
            requirementItem.setRemark(currentRow.getCell(9).getStringCellValue());

            requirementList.addItem(requirementItem);
        }
        try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
