package requirementList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import rdf.RDFBaseModel;

public class RequirementListRDFWriter {
	public static final String BASE_URI = "http://example.org";
	public static final String REQUIREMENT_LIST_NS = BASE_URI+"/"+"requirementList";
	public static final String REQUIREMENT_LIST_PROPERTY_NS = REQUIREMENT_LIST_NS+"/"+"requirementListItem#";
	public static final String REQUIREMENT_LIST_COLLECTION_NS = REQUIREMENT_LIST_NS+"/"+"requirementListItem";
	public static final String REQUIREMENT_LIST_PROPERTY_NAME = REQUIREMENT_LIST_PROPERTY_NS+"name";
	public static final String REQUIREMENT_LIST_PROPERTY_MIN_VALUE = REQUIREMENT_LIST_PROPERTY_NS+"minValue";
	public static final String REQUIREMENT_LIST_PROPERTY_MAX_VALUE = REQUIREMENT_LIST_PROPERTY_NS+"maxValue";
	public static final String REQUIREMENT_LIST_PROPERTY_UNIT = REQUIREMENT_LIST_PROPERTY_NS+"unit";
	public static final String REQUIREMENT_LIST_PROPERTY_EQUAL_VALUE = REQUIREMENT_LIST_PROPERTY_NS+"equalValue";
	
	public static final String REQUIREMENT_LIST_PROPERTY_NUMBER = REQUIREMENT_LIST_PROPERTY_NS+"number";
	public static final String REQUIREMENT_LIST_PROPERTY_IMPORTANCE = REQUIREMENT_LIST_PROPERTY_NS+"importance";
	public static final String REQUIREMENT_LIST_PROPERTY_TYP = REQUIREMENT_LIST_PROPERTY_NS+"typ";
	public static final String REQUIREMENT_LIST_PROPERTY_REMARK = REQUIREMENT_LIST_PROPERTY_NS+"remark";
	
	Property requirementListPropertyName;
	Property requirementListPropertyMinValue;
	Property requirementListPropertyMaxValue;
	Property requirementListPropertyUnit;
	Property requirementListPropertyEqualValue;
	
	Property requirementListPropertyNumber;
	Property requirementListPropertyImportance;
	Property requirementListPropertyTyp;
	Property requirementListPropertyRemark;
	
	public Model model;
	private RDFBaseModel baseModel;
	
	public RequirementListRDFWriter(RDFBaseModel baseModel) {
		this.baseModel = baseModel;
		this.model = baseModel.getModel();
		initRequirementListRdfModel();
	}
	
	public static void resetCounter() {
		requirementListItemCounter = 1;
	}
	
	private static int requirementListItemCounter=1;
	
	public void initRequirementListRdfModel() {		
		model.setNsPrefix( "requirementListItem", REQUIREMENT_LIST_PROPERTY_NS );
		requirementListPropertyName = model.createProperty(REQUIREMENT_LIST_PROPERTY_NAME);
		requirementListPropertyMinValue = model.createProperty(REQUIREMENT_LIST_PROPERTY_MIN_VALUE);
		requirementListPropertyMaxValue = model.createProperty(REQUIREMENT_LIST_PROPERTY_MAX_VALUE);
		requirementListPropertyUnit = model.createProperty(REQUIREMENT_LIST_PROPERTY_UNIT);
		requirementListPropertyEqualValue = model.createProperty(REQUIREMENT_LIST_PROPERTY_EQUAL_VALUE);
		
		requirementListPropertyNumber = model.createProperty(REQUIREMENT_LIST_PROPERTY_NUMBER);
		requirementListPropertyImportance = model.createProperty(REQUIREMENT_LIST_PROPERTY_IMPORTANCE);
		requirementListPropertyTyp = model.createProperty(REQUIREMENT_LIST_PROPERTY_TYP);
		requirementListPropertyRemark = model.createProperty(REQUIREMENT_LIST_PROPERTY_REMARK);
	}
	
	public void writeRequirementListItemsToRDFModel(List<RequirementItem> requirementListItems, File file) {
		for(RequirementItem requirementItem : requirementListItems) {
			Resource requirementItemSubject = model.createResource(REQUIREMENT_LIST_COLLECTION_NS+"/"+"requirementListItem_"+requirementListItemCounter++);
			
			model.add(requirementItemSubject,requirementListPropertyName,requirementItem.getPropertyName());
			if(requirementItem.getMinValue()!=null) {
				model.add(requirementItemSubject,requirementListPropertyMinValue,model.createTypedLiteral(requirementItem.getMinValue()));
			}
			if(requirementItem.getMaxValue()!=null) {
				model.add(requirementItemSubject,requirementListPropertyMaxValue,model.createTypedLiteral(requirementItem.getMaxValue()));
			}
			if(requirementItem.getEqualValue()!=null) {
				model.add(requirementItemSubject,requirementListPropertyEqualValue,requirementItem.getEqualValue());
			}
			if(requirementItem.getNumber()!=null) {
				model.add(requirementItemSubject,requirementListPropertyNumber,requirementItem.getNumber());
			}
			if(requirementItem.getImportance()!=null) {
				model.add(requirementItemSubject,requirementListPropertyImportance,requirementItem.getImportance());
			}
			if(requirementItem.getTyp()!=null) {
				model.add(requirementItemSubject,requirementListPropertyTyp,requirementItem.getTyp());
			}
			if(requirementItem.getRemark()!=null) {
				model.add(requirementItemSubject,requirementListPropertyRemark,requirementItem.getRemark());
			}
			
			switch(requirementItem.getUnit()) {
				case min:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_min);break;
				case ms:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_ms);break;
				case s:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_s);break;
				case h:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_h);break;
				case mm:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_mm);break;
				case m:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_m);break;
				case cm:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_cm);break;
				case m_div_s:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_m_div_s);break;
				case one_div_s:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_one_div_s);break;
				case noUnit:model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_noUnit);break;
			default:
				model.add(requirementItemSubject,requirementListPropertyUnit,baseModel.unit_h);
			}
		}
		if(file!=null) {
			try {
				FileOutputStream fop = new FileOutputStream(file);
				RDFDataMgr.write(fop, model, Lang.RDFXML);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
