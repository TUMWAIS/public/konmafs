package generateViewModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;

import entities.ModelFile;
import gui.GuiConsistencRules;
import javafx.concurrent.Task;
import resultProcessing.ReadAndUploadModels;

public class ProcessReadElementsFromToolFiles extends Task<Void> {
	
	List<ModelFile> modelFiles;
	GuiConsistencRules gui;
	private Model modelWithData;
	private List<String> elementNames = new ArrayList<>();

	public ProcessReadElementsFromToolFiles(List<ModelFile> modelFiles, GuiConsistencRules gui) {
		this.modelFiles = modelFiles;
		this.gui = gui;
	}
	
	@Override
	protected Void call() throws Exception {
		processReadElementsFromToolFiles();
		return null;
	}
	
	public void processReadElementsFromToolFiles() {
		updateMessage("Reading files please wait");
		ReadAndUploadModels readModelsFromList = new ReadAndUploadModels(null);
		readModelsFromList.readAndUploadModels(modelFiles,null);
		
		modelWithData = readModelsFromList.getClientModel();
		readNamesForModeltyp();
		
	}
	
	private void readNamesForModeltyp() {
		String queryString= "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+ "PREFIX simItem: <http://example.org/simulation/simulationItem#> ";
		
		queryString += " SELECT ?name ?typ WHERE { ?s simItem:name ?name. ?s simItem:typ ?typ. ";//+"}";
		queryString +="FILTER(?typ IN (";
		queryString +="<http://example.org/simulationItemTyp/EntityGenerator>,";
		queryString +="<http://example.org/simulationItemTyp/Server>,";
		queryString +="<http://example.org/simulationItemTyp/Branch>,";
		queryString +="<http://example.org/simulationItemTyp/EntitySink>,";
		queryString +="<http://example.org/simulationItemTyp/Queue>,";
		queryString +="<http://example.org/simulationItemTyp/EntityConveyor>)).}";
		
		Query query = QueryFactory.create(queryString) ;
		  try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithData)) {
		    ResultSet results = qexec.execSelect() ;
		    List<QuerySolution> querieResult = ResultSetFormatter.toList(results);
		    for(QuerySolution result : querieResult) {
		    	elementNames.add(result.getLiteral("name").toString());		
		    }	
		  }
		  catch(Exception e) {
			  e.printStackTrace();
		  }
	}

	public List<String> getElementNames() {
		return elementNames;
	}

	public Model getModelWithData() {
		return modelWithData;
	}
	
}
