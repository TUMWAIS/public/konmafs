package generateViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.QName;

import freeCad.FreeCadObject;
import jaamSim.JaamSimEntity;
import jaamSim.SimulationItemTyp;

public class WritePapyursModel {
	
	public static final String MODEL_NAME = "composedViews";
	public static final String ACTIVITY_NAME = "PaintingPlant";
	
	private int counter =100;
	private int counterNode =100;
	
	Element packagedElement = null;
	Element xmi_Outer = null;

	Namespace xmiNS = new Namespace("xmi","http://www.omg.org/spec/XMI/20131001");
	Namespace umlNS = new Namespace("uml","http://www.eclipse.org/uml2/5.0.0/UML");
	Namespace simulationNS = new Namespace("Simulation","http://views/schemas/Simulation/_5f1jIMTVEe2JxLl_9C5anw/0");
	Namespace cadNS = new Namespace("CAD","http://views/schemas/CAD/_5f3_dMTVEe2JxLl_9C5anw/0");
	
	Map<String,PapyrusNodeInfo> nodeNameToNodeInfoMap = new HashMap<>();
	@SuppressWarnings("unused")
	public Document createHeader() {
		Document document = DocumentHelper.createDocument();
		
		
		xmi_Outer = document.addElement(new QName("XMI",xmiNS))
				.addAttribute("xmi:version", "20131001")
				.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance")
				.addNamespace("CAD", cadNS.getText())
				.addNamespace("Simulation", simulationNS.getText())
				.addNamespace("ecore", "http://www.eclipse.org/emf/2002/Ecore")
				.addNamespace("uml",umlNS.getText())
				.addAttribute("xsi:schemaLocation", "http://views/schemas/CAD/_5f3_dMTVEe2JxLl_9C5anw/0 views.profile.uml#_5f4mccTVEe2JxLl_9C5anw http://views/schemas/Simulation/_5f1jIMTVEe2JxLl_9C5anw/0 views.profile.uml#_5f3_YMTVEe2JxLl_9C5anw");//"http://Repräsentationsschema/schemas/CAD/_EBqXQmvuEe2zlMAMQh6WxQ/34 Repräsentationsschema.profile.uml#_EBq-UGvuEe2zlMAMQh6WxQ http://Repräsentationsschema/schemas/Simulation/_EBq-g2vuEe2zlMAMQh6WxQ/15 Repräsentationsschema.profile.uml#_EBq-hGvuEe2zlMAMQh6WxQ");
		
		Element uml_Model = xmi_Outer.addElement(new QName("Model",umlNS))
				 .addAttribute("xmi:id", "_A000000000000000000"+counter++)
				 .addAttribute("name",MODEL_NAME);
			 
		 Element packageImport = uml_Model.addElement("packageImport")
				 .addAttribute("xmi:type", "uml:PackageImport")
				 .addAttribute("xmi:id", "_A000000000000000000"+counter++);
		 
		 Element importedPackage = packageImport.addElement("importedPackage")
				 .addAttribute("xmi:type", "uml:Model")
				 .addAttribute("href", "pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#_0");
		 
		 packagedElement = uml_Model.addElement("packagedElement")
				 .addAttribute("xmi:type", "uml:Activity")
				 .addAttribute("xmi:id", "_A000000000000000000"+counter++)
		 		.addAttribute("name", ACTIVITY_NAME);;
		 
		 String cadProfilId = "_P000000000000000000"+counter++;
		 Element profileApplicationCAD = uml_Model.addElement("profileApplication") 
				 .addAttribute("xmi:type", "uml:ProfileApplication")
				.addAttribute("xmi:id", cadProfilId);
		 Element profileApplicationCADENottations = profileApplicationCAD
		 		.addElement("eAnnotations")
		 		.addAttribute("xmi:type", "ecore:EAnnotation")
		 		.addAttribute("xmi:id", cadProfilId)
		 		.addAttribute("source", "http://www.eclipse.org/uml2/2.0.0/UML")
		 		.addElement("references")
		 		.addAttribute("xmi:type", "ecore:EPackage")
		 		.addAttribute("href", "views.profile.uml#_5f4mccTVEe2JxLl_9C5anw");
		 
		 Element appliedProfilCAD = profileApplicationCAD.addElement("appliedProfile")
				 .addAttribute("xmi:type", "uml:Profile")
				 .addAttribute("href", "views.profile.uml#_izaD8MTUEe2JxLl_9C5anw");
		 
		 String simulationProfilId = "_P000000000000000000"+counter++;
		 Element profileApplicationSimulation = uml_Model.addElement("profileApplication") 
				 .addAttribute("xmi:type", "uml:ProfileApplication")
				.addAttribute("xmi:id", simulationProfilId);
		 Element profileApplicationSimulationENottations = profileApplicationSimulation
		 		.addElement("eAnnotations")
		 		.addAttribute("xmi:type", "ecore:EAnnotation")
		 		.addAttribute("xmi:id", simulationProfilId)
		 		.addAttribute("source", "http://www.eclipse.org/uml2/2.0.0/UML")
		 		.addElement("references")
		 		.addAttribute("xmi:type", "ecore:EPackage")
		 		.addAttribute("href", "views.profile.uml#_5f3_YMTVEe2JxLl_9C5anw");
		 
		 Element appliedProfilSimulation = profileApplicationSimulation.addElement("appliedProfile")
				 .addAttribute("xmi:type", "uml:Profile")
				 .addAttribute("href", "views.profile.uml#_ekdzwMTTEe2JxLl_9C5anw");
		 return document;
	}
	
	Map<JaamSimEntity,String> idToEntityMap = new HashMap<>();
	HashMap<String,String> simTypeToPayprusType = new HashMap<>();
	
	private void fillPapyursTypeMap() {
		simTypeToPayprusType.put(SimulationItemTyp.EntityGenerator.toString(), "uml:InitialNode");
		simTypeToPayprusType.put(SimulationItemTyp.EntityConveyor.toString(), "uml:OpaqueAction");
		simTypeToPayprusType.put(SimulationItemTyp.Server.toString(), "uml:OpaqueAction");
		simTypeToPayprusType.put(SimulationItemTyp.Queue.toString(), "uml:OpaqueAction");
		simTypeToPayprusType.put(SimulationItemTyp.EntitySink.toString(), "uml:ActivityFinalNode");
		simTypeToPayprusType.put(SimulationItemTyp.Branch.toString(), "uml:DecisionNode");
	}
	
	Map<JaamSimEntity,Map<JaamSimEntity,String>> targetMap = new HashMap<>();
	Map<JaamSimEntity,List<String>> sourceMap = new HashMap<>();
	
	
	public void fillSimulationProfiles(List<JaamSimEntity> simulationNodes) {
		for(JaamSimEntity jaamItem : simulationNodes) {
			String nodeId = idToEntityMap.get(jaamItem);
			
			
			
			String papyursType = simTypeToPayprusType.get(jaamItem.getType());
			if(jaamItem.getType()==SimulationItemTyp.Branch.toString() && jaamItem.getPreviousComponents().size()>1) {
				papyursType = "uml:MergeNode";
			}
			papyursType = "base_"+papyursType.split(":")[1];
			
			nodeNameToNodeInfoMap.put(jaamItem.getName(), new PapyrusNodeInfo(nodeId,papyursType));
			
			Element simulationProfilElement = xmi_Outer.addElement(new QName("Simulation",simulationNS))
					.addAttribute("xmi:id", "_P000000000000000000"+counter++)
					.addAttribute(papyursType,idToEntityMap.get(jaamItem))
					.addAttribute("Name", jaamItem.getName())
					;
			if(jaamItem.getTravelTime()!=null) {
				simulationProfilElement.addAttribute("Travel_Time", jaamItem.getTravelTime())
										.addAttribute("Unit", "s");
			}
		}		
	}
	
	class PapyrusNodeInfo {
		public String nodeID;
		public String papyrusType;
		public PapyrusNodeInfo(String nodeID, String papyrusType) {
			this.nodeID = nodeID;
			this.papyrusType = papyrusType;
		}
		
		
		
	}
	

	
	@SuppressWarnings("unused")
	public void fillCADProfiles(List<FreeCadObject> cadItems) {
		for(FreeCadObject cadItem : cadItems) {
			PapyrusNodeInfo nodeInfo = nodeNameToNodeInfoMap.get(cadItem.getLabel());
			if(nodeInfo==null) {
				continue;
			}
			
			Element cadProfilElement = xmi_Outer.addElement(new QName("CAD",cadNS))
					.addAttribute("xmi:id", "_P000000000000000000"+counter++)
					.addAttribute(nodeInfo.papyrusType,nodeInfo.nodeID)
					.addAttribute("Name", cadItem.getLabel())
					.addAttribute("Lenght", Double.toString(cadItem.getLength()))
					.addAttribute("Width", Double.toString(cadItem.getWidth()))
					.addAttribute("Height", Double.toString(cadItem.getHeight()))
					;
			
			
			
		}
	}
	
	
	public void createNodes(List<JaamSimEntity> simulationNodes) {
		fillPapyursTypeMap();
		for(JaamSimEntity e : simulationNodes) {
			idToEntityMap.put(e,"_N000000000000000000"+counterNode++);
		}
		String nodesString = idToEntityMap.values().stream().collect(Collectors.joining(" "));
		packagedElement.addAttribute("node", nodesString);
		
		for(Entry< JaamSimEntity,String> item : idToEntityMap.entrySet()) {
			for(JaamSimEntity target :item.getKey().getNextComponents()){
				String edgeId = "_E000000000000000000"+counterNode++ ;
				if(targetMap.get(item.getKey())==null) {
					Map<JaamSimEntity,String> targetEdgeMap = new HashMap<>();
					targetMap.put(item.getKey(), targetEdgeMap);
				}
				
				Map<JaamSimEntity,String> targetEdgeMap =targetMap.get(item.getKey());
				targetEdgeMap.put(target, edgeId);
				
				
				packagedElement.addElement("edge")
				.addAttribute("xmi:type", "uml:ControlFlow")
				.addAttribute("xmi:id", edgeId)
				.addAttribute("target", idToEntityMap.get(target))
				.addAttribute("source", item.getValue());
			}
			
			
		}
		
		for(Entry< JaamSimEntity,String> item : idToEntityMap.entrySet()) {
			for(Map<JaamSimEntity, String> targetEntry : targetMap.values()) {
				String edgeId = targetEntry.get(item.getKey());
				if(edgeId==null) {
					continue;
				}
				if(sourceMap.get(item.getKey())==null) {
					List<String> sourceEdges = new ArrayList<String>();
					sourceEdges.add(edgeId);
					sourceMap.put(item.getKey(), sourceEdges);
				}else {
					List<String> sourceEdges =sourceMap.get(item.getKey());
					sourceEdges.add(edgeId);
				}
			}
		}
		
		for(Entry< JaamSimEntity,String> item : idToEntityMap.entrySet()) {
			String papyursType = simTypeToPayprusType.get(item.getKey().getType());
			if(item.getKey().getType()==SimulationItemTyp.Branch.toString() && item.getKey().getPreviousComponents().size()>1) {
				papyursType = "uml:MergeNode";
			}
			String outgoingString = null;
			if(targetMap.get(item.getKey())!=null) {
				outgoingString = targetMap.get(item.getKey()).values().stream().collect(Collectors.joining(" "));
			}
			String incomingString = null;
			if(sourceMap.get(item.getKey())!=null) {
				incomingString = sourceMap.get(item.getKey()).stream().collect(Collectors.joining(" "));
			}
			
			Element node = packagedElement.addElement("node")
			.addAttribute("xmi:type", papyursType)
			.addAttribute("xmi:id", item.getValue())
			.addAttribute("name", item.getKey().getName());
			if(incomingString!=null) {
				node.addAttribute("incoming",incomingString);
			}
			if(outgoingString!=null) {
				node.addAttribute("outgoing",outgoingString);
			}
		}
	}
}
