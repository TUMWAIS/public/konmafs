package generateViewModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import freeCad.FreeCadObject;
import gui.GuiConsistencRules;
import jaamSim.JaamSimEntity;
import javafx.concurrent.Task;

public class ProcessGenerateViewModelFile extends Task<Void> {
	
	Model modelWithData;
	GuiConsistencRules gui;
	private File directory;
	List<String> elmentNames;
	HashMap<String,JaamSimEntity> simulationItems = new HashMap<>();
	private String elementNameString;
	List<JaamSimEntity> simulationItemsForView = new ArrayList<>();
	List<FreeCadObject> cadItemsForView = new ArrayList<>();

	public ProcessGenerateViewModelFile(List<String> elmentNames,Model modelWithData, GuiConsistencRules gui,File directory) {
		this.elmentNames = elmentNames;
		this.modelWithData = modelWithData;
		this.gui = gui;
		this.directory = directory;
	}
	
	@Override
	protected Void call() throws Exception {
		readElementsForNames();
		return null;
	}
	
	private void readElementsForNames() {
		updateMessage("Writing view");
		String outputFilePath = directory+"\\"+WritePapyursModel.MODEL_NAME+".uml";
		readSimulationItems();
		readCadItems();
		
		WritePapyursModel papyrusModel = new WritePapyursModel();
		Document doc = papyrusModel.createHeader();
		papyrusModel.createNodes(simulationItemsForView);
		papyrusModel.fillSimulationProfiles(simulationItemsForView);
		papyrusModel.fillCADProfiles(cadItemsForView);
		OutputFormat format = OutputFormat.createPrettyPrint();
		XMLWriter writer;
		OutputStream out;
		try {
			out = new FileOutputStream(outputFilePath);
			writer = new XMLWriter(out, format);
			writer.write(doc);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		updateMessage("File: "+WritePapyursModel.MODEL_NAME+".uml"+" generated");
	}
	
	private void readCadItems() {
		
		String queryString= "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+ "PREFIX cadItem: <http://example.org/cad/cadItem#> ";
		
		queryString += " SELECT ?name ?length ?width ?height WHERE { ?s cadItem:name ?name. ";
		queryString += "  ?s cadItem:length ?length. ";
		queryString += "  ?s cadItem:width ?width. ";
		queryString += "  ?s cadItem:height ?height. ";
		queryString += " FILTER( ?name IN "+elementNameString+").}";
		
		try {
		Query query = QueryFactory.create(queryString) ;
		  try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithData)) {
		    ResultSet results = qexec.execSelect() ;
		    List<QuerySolution> querieResult = ResultSetFormatter.toList(results);
		    for(QuerySolution result : querieResult) {
		    	String name = result.getLiteral("name").toString();
		    	FreeCadObject cadItem = new FreeCadObject(name);
		    	cadItem.setLabel(name);
		    	String length = result.getLiteral("?length").toString().split("\\^\\^")[0];
		    	cadItem.setLength(Double.parseDouble(length));
		    	String width = result.getLiteral("?width").toString().split("\\^\\^")[0];
		    	cadItem.setWidth(Double.parseDouble(width));
		    	String height = result.getLiteral("?height").toString().split("\\^\\^")[0];
		    	cadItem.setHeight(Double.parseDouble(height));
		    	cadItemsForView.add(cadItem);
		    }
		}}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void readSimulationItems() {
		
		for(String name :elmentNames) {
			simulationItems.put(name, new JaamSimEntity(name));
		}
		
		elementNameString = elmentNames.stream().collect(Collectors.joining("\','", "(\'", "\')"));
		
		String queryString= "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
				+ "PREFIX simItem: <http://example.org/simulation/simulationItem#> ";
		
		queryString += " SELECT ?name ?nextName ?travelTime  ?typ WHERE { ?s simItem:name ?name. ";
		queryString += "  OPTIONAL { ?s simItem:nextSimulationItem ?nextItem. ";
		queryString += "   ?nextItem simItem:name ?nextName. }";
		queryString += "  OPTIONAL { ?s simItem:travelTime ?travelTime. }";
		queryString += "  OPTIONAL {?s simItem:previousSimulationItem ?prevItem. ";
		queryString += "  ?prevItem simItem:name ?prevName. }";
		queryString += "  ?s simItem:typ ?typ. ";
		queryString += " FILTER( ?name IN "+elementNameString+").}";
		try {
		Query query = QueryFactory.create(queryString) ;
		  try (QueryExecution qexec = QueryExecutionFactory.create(query, modelWithData)) {
		    ResultSet results = qexec.execSelect() ;
		    List<QuerySolution> querieResult = ResultSetFormatter.toList(results);
		    for(QuerySolution result : querieResult) {
		    	JaamSimEntity jaamSimEntity = simulationItems.get(result.getLiteral("name").toString());
		    	if(result.getLiteral("nextName")!=null) {
		    		JaamSimEntity nextJaamSimEntity = simulationItems.get(result.getLiteral("nextName").toString());
		    		jaamSimEntity.addNextComponent(nextJaamSimEntity);
		    	}
		    	if(result.getLiteral("prevName")!=null) {		    	
		    		JaamSimEntity prevJaamSimEntity = simulationItems.get(result.getLiteral("prevName").toString());
		    		jaamSimEntity.addPreviousComponent(prevJaamSimEntity);
		    	}
		    	
		    	if(result.getLiteral("?travelTime")!=null) {
		    		String travelTime = result.getLiteral("?travelTime").toString().split("\\^\\^")[0];
		    		jaamSimEntity.setTravelTime(travelTime);
		    	}
		    	String[] typ = result.getResource("?typ").toString().split("/");
		    	jaamSimEntity.setType(typ[typ.length-1]);
		    	simulationItemsForView.add(jaamSimEntity);	
		    }	
		  }}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
