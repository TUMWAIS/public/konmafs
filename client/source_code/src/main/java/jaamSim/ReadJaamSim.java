package jaamSim;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entities.ModelFile;

public class ReadJaamSim {
	private ModelFile jaamSimModelFile;
	private JaamSimList currentJaamSimList;
	private Map<String,JaamSimEntity> jaamSimEntities;	
	static List<String> notShowableTypes;
	
	static {
		notShowableTypes = new ArrayList<>();
		notShowableTypes.add("ColladaModel");
		notShowableTypes.add("ImageModel");
		notShowableTypes.add("DisplayEntity");
		notShowableTypes.add("EntityLabel");
		notShowableTypes.add("OverlayClock");
		notShowableTypes.add("OverlayText");
		notShowableTypes.add("View");
		notShowableTypes.add("Text");
	}
	
	public ReadJaamSim(ModelFile jaamSimModelFile) {
		this.jaamSimModelFile = jaamSimModelFile;
		currentJaamSimList = new JaamSimList();
		currentJaamSimList.setFileName(jaamSimModelFile.getFile().getPath());
		jaamSimEntities = new HashMap<>();
	}
	
	public void readJaamSimEntities() {
		readJaamSimEntitiesFromFile(jaamSimModelFile.getFile());
		currentJaamSimList.setJaamSimEntities(jaamSimEntities);
	}
	
	public JaamSimList getJaamSimList() {
		return currentJaamSimList;
	}
	
	private void readJaamSimEntitiesFromFile(File jaamSimFile){		
	    BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(jaamSimFile));
			String currentLine = reader.readLine();
		     while(currentLine!= null) {

		    	 if(currentLine.contains("Define")) {
		    		 readDefine(currentLine);
		    	 }
		    	 if(currentLine.contains("NextComponent")) {
		    		 readNextComponent(currentLine);
		    	 }
		    	 if(currentLine.contains("NextComponentList")) {
		    		 readNextComponent(currentLine);
		    	 }
		    	 if(currentLine.contains("WaitQueue")) {
		    		 readWaitQueue(currentLine);
		    	 }
		    	 if(currentLine.contains("TravelTime")) {
		    		 readTravelTime(currentLine);
		    	 }
		    	 if(currentLine.contains("ServiceTime")) {
		    		 readServiceTime(currentLine);
		    	 }
		    	 if(currentLine.contains("Position")|| currentLine.contains("Points")) {
		    		 readPosition(currentLine);
		    	 }
		    	 currentLine = reader.readLine();
		     }

		    
		     reader.close();		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readTravelTime(String line) {
		String[] travelTimeParts = line.split(" ");
		JaamSimEntity sourceEntity = jaamSimEntities.get(travelTimeParts[0]);
		sourceEntity.setTravelTime("");
		 for(int i=2;i<travelTimeParts.length;i++) {
			 if(!isDefinePartBracketOrEmpty(travelTimeParts[i].trim())) {
				sourceEntity.setTravelTime(sourceEntity.getTravelTime()+travelTimeParts[i]);
			 }
		 }
	}
	
	private void readServiceTime(String line) {
		String[] travelTimeParts = line.split(" ");
		JaamSimEntity sourceEntity = jaamSimEntities.get(travelTimeParts[0]);
		sourceEntity.setServiceTime("");
		 for(int i=2;i<travelTimeParts.length;i++) {
			 if(!isDefinePartBracketOrEmpty(travelTimeParts[i].trim())) {
				sourceEntity.setServiceTime(sourceEntity.getServiceTime()+travelTimeParts[i]);
			 }
		 }
	}

	
	private void readPosition(String line) {
		JaamSimEntity entity = jaamSimEntities.get(line.split(" ")[0]);
		if(entity==null) {
			return;
		}
		Pattern patternDoubleValues = Pattern.compile("\\d+\\.\\d+");
		Matcher m = patternDoubleValues.matcher(line);
		while(m.find()) {
			try {
				entity.getPosition().add(Double.parseDouble(m.group()));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void readWaitQueue(String line) {
		String[] waitQueueLineParts = line.split(" ");
		JaamSimEntity nextComponent = jaamSimEntities.get(waitQueueLineParts[0]);
		for(int i=2;i<waitQueueLineParts.length;i++) {
			 if(!isDefinePartBracketOrEmpty(waitQueueLineParts[i].trim())) {
				JaamSimEntity sourceEntity = jaamSimEntities.get(waitQueueLineParts[i]);
				sourceEntity.addNextComponent(nextComponent);
			 }
		 }
	}
	
	private void readNextComponent(String line) {
		String[] nextComponentLineParts = line.split(" ");
		JaamSimEntity sourceEntity = jaamSimEntities.get(nextComponentLineParts[0]);
		 for(int i=2;i<nextComponentLineParts.length;i++) {
			 if(!isDefinePartBracketOrEmpty(nextComponentLineParts[i].trim())) {
				JaamSimEntity nextComponent = jaamSimEntities.get(nextComponentLineParts[i]);
				sourceEntity.addNextComponent(nextComponent);
				nextComponent.addPreviousComponent(sourceEntity);
			 }
		 }
		
	}
	
	private void readDefine(String line) {
		 String[] defineParts = line.split(" ");
		 String type = defineParts[1];
		 boolean isShowable = true;
		 if(notShowableTypes.contains(type)) {
			 isShowable = false;
		 }
		 for(int i=2;i<defineParts.length;i++) {
			 if(!isDefinePartBracketOrEmpty(defineParts[i].trim())) {
				 String entityName = defineParts[i];
				 JaamSimEntity newJaamSimEntity = new JaamSimEntity(entityName, type,isShowable,currentJaamSimList);
				 if(isShowable) {
					 currentJaamSimList.addShowableEntityToList(newJaamSimEntity);
				 }
				 jaamSimEntities.put(entityName,newJaamSimEntity);
			 }
		 }		
	}
	
	private boolean isDefinePartBracketOrEmpty(String definePart) {
		 if((definePart.equals("{") || definePart.equals("}") || definePart.isEmpty())) {
			 return true;
		 }
		 return false;
	}

}
