package jaamSim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.EntityFileList;

public class JaamSimList extends EntityFileList<JaamSimEntity>{

	private Map<String,JaamSimEntity> jaamSimEntities = new HashMap<>();
	
	public List<JaamSimEntity> getJaamSimEntityList(){
		return new ArrayList<JaamSimEntity>(jaamSimEntities.values());
	}
	
	private List<JaamSimEntity> showableJaamSimEntities = new ArrayList<>();
	
	void addShowableEntityToList(JaamSimEntity showableEntity) {
		this.showableJaamSimEntities.add(showableEntity);
	}
	
	@Override
	public List<JaamSimEntity> getShowableEntityList() {
		return showableJaamSimEntities;
	}
	
	void setShowableEntityList(List<JaamSimEntity> showableJaamSimEntities) {
		this.showableJaamSimEntities = showableJaamSimEntities;
	}

	public Map<String, JaamSimEntity> getJaamSimEntities() {
		return jaamSimEntities;
	}

	public void setJaamSimEntities(Map<String, JaamSimEntity> jaamSimEntities) {
		this.jaamSimEntities = jaamSimEntities;
	}

}
