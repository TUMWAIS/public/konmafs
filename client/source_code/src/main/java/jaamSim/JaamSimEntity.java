package jaamSim;

import java.util.ArrayList;
import java.util.List;

import entities.EntityFileList;

public class JaamSimEntity {
	
	private String name;
	private String type;
	private ArrayList<JaamSimEntity> nextComponents = new ArrayList<>();
	private ArrayList<JaamSimEntity> previousComponents = new ArrayList<>();
	private List<Double> position = new ArrayList<Double>();
	private String travelTime;
	private String serviceTime;
	

	private boolean showable;
	private EntityFileList<JaamSimEntity> fileListOfEntity;
	
	public JaamSimEntity(String name) {this.name = name;}

	public JaamSimEntity(String name, String type,boolean showable,EntityFileList<JaamSimEntity> fileListOfEntity) {
		this.name = name;
		this.type = type;
		this.showable = showable;
		this.setFileListOfEntity(fileListOfEntity);
	}
	
	public String toString() {
		return name+ "("+type+")";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<JaamSimEntity> getNextComponents() {
		return nextComponents;
	}

	public void addNextComponent(JaamSimEntity nextComponent) {
		nextComponents.add(nextComponent);
	}
	
	public void addPreviousComponent(JaamSimEntity previousComponent) {
		previousComponents.add(previousComponent);
	}
	
	

	public List<Double> getPosition() {
		return position;
	}

	public void setPosition(List<Double> position) {
		this.position = position;
	}

	public boolean isShowable() {
		return showable;
	}

	public void setShowable(boolean showable) {
		this.showable = showable;
	}

	public String getTravelTime() {
		return travelTime;
	}

	public void setTravelTime(String travelTime) {
		this.travelTime = travelTime;
	}

	public EntityFileList<JaamSimEntity> getFileListOfEntity() {
		return fileListOfEntity;
	}

	public void setFileListOfEntity(EntityFileList<JaamSimEntity> fileListOfEntity) {
		this.fileListOfEntity = fileListOfEntity;
	}
	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public ArrayList<JaamSimEntity> getPreviousComponents() {
		return previousComponents;
	}

	public void setPreviousComponents(ArrayList<JaamSimEntity> previousComponents) {
		this.previousComponents = previousComponents;
	}
	
	
	
}
