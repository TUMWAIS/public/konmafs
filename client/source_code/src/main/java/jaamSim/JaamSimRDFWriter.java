package jaamSim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import rdf.RDFBaseModel;

public class JaamSimRDFWriter {
	public static final String BASE_URI = "http://example.org";
	public static final String SIM_NS = BASE_URI + "/"+ "simulation";
	public static final String SIM_ITEM_PROP_NS = SIM_NS+"/"+"simulationItem#";
	public static final String SIM_ITEM_COLLECTION_NS = SIM_NS + "/"+ "simulationItem";
	public static final String UNIT_NS = "http://example.org/unit";
	public static final String SIM_ITEM_PROPERTY_URI_NAME = SIM_ITEM_PROP_NS+ "name";
	public static final String SIM_ITEM_PROPERTY_URI_TYP = SIM_ITEM_PROP_NS+ "typ";
	public static final String SIM_ITEM_PROPERTY_URI_NEXT_SIM_ITEM = SIM_ITEM_PROP_NS+ "nextSimulationItem";
	public static final String SIM_ITEM_PROPERTY_URI_PREV_SIM_ITEM = SIM_ITEM_PROP_NS+ "previousSimulationItem";
	public static final String SIM_ITEM_PROPERTY_TRAVEL_TIME = SIM_ITEM_PROP_NS+ "travelTime";
	public static final String SIM_ITEM_PROPERTY_SERVICE_TIME = SIM_ITEM_PROP_NS+ "serviceTime";
	public static final String SIM_ITEM_PROPERTY_TIME_UNIT = SIM_ITEM_PROP_NS+ "timeUnit";
	public static final String SIM_ITEM_TYPE_NS = BASE_URI+"/"+"simulationItemTyp";
	
	public static final String SIM_ITEM_TYPE_PREFIX = "simItemType_";
	private Map<String,Resource> resourceMap = new HashMap<>();
	
	Property simItemPropertyName;
	Property simItemPropertyType;
	Property simItemPropertyNextSimItem;
	Property simItemPropertyPreviousSimItem;
	Property simItemPropertyTravelTime;
	Property simItemPropertyServiceTime;
	Property simItemPropertyTimeUnit;

	public Model model;
	private RDFBaseModel baseModel;
	
	public static void resetCounter() {
		simItemCounter=1;
	}
	
	private static int simItemCounter;
	
	public JaamSimRDFWriter(RDFBaseModel baseModel) {
		this.baseModel = baseModel;
		this.model = baseModel.getModel();
		init();
	}

	private void init() {
		model.setNsPrefix( "simItem", SIM_ITEM_PROP_NS );

		simItemPropertyName = model.createProperty( SIM_ITEM_PROPERTY_URI_NAME );
		simItemPropertyType = model.createProperty( SIM_ITEM_PROPERTY_URI_TYP );
		simItemPropertyNextSimItem = model.createProperty( SIM_ITEM_PROPERTY_URI_NEXT_SIM_ITEM );
		simItemPropertyPreviousSimItem = model.createProperty( SIM_ITEM_PROPERTY_URI_PREV_SIM_ITEM );
		simItemPropertyTravelTime = model.createProperty( SIM_ITEM_PROPERTY_TRAVEL_TIME );
		simItemPropertyServiceTime = model.createProperty( SIM_ITEM_PROPERTY_SERVICE_TIME );
		simItemPropertyTimeUnit = model.createProperty( SIM_ITEM_PROPERTY_TIME_UNIT );
		
		for(SimulationItemTyp type : SimulationItemTyp.values()) {
			Resource nodeTypeResource = model.createResource(SIM_ITEM_TYPE_NS+"/"+type.toString());
			resourceMap.put(SIM_ITEM_TYPE_PREFIX+type.toString(), nodeTypeResource);
		}
		
		
	}
	
	public void writeJaamSimItemsToRDFModel(List<JaamSimEntity> jaamSimItems, File file) {
		for(JaamSimEntity jaamSimItem : jaamSimItems) {
			Resource simItemSubject = model.createResource(SIM_ITEM_COLLECTION_NS+"/"+"simulationItem_"+simItemCounter++);
			resourceMap.put(jaamSimItem.getName(), simItemSubject);
			
		}
		
		for(JaamSimEntity jaamSimItem : jaamSimItems) {
			Resource simTypeResource = resourceMap.get(SIM_ITEM_TYPE_PREFIX+jaamSimItem.getType());
			if(simTypeResource == null) {
				continue;
			}
			Resource simItemSubject = resourceMap.get(jaamSimItem.getName());
			model.add(simItemSubject,simItemPropertyName,jaamSimItem.getName());
			
			model.add(simItemSubject,simItemPropertyType,simTypeResource);
			for(JaamSimEntity nextItem : jaamSimItem.getNextComponents()) {
				if(resourceMap.get(nextItem.getName())!=null) {
					model.add(simItemSubject,simItemPropertyNextSimItem,resourceMap.get(nextItem.getName()));
				}
			}
			
			for(JaamSimEntity prevItem : jaamSimItem.getPreviousComponents()) {
				if(resourceMap.get(prevItem.getName())!=null) {
					model.add(simItemSubject,simItemPropertyPreviousSimItem,resourceMap.get(prevItem.getName()));
				}
			}
			if(jaamSimItem.getTravelTime()!= null) {
				String travelTimeString = jaamSimItem.getTravelTime().split("[a-z]")[0];
				model.add(simItemSubject,simItemPropertyTravelTime,model.createTypedLiteral(Double.valueOf(travelTimeString)));
				
				if(jaamSimItem.getTravelTime().contains("ms")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_ms);
				}
				else if(jaamSimItem.getTravelTime().contains("s")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_s);
				}
				else if(jaamSimItem.getTravelTime().contains("h")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_h);
				}
				else if(jaamSimItem.getTravelTime().contains("min")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_min);
				}
			}
			if(jaamSimItem.getServiceTime()!= null) {
				String serviceTimeString = jaamSimItem.getServiceTime().split("[a-z]")[0];
				model.add(simItemSubject,simItemPropertyServiceTime,model.createTypedLiteral(Double.valueOf(serviceTimeString)));
				
				if(jaamSimItem.getServiceTime().contains("ms")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_ms);
				}
				else if(jaamSimItem.getServiceTime().contains("s")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_s);
				}
				else if(jaamSimItem.getServiceTime().contains("h")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_h);
				}
				else if(jaamSimItem.getServiceTime().contains("min")) {
					model.add(simItemSubject,simItemPropertyTimeUnit,baseModel.unit_min);
				}
			}
		}

		if(file!=null) {
			try {
				FileOutputStream fop = new FileOutputStream(file);
				RDFDataMgr.write(fop, model, Lang.RDFXML);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
