package emf;

import java.util.List;

public class EmfClassInstance {
	String name;
	List<String> structuralFeatures;
	
	public EmfClassInstance(String name, List<String> structuralFeatures) {
		this.name = name;
		this.structuralFeatures = structuralFeatures;
	}
	
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Class: " + name + "\n");
		if(structuralFeatures.size() > 0) {
			stringBuilder.append("    Features: ");
			for(String feature : structuralFeatures) {
				stringBuilder.append(feature + ", ");
			}
			String s = stringBuilder.toString();
			if(s.substring(s.length() - 2, s.length()).equals(", ")) {
				s = s.substring(0, s.length() - 2);
			}
			return s;
		} else return stringBuilder.toString().trim();
	}
	
}
