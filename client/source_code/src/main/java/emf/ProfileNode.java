package emf;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

public class ProfileNode {
	List<ProfileNode> children = new ArrayList<>();
	Element content;
	
	public ProfileNode(Element content) {
		this.content = content;
	}

	public void appendChild(ProfileNode node) {
		this.children.add(node);
	}
}
