package emf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class EmfFile extends EmfDocument {
	enum DocType {
		MODEL, PROFILE;
	}

	List<EmfProfileInstance> profileInstances;
	Map<String, String> associations;
	DocType docType; 
	
	public List<EmfNode> allNodes;
	public List<EmfEdge> allEdges;
	public List<String> acitivatingNodeIds;
	public List<EmfNode> activatingNodes;
	public List<EmfNode> notActiveNodes;
	public ArrayList<EmfNode> nodeWithIncommingFromNotActive;
	
	
	
	public EmfNode getEmfNodeByEdgeId(String edgeId,boolean target) {
		if(allEdges.stream().filter(edge -> edge.id.equals(edgeId)).findFirst().isPresent()) {
			EmfEdge edgeNode = allEdges.stream().filter(edge -> edge.id.equals(edgeId)).findFirst().get();
			if(target) {
				return getEmfNodeForId(edgeNode.target);
			}
			else{
				return getEmfNodeForId(edgeNode.source);
			}
		}
		return null;
	}
	
	public  EmfNode getEmfNodeForId(String id) {
		if(allNodes.stream().filter(node -> node.id.equals(id)).findFirst().isPresent()) {
			return allNodes.stream().filter(node -> node.id.equals(id)).findFirst().get();
		}
		else {
			return null;
		}
	}
	
	public EmfFile() {
		this.profileInstances = new ArrayList<>();
	}
	
	public List<EmfProfileInstance> insertProfileInstance(String name, List<EmfClassInstance> classInstances) {
		EmfProfileInstance profileInstance = new EmfProfileInstance(name, classInstances);
		profileInstances.add(profileInstance);
		return profileInstances;
	} 
	
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Profile name: " + name + "\n");
		for(EmfProfileInstance emfProfileInstance : profileInstances) {
			stringBuilder.append(emfProfileInstance.toString() + "\n");
		}
		return stringBuilder.toString().trim();
	}
}
