package emf;

import java.util.ArrayList;
import java.util.List;

/*
 * Representation of an EmfNode found in emf Model files. Contains 2 lists, one about incoming and 
 * one about outgoing nodes.
 */
public class EmfNode {
	public String name, type, id;
	public List<String> incoming, outgoing;
	public List<String> exceptionInput = new ArrayList<>();
	public String first;
	public String second;
	public List<TagValue> appliedValues = new ArrayList<>();
	public List<String> appliedProfils = new ArrayList<>();
	public boolean isVisited = false;
	public List<EmfNode> incommingNotActiveNodes = new ArrayList<>();

	
	public EmfNode(String name, String type,String id, List<String> incoming, List<String> outgoing) {
		this.name = name;
		this.type = type;
		this.id = id;
		this.incoming = incoming;
		this.outgoing = outgoing;
	}
	
	public String toString() {
		return name + "[" + type + "] incomming:"+incoming.toString();
	}
	
	public boolean equals(Object obj) {
		String otherId = ((EmfNode)obj).id;
		if(id.equals(otherId)) {
			return true;
		}
		return false;
	}
}
