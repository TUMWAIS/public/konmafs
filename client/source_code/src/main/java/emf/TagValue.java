package emf;

public class TagValue {
	public String tag;
	public String value;
	public String profil;
	
	public TagValue(String profil, String tag, String value) {
		this.profil = profil;
		this.tag = tag;
		this.value = value;
	}	
}
