package emf;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class EmfParser {

	public	Document doc; 
	public EmfFile emfFile = new EmfFile(); 
	

	public void parseDocument(File file) {

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setNamespaceAware(true);
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(file);
			doc.getDocumentElement().normalize();
			
			emfFile.name = file.getName();
			
			if(getDocType() == EmfFile.DocType.MODEL) {
				emfFile = (EmfFile) emfFile;
			}
			emfFile.docType = getDocType();
			if(emfFile.docType.equals(EmfFile.DocType.PROFILE)) {
				NodeList rootChildren = doc.getDocumentElement().getChildNodes();
				Element rootProfile = findFirstChildElement(rootChildren, "uml:Profile");
				getAllClassesAndAttributesForProfile(rootProfile);
				emfFile.associations = getAllAssociations(rootProfile);
			} else if(emfFile.docType.equals(EmfFile.DocType.MODEL)){
				emfFile.allNodes = getAllNodes();
				emfFile.allEdges = getAllEdges();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private EmfFile.DocType getDocType() {
		NodeList rootChildren = this.doc.getDocumentElement().getChildNodes();
		if(findFirstChildElement(rootChildren, "uml:Profile") != null) {
			return EmfFile.DocType.PROFILE;
		} else if(findFirstChildElement(rootChildren, "uml:Model") != null) {
			return EmfFile.DocType.MODEL;
		} else return null;
	}
	
	private List<EmfNode> getAllNodes() {
		List<EmfNode> emfNodes = new ArrayList<>();
		if(emfFile.docType != EmfFile.DocType.MODEL) {
			return null;
		}
		Element rootModel = findFirstChildElement(this.doc.getDocumentElement().getChildNodes(), "uml:Model");
		Element rootPackagedElement = findFirstChildElement(rootModel.getChildNodes(), "packagedElement");
		List<Element> nodes = findAllChildElements(rootPackagedElement.getChildNodes(), "node");
		nodes.addAll(findAllChildElements(rootPackagedElement.getChildNodes(), "ownedBehavior"));

		for (Element node : nodes) {
			List<String> incoming = Arrays.asList(node.getAttribute("incoming").split(" ")); 
			List<String> outgoing = Arrays.asList(node.getAttribute("outgoing").split(" "));
			String name = node.getAttribute("name");
			String type = node.getAttribute("xmi:type");
			String id = node.getAttribute("xmi:id");
			EmfNode newNode = new EmfNode(name, type,id, incoming, outgoing);
			emfNodes.add(newNode);
			if(node.hasChildNodes()) {
				List<Element> handlerNodes = findAllChildElements(node.getChildNodes(), "handler");
				if(!handlerNodes.isEmpty()) {
					newNode.exceptionInput.add(handlerNodes.get(handlerNodes.size()-1).getAttribute("exceptionInput"));
					continue;
				}
				else if(type.equals("uml:TestIdentityAction")) {
					List<Element> firstNodes = findAllChildElements(node.getChildNodes(), "first");
					newNode.first = firstNodes.get(0).getAttribute("xmi:id");
					List<Element> secondNodes = findAllChildElements(node.getChildNodes(), "second");
					newNode.second = secondNodes.get(0).getAttribute("xmi:id");
				}
			}
		}
		addAppliedProfilToNode(emfNodes,this.doc.getDocumentElement().getChildNodes());
		return emfNodes;
	}
	
	private List<EmfEdge> getAllEdges(){
		List<EmfEdge> emfEdges = new ArrayList<>();
		if(emfFile.docType != EmfFile.DocType.MODEL) {
			return null;
		}
		Element rootModel = findFirstChildElement(this.doc.getDocumentElement().getChildNodes(), "uml:Model");
		Element rootPackagedElement = findFirstChildElement(rootModel.getChildNodes(), "packagedElement");
		List<Element> edges = findAllChildElements(rootPackagedElement.getChildNodes(), "edge");
		for (Element edge : edges) {

			String type = edge.getAttribute("xmi:type");
			String id = edge.getAttribute("xmi:id");
			String target = edge.getAttribute("target");
			String source = edge.getAttribute("source");
			emfEdges.add(new EmfEdge(source,target,type,id));
		}
		return emfEdges;
	}
	
	private EmfNode getEmfNodeForId(String id,List<EmfNode> emfNodes) {
		return emfNodes.stream().filter(node -> node.id.equals(id)).findFirst().get();
	}
	
	private void addAppliedProfilToNode(List<EmfNode> emfNodes,NodeList childNodes) {
		for(int j=0;j<childNodes.getLength();j++ ) {
			Node childNode = childNodes.item(j);
			if((childNode.getNodeType() == Node.ELEMENT_NODE)) {
				Element currentChildNode = (Element)childNode;
				if(currentChildNode.getTagName().equals("uml:Model")) {
					continue;
				}
				String profilName = currentChildNode.getNodeName();
				EmfNode nodeForId = null;
				List<TagValue> appliedValues = new ArrayList<>();
				for(int i=0;i<currentChildNode.getAttributes().getLength();i++) {					
					Node attribute = currentChildNode.getAttributes().item(i);
					if(attribute.getNodeName().contains("base_")) {
						String nodeIdOfAppliedProfil = attribute.getNodeValue();
						try {
						nodeForId = getEmfNodeForId(nodeIdOfAppliedProfil,emfNodes);
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
					else if(!attribute.getNodeName().equals("xmi:id")) {
						appliedValues.add(new TagValue(profilName,attribute.getNodeName(), attribute.getNodeValue()));
					}
				}
				if(nodeForId!=null) {
					nodeForId.appliedProfils.add(profilName);
					nodeForId.appliedValues.addAll(appliedValues);
				}
				
				
			}
		}
	}
	
	private Map<String, String> getAllAssociations(Element elem) {
		Map<String, String> associations = new HashMap<>();
		List<Element> packagedElements = findAllChildElements(elem.getChildNodes(), "packagedElement");
		for (Element packagedElement : packagedElements) {
			if (packagedElement.hasAttribute("xmi:type") & packagedElement.getAttribute("xmi:type").equals("uml:Association")) {
				List<Element> ownedEnds = findAllChildElements(packagedElement.getChildNodes(), "ownedEnd");
				if(ownedEnds.size() == 2) {
					associations.put(ownedEnds.get(0).getAttribute("name"), ownedEnds.get(1).getAttribute("name"));
				}
			}
		}
		return associations.size() > 0 ? associations : null;
	}
	
	private EmfFile getAllClassesAndAttributesForProfile(Element elem) {
		List<Element> packagedElements = findAllChildElements(elem.getChildNodes(), "packagedElement");
		for (Element packagedElement : packagedElements) {
			if (packagedElement.hasAttribute("xmi:type")
					& packagedElement.getAttribute("xmi:type").equals("uml:Profile")
					& packagedElement.hasAttribute("name")) {
				NodeList contentsChildren = findFirstChildElement(
						findFirstChildElement(packagedElement.getChildNodes(), "eAnnotations").getChildNodes(),
						"contents").getChildNodes();
				List<Element> classes = findAllChildElements(contentsChildren, "eClassifiers");
				List<EmfClassInstance> classInstances = new ArrayList<>();
				for (Element classElement : classes) {
					if (classElement.hasAttribute("xmi:type")
							& classElement.getAttribute("xmi:type").equals("ecore:EClass")
							& classElement.hasAttribute("name")) {
						String className = classElement.getAttribute("name");
						List<String> structuralFeaturesContained = new ArrayList<>();
						List<Element> structuralFeatures = findAllChildElements(classElement.getChildNodes(),
								"eStructuralFeatures");
						for (Element feature : structuralFeatures) {
							if (feature.hasAttribute("name"))
								structuralFeaturesContained.add(feature.getAttribute("name"));
						}
						classInstances.add(new EmfClassInstance(className, structuralFeaturesContained));
					}
				}
				emfFile.insertProfileInstance(packagedElement.getAttribute("name"), classInstances);
				getAllClassesAndAttributesForProfile(packagedElement);
			}
		}
		emfFile.name = elem.getAttribute("name");
		return emfFile;
	}
	
	private Element findFirstChildElement(NodeList nodes, String tagName) {
		for (int i = 0; i < nodes.getLength(); i++) {
			Node currentNode = nodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element) currentNode;
				if (currentElement.getTagName().equals(tagName)) {
					return currentElement;
				}
			}
		}
		return null;
	}

	private List<Element> findAllChildElements(NodeList nodes, String tagName) {
		List<Element> foundElements = new ArrayList<Element>();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node currentNode = nodes.item(i);
			if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
				Element currentElement = (Element) currentNode;
				if (currentElement.getTagName().equals(tagName)) {
					foundElements.add(currentElement);
				}
			}
			if(currentNode.hasChildNodes()) {
				foundElements.addAll(findAllChildElements(currentNode.getChildNodes(),tagName));
			}
		}
		return foundElements;
	}
}