package emf;

import java.util.List;


public class EmfProfileInstance {
	String name;
	List<EmfClassInstance> classInstances; 
	
	public EmfProfileInstance(String name, List<EmfClassInstance> classInstances) {
		this.name = name;
		this.classInstances = classInstances;
	}
	
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Profile: " + name + "\n");
		for(EmfClassInstance emfClassInstance : classInstances) {
			stringBuilder.append("  " + emfClassInstance.toString() + "\n");
		}
		return stringBuilder.toString().trim();
	}
}
