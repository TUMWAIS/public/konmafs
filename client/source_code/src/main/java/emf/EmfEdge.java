package emf;

/*
 * Representation of an edge in an emf file. 
 */
public class EmfEdge {
	public String source, target;
	public String type,id;

	public EmfEdge(String source, String target, String type, String id) {
		this.source = source;
		this.target = target;
		this.type = type;
		this.id = id;
	}
	
	
	
}