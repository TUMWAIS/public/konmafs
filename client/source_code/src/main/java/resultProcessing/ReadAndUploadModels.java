package resultProcessing;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;

import bpmn.ReadBPMN;
import bpmn.bpmnEntities.BPMNNodeEntity;
import bpmn.bpmnEntities.BPMNRdfWriter;
import componentList.ComponentListRDFWriter;
import componentList.ReadComponentList;
import entities.ModelFile;
import freeCad.CadRDFWriter;
import freeCad.ReadFreeCad;
import jaamSim.JaamSimRDFWriter;
import jaamSim.ReadJaamSim;
import plc.PLCVariableRDFWriter;
import plc.ReadPLCVariable;
import rdf.RDFBaseModel;
import requirementList.ReadRequirementList;
import requirementList.RequirementListRDFWriter;
import restClient.ServerCommunication;
import simulation.ReadSimElementLogger;
import simulation.ReadSimulationReport;
import simulation.SimulationElementLoggerRDFWriter;
import simulation.SimulationReportRDFWriter;

public class ReadAndUploadModels {
	ServerCommunication sendToServer;
	RDFBaseModel clientModel = new RDFBaseModel();
	private List<String> filledViews = new ArrayList<>();
	
	public Model getClientModel() {
		return clientModel.getModel();
	}

	public ReadAndUploadModels(ServerCommunication sendToServer) {
		this.sendToServer = sendToServer;
	}
	
	public static void resetCounters() {
		ComponentListRDFWriter.resetCounter();
		JaamSimRDFWriter.resetCounter();
		BPMNRdfWriter.resetCounter();
		CadRDFWriter.resetCounter();
		SimulationReportRDFWriter.resetCounter();
		RequirementListRDFWriter.resetCounter();
		SimulationElementLoggerRDFWriter.resetCounter();
		PLCVariableRDFWriter.resetCounter();
	}
	
	public void readAndUploadModels(List<ModelFile> modelFiles,List<String> rdfFileNamesOnServer) {
		resetCounters();		
		
		List<ModelFile> allLogFiles = new ArrayList<>();
		ModelFile jaamSimConfigFile = null;


		for(ModelFile modelFile : modelFiles) {			
			if(modelFile.isFileOfType(".FCStd")) {
				filledViews.add("3D-CAD");
				ReadFreeCad readFreeCad = new ReadFreeCad(modelFile);
				readFreeCad.readFreeCadEntity();
				RDFBaseModel baseModel = new RDFBaseModel();
				CadRDFWriter cadRdfWirter = new CadRDFWriter(baseModel);
				cadRdfWirter.writeCADElementsToRdfModel(readFreeCad.getDocumentContent().getVisibleObjects(),null);
				cadRdfWirter.writeCADGroupElementsToRdfModel(readFreeCad.getDocumentContent().getGroups());
				clientModel.getModel().add(baseModel.getModel());
				
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"cadRDF.xml"));
			}
			if(modelFile.isFileOfType(".bpmn")) {
				filledViews.add("BPMN");
				ReadBPMN readBPMN = new ReadBPMN(modelFile);
				readBPMN.readBPMNFile();
				RDFBaseModel baseModel = new RDFBaseModel();
				BPMNRdfWriter bpmnRdfWirter = new BPMNRdfWriter(baseModel);
				ArrayList<BPMNNodeEntity> allBPMNNodes = new ArrayList<>();
				allBPMNNodes.addAll(readBPMN.getBPMNEntity().getBPMNNodes().values());
				bpmnRdfWirter.writeBPMNodesToRDF(allBPMNNodes,null);
				
				clientModel.getModel().add(baseModel.getModel());
				
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"bpmnRDF.xml"));
			}
			if(modelFile.isFileOfType(".xlsx")&&!(modelFile.toString().toLowerCase().contains("requirement"))) {
				filledViews.add("Componentlist");
				ReadComponentList readComponentList = new ReadComponentList(modelFile);
				readComponentList.readCompontenListFile();
				RDFBaseModel baseModel = new RDFBaseModel();
				ComponentListRDFWriter componentListRDFWriter = new ComponentListRDFWriter(baseModel);
				componentListRDFWriter.writeComponentListItemsToRDFModel(readComponentList.getComponentList().getComponents(),null);
				clientModel.getModel().add(baseModel.getModel());
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"componentListRDF.xml"));
			}
			if(modelFile.isFileOfType(".xlsx")&&(modelFile.toString().toLowerCase().contains("requirement"))) {
				filledViews.add("Requirement");
				ReadRequirementList readRequirementList = new ReadRequirementList(modelFile);
				readRequirementList.readRequirementListFile();
				RDFBaseModel baseModel = new RDFBaseModel();
				RequirementListRDFWriter requirementListRDFWriter = new RequirementListRDFWriter(baseModel);
				requirementListRDFWriter.writeRequirementListItemsToRDFModel(readRequirementList.getRequirementList().getShowableEntityList(), null);
				
				if(sendToServer!=null)
				clientModel.getModel().add(baseModel.getModel());
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"requirementRDF.xml"));
			}
			if(modelFile.isFileOfType(".cfg")) {
				filledViews.add("Simulation");
				jaamSimConfigFile = modelFile;
				ReadJaamSim readJaamSim = new ReadJaamSim(modelFile);
				readJaamSim.readJaamSimEntities();
				RDFBaseModel baseModel = new RDFBaseModel();
				JaamSimRDFWriter jaamSimRDFWriter = new JaamSimRDFWriter(baseModel);
				jaamSimRDFWriter.writeJaamSimItemsToRDFModel(readJaamSim.getJaamSimList().getShowableEntityList(), null);
				
				clientModel.getModel().add(baseModel.getModel());
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"simulationRDF.xml"));
			}
			if(modelFile.isFileOfType(".xml") || modelFile.isFileOfType(".txt")) {
				filledViews.add("PLC");
				ReadPLCVariable readPLCVariable = new ReadPLCVariable(modelFile);
				readPLCVariable.readPLCFileForConveyorProperties();
				RDFBaseModel baseModel = new RDFBaseModel();
				PLCVariableRDFWriter plcVariableRDFWriter = new PLCVariableRDFWriter(baseModel);
				plcVariableRDFWriter.writePLCVariableOfConvyeorPropertiesToRDFModel(readPLCVariable.getConveyorPropertyList());
				clientModel.getModel().add(baseModel.getModel());
				
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"plcVariableRDF.xml"));
			}
			if(modelFile.isFileOfType(".rep")) {
				filledViews.add("Simulation Report");
				ReadSimulationReport readSimulationReport = new ReadSimulationReport(modelFile); 
				readSimulationReport.readSimulationReportFile();
				RDFBaseModel baseModel = new RDFBaseModel();
				SimulationReportRDFWriter simReportRDFWriter = new SimulationReportRDFWriter(baseModel);
				simReportRDFWriter.writeSimReportItemsToRDFModel(readSimulationReport.getSimReportList().getShowableEntityList(), null);
				
				clientModel.getModel().add(baseModel.getModel());
				if(sendToServer!=null)
				rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"simReportRDF.xml"));
				
			}
			if(modelFile.isFileOfType(".log")) {
				allLogFiles.add(modelFile);				
			}
		}
		if(jaamSimConfigFile!=null) {
			List<ReadSimElementLogger> loggerReaders = new ArrayList<>();
			for(ModelFile logFile : allLogFiles) { 
				ReadSimElementLogger readSimElementLogger = new ReadSimElementLogger(logFile,jaamSimConfigFile);
				readSimElementLogger.readLoggerFile();
				loggerReaders.add(readSimElementLogger);
			}
			RDFBaseModel baseModel = new RDFBaseModel();
			SimulationElementLoggerRDFWriter simLogWriter = new SimulationElementLoggerRDFWriter(baseModel);
			simLogWriter.writeSimLogItemsToRDFModel(loggerReaders);
			
			clientModel.getModel().add(baseModel.getModel());
			
			if(sendToServer!=null)
			rdfFileNamesOnServer.add(sendToServer.sendRDF(baseModel,"simLogRDF.xml"));
		}
	}

	public List<String> getFilledViews() {
		return filledViews;
	}

	public void setFilledViews(List<String> filledViews) {
		this.filledViews = filledViews;
	}
}
