package resultProcessing;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "ruleId", "ruleDefinitionLink", "models", "message"})
public class ResultObject {

	@JsonProperty("ruleId")
	private Integer ruleId;
	@JsonProperty("ruleDefinitionLink")
	private String ruleDefinitionLink;
	@JsonProperty("models")
	private List<Model> models = null;
	@JsonProperty("message")
	private String message;

	public ResultObject() {
	}

	/**
	 *
	 * @param severity
	 * @param models
	 * @param parts
	 * @param ruleId
	 * @param message
	 * @param ruleDefinitionLink
	 */
	public ResultObject(Integer ruleId, String ruleDefinitionLink, List<Model> models, String message,
			String severity) {
		super();
		this.ruleId = ruleId;
		this.ruleDefinitionLink = ruleDefinitionLink;
		this.models = models;
		this.message = message;
	}

	@JsonProperty("ruleId")
	public Integer getRuleId() {
		return ruleId;
	}

	@JsonProperty("ruleId")
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	@JsonProperty("ruleDefinitionLink")
	public String getRuleDefinitionLink() {
		return ruleDefinitionLink;
	}

	@JsonProperty("ruleDefinitionLink")
	public void setRuleDefinitionLink(String ruleDefinitionLink) {
		this.ruleDefinitionLink = ruleDefinitionLink;
	}

	@JsonProperty("models")
	public List<Model> getModels() {
		return models;
	}

	@JsonProperty("models")
	public void setModels(List<Model> models) {
		this.models = models;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	@JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(ResultObject.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this)))
				.append('[');
		sb.append("ruleId");
		sb.append('=');
		sb.append(((this.ruleId == null) ? "<null>" : this.ruleId));
		sb.append(',');
		sb.append("ruleDefinitionLink");
		sb.append('=');
		sb.append(((this.ruleDefinitionLink == null) ? "<null>" : this.ruleDefinitionLink));
		sb.append(',');
		sb.append("models");
		sb.append('=');
		sb.append(((this.models == null) ? "<null>" : this.models));
		sb.append(',');
		sb.append("message");
		sb.append('=');
		sb.append(((this.message == null) ? "<null>" : this.message));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = ((result * 31) + ((this.models == null) ? 0 : this.models.hashCode()));
		result = ((result * 31) + ((this.ruleId == null) ? 0 : this.ruleId.hashCode()));
		result = ((result * 31) + ((this.message == null) ? 0 : this.message.hashCode()));
		result = ((result * 31) + ((this.ruleDefinitionLink == null) ? 0 : this.ruleDefinitionLink.hashCode()));
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ResultObject) == false) {
			return false;
		}
		ResultObject rhs = ((ResultObject) other);
		return ((((((
				 ((this.models == rhs.models) || ((this.models != null) && this.models.equals(rhs.models))))
				)
				&& ((this.ruleId == rhs.ruleId) || ((this.ruleId != null) && this.ruleId.equals(rhs.ruleId))))
				&& ((this.message == rhs.message) || ((this.message != null) && this.message.equals(rhs.message))))
				&& ((this.ruleDefinitionLink == rhs.ruleDefinitionLink) || ((this.ruleDefinitionLink != null)
						&& this.ruleDefinitionLink.equals(rhs.ruleDefinitionLink)))));
	}

}