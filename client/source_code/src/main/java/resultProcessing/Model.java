package resultProcessing;

import java.io.Serializable;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "view", "element name", "property", "oldValue","newValue" })
@Generated("jsonschema2pojo")
public class Model implements Serializable {

	@JsonProperty("view")
	private String view;
	@JsonProperty("element name")
	private String elementName;
	@JsonProperty("property")
	private String property;
	@JsonProperty("oldValue")
	private String oldValue;
	@JsonProperty("newValue")
	private String newValue;
	private final static long serialVersionUID = -2799210471014191392L;

	@JsonProperty("view")
	public String getView() {
		return view;
	}

	@JsonProperty("view")
	public void setView(String view) {
		this.view = view;
	}

	@JsonProperty("element name")
	public String getElementName() {
		return elementName;
	}

	@JsonProperty("element name")
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	@JsonProperty("property")
	public String getProperty() {
		return property;
	}

	@JsonProperty("property")
	public void setProperty(String property) {
		this.property = property;
	}

	@JsonProperty("oldValue")
	public String getOldValue() {
		return oldValue;
	}

	@JsonProperty("oldValue")
	public void setValue(String oldValue) {
		this.oldValue = oldValue;
	}
	
	@JsonProperty("newValue")
	public String getNewValue() {
		return newValue;
	}

	@JsonProperty("newValue")
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Model.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this)))
				.append('[');
		sb.append("view");
		sb.append('=');
		sb.append(((this.view == null) ? "<null>" : this.view));
		sb.append(',');
		sb.append("elementName");
		sb.append('=');
		sb.append(((this.elementName == null) ? "<null>" : this.elementName));
		sb.append(',');
		sb.append("property");
		sb.append('=');
		sb.append(((this.property == null) ? "<null>" : this.property));
		sb.append(',');
		sb.append("oldValue");
		sb.append('=');
		sb.append(((this.oldValue == null) ? "<null>" : this.oldValue));
		sb.append(',');
		sb.append("newValue");
		sb.append('=');
		sb.append(((this.newValue == null) ? "<null>" : this.newValue));
		sb.append(',');
		if (sb.charAt((sb.length() - 1)) == ',') {
			sb.setCharAt((sb.length() - 1), ']');
		} else {
			sb.append(']');
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = ((result * 31) + ((this.property == null) ? 0 : this.property.hashCode()));
		result = ((result * 31) + ((this.view == null) ? 0 : this.view.hashCode()));
		result = ((result * 31) + ((this.oldValue == null) ? 0 : this.oldValue.hashCode()));
		result = ((result * 31) + ((this.newValue == null) ? 0 : this.newValue.hashCode()));
		result = ((result * 31) + ((this.elementName == null) ? 0 : this.elementName.hashCode()));
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Model) == false) {
			return false;
		}
		Model rhs = ((Model) other);
		return ((((((this.property == rhs.property) || ((this.property != null) && this.property.equals(rhs.property)))
				&& ((this.view == rhs.view) || ((this.view != null) && this.view.equals(rhs.view))))
				&& ((this.oldValue == rhs.oldValue) || ((this.oldValue != null) && this.oldValue.equals(rhs.oldValue))))
				&& ((this.newValue == rhs.newValue) || ((this.newValue != null) && this.newValue.equals(rhs.newValue))))
				&& ((this.elementName == rhs.elementName)
						|| ((this.elementName != null) && this.elementName.equals(rhs.elementName))));
	}
}
