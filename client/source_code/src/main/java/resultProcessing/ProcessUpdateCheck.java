package resultProcessing;

import java.util.ArrayList;
import java.util.List;

import entities.ModelFile;
import gui.GuiConsistencRules;
import gui.ResultMode;
import javafx.concurrent.Task;
import restClient.FileNames;
import restClient.ServerCommunication;

public class ProcessUpdateCheck extends Task<Void>{
	
	List<ModelFile> modelFiles;
	private List<String> rdfFileNamesOnServer = new ArrayList<>();
	ServerCommunication sendToServer;
	GuiConsistencRules gui;
	private List<ResultObject> results =null;
	
	public ProcessUpdateCheck(List<ModelFile> modelFiles,ServerCommunication sendToServer, GuiConsistencRules gui) {
		this.modelFiles = modelFiles;
		this.sendToServer = sendToServer;
		this.gui = gui;
	}
	
	@Override
	protected Void call() throws Exception {
		results = processCheckUpdate();
		return null;		
	}
	
	private List<ResultObject> processCheckUpdate() {

		List<ResultObject> results = null;
		try {       
			updateMessage("Reading files and uploading on server, please wait");
			
			ReadAndUploadModels readModelsFromList = new ReadAndUploadModels(sendToServer);
			readModelsFromList.readAndUploadModels(modelFiles,rdfFileNamesOnServer);
			
			FileNames fileNames = new FileNames();
			if(rdfFileNamesOnServer.size()>0) {
				String[] fileNamesOfRDF = new String[rdfFileNamesOnServer.size()];
				fileNames.setRdfModels(rdfFileNamesOnServer.toArray(fileNamesOfRDF));
			}
			updateMessage("Checking model updates");
			results =sendToServer.sendFileListAndGetResults(fileNames,ResultMode.UPDATE);
			updateMessage("Processing model updates results");
		}catch(Exception e) {
			e.printStackTrace();
		}

		return results;
	}
	public List<ResultObject> getResults() {
		return results;
	}
}
