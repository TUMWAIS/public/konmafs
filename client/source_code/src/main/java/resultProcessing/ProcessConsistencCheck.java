package resultProcessing;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import emf.EmfParser;
import entities.ModelFile;
import gui.GuiConsistencRules;
import gui.ResultMode;
import javafx.concurrent.Task;
import manuelModelToJson.CheckEqualValueForActivtyDiagramm;
import manuelModelToJson.ComparisonContainer;
import restClient.FileNames;
import restClient.ServerCommunication;

public class ProcessConsistencCheck extends Task<Void>{

	List<ModelFile> modelFiles;
	private List<String> rdfFileNamesOnServer = new ArrayList<>();
	ServerCommunication sendToServer;
	GuiConsistencRules gui;
	private List<ResultObject> results =null;
	
	public ProcessConsistencCheck(List<ModelFile> modelFiles,ServerCommunication sendToServer, GuiConsistencRules gui) {
		this.modelFiles = modelFiles;
		this.sendToServer = sendToServer;
		this.gui = gui;
	}
	
	@Override
	protected Void call() throws Exception {
		results = processCheckConsistency();
		return null;
		
	}
	
	
	
	private List<ResultObject> processCheckConsistency() {

		List<ResultObject> results = null;
		try {       
			updateMessage("Reading files and uploading on server, please wait");
			
			ReadAndUploadModels readModelsFromList = new ReadAndUploadModels(sendToServer);
			readModelsFromList.readAndUploadModels(modelFiles,rdfFileNamesOnServer);
			
			FileNames fileNames = new FileNames();
			if(rdfFileNamesOnServer.size()>0) {
				String[] fileNamesOfRDF = new String[rdfFileNamesOnServer.size()];
				fileNames.setRdfModels(rdfFileNamesOnServer.toArray(fileNamesOfRDF));
			}
			
			String jsonString = processJSONFiles(readModelsFromList.getClientModel());
			if(jsonString!=null) {
				fileNames.setJsonFiles(jsonString);
			}
			updateMessage("Checking consistency");
			results =sendToServer.sendFileListAndGetResults(fileNames,ResultMode.INCONSISTENCIES);
			updateMessage("Processing consistency results");
		}catch(Exception e) {
			e.printStackTrace();
		}

		return results;
	}

	private String processJSONFiles(Model model) {
		String fileName = null;
		List<ComparisonContainer> containers = new ArrayList<>();
		for(int i=0;i<modelFiles.size();i++) {
			
			if(modelFiles.get(i).toString().endsWith(".uml")) {
				updateMessage("Processing user consistence rule file");
				EmfParser emfParserDefine = new EmfParser();
				emfParserDefine.parseDocument(modelFiles.get(i).getFile());
				
				CheckEqualValueForActivtyDiagramm checkEqualModel = new CheckEqualValueForActivtyDiagramm(model,emfParserDefine.emfFile);
				
				ComparisonContainer comp = checkEqualModel.readAppliedValuesIntoJson();
				containers.add(comp);
			}
		}
		if(containers.isEmpty()) {
			return null;
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		ComparisonContainer[] containersAsArray = new ComparisonContainer[containers.size()];
		containers.toArray(containersAsArray);
		
		
		try {
			String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(containersAsArray);
			fileName =sendToServer.sendJSON(jsonString);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
	
		return fileName;
	}

	public List<ResultObject> getResults() {
		return results;
	}
}
