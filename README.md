# KonMaFS-Software

# Description
This repository contains the software source code of the research project: 

Konsistente Entwicklung von Materialflusssystemen durch eine modellbasierte Vorgehensweise (KonMaFS) 
(Consistent development of material flow systems through a model-based approach) 

For more information about the project, please visit: https://gepris.dfg.de/gepris/projekt/451550676

The KonMaFS software contains two modules: 
* The client module - java maven project (source code available) in folder client 
* The inconsistency  management module -  python project compiled as .exe file in folder icm_server

## models
  Models for testing each function of the KonMaFS software
  ### inconsistencyCheckExample- input models to check the function "check consistency"
      - conveyorCAD_io.FCStd: cad model of four conveyors modeled in FreeCAD tool
      - plcExport.txt: Excerpt of the PLCopenCML export of the control code created in TwinCAT
      - conveyorSimulation.cfg: simulation model of four conveyors modeled in JaamSim tool
  ### modelUpdateExample- input models to check the function "check update"
      - conveyorCAD.FCStd: cad model of four conveyors modeled in FreeCAT tool with inconsistencies
  ### generateSimulationExample - input models to check the function "generate simulation"
      - cad.FCStd: cad model of six conveyors and two switches modeled in FreeCAD tool
      - plcExport.txt: Excerpt of the PLCopenCML export of the control code created in TwinCAT
      - Painting_Process.bpmn: BPMN model of six conveyors and two switches modeled in Modelio tool 
  ### correctInconsistenciesExample
      - cad.FCStd: cad model of six conveyors and two switches modeled in FreeCAT tool
      - componentList.xlsx: component list of six conveyors and two switches created in Excel
      - Correct_bill_of_materials.uml uml model with correction mode modeled with Eclipse Papyrus
  ### generateViewExample
      - cad.FCStd: cad model of six conveyors and two switches modeled in FreeCAT tool
	    - generated.cfg: simulation model for JaamSim tool generated from Painting_Process.bpmn in generate Simulation function
  ### UML_profiles/composedViews
	  - views.profil uml profil files for Eclipse Papyrus
  ### UML_profiles/correctInconsistencies
      - CorrectInconsistence.profile uml profil files for Eclipse Papyrus
	    - Correct_bill_of_materials uml model files for Eclipse Papyrus
  ### UML_profiles/userModelBasedQuery
      - Vergleich.profile uml profil files for Eclipse Papyrus
	    - Sichten.di.profile uml profil files for Eclipse Papyrus
	    - AbgleichAnforderungen uml model files for Eclipse Papyrus

## Client Modul (including GUI)
  ### Description of Functions:
    - Function 1: Inconsistency check - Checking inconsistencies between input models
    - Function 2: Model change check - Checking the impact of model changes
    - Function 3: Inconsistency correction - Comparing components in the component list and in the CAD model, correcting incorrect number of components in the component list
    - Function 4: Generation of a simulation model based on information from CAD and BPMN models
    - Function 5: Display of all information of selected components collected from all input models.

## "icm_server"-Inconsistency Management Modul 
description of the files in each folder
  ### restServerKonMaFs.exe
    executable file of the inconsistency mananagement modul
  ### files
     folder to save the temporary RDF files recived from the client module
  ### inputs
    - "Model/model_param.csv - model parameters to be extraced from the model RDF Files
    - "Taxonomy/Foerdermittel.csv"- Taxonomy of the conveyors including the velocity limits created in Excel (example for considering domain knowledge)
  ### link_repos
    FOL rules in SWRL to generated links between model instances
  ### query_repos
    - "model_inconsistency/cad_equivalent_check.txt": Query if the z_positions of two adjacent conveyors are same
    - "model_inconsistency/motor_speed_check.txt": Query the inconsistency between plc velocity, length of the conveyor in CAD, and the transportation time in simulation model
    - "model_generation/get_simulation_traveltime.sparql": Query and calculate the transportation time of workpieces on each conveyor for generating simulation model
  ### rule_repos
    - "rule1.swrl": rule to generate links between model instances in Semantic Web Rule Language (SWRL)
  ### outputs
    - "onto.owl": knowledge base in Web Ontology Language (OWL) generated during checking the model inconsistencies
    - "onto-update.owl": knowledge base in Web Ontology Language (OWL) generated during checking the model updates 
  ### results 
    - "res_ic.json": inconsistency checking results to be sent to the client module in JSON format
    - "res_update.json": Model update checking results to be sent to the client module in JSON format

## Installation
The software needs a windows operating system and installed Java Runtime Environment with minumum version java 11.
To build the client modul from source please build the java project with maven. Start the client with executing the Main Class in the gui package.

To read the models please use following software for given filetyp:

Filtetyp: FCStd  Software: FreeCad
Filtetyp: xlsx Software: Microsoft Excel
Filtetyp: di, noation and uml Software: Eclipse Papyrus
Filteyp: cfg Software: JaamSim
Filtetyp: bpmn Software: Modelio 4 or text editor
For all other filetyps please use a text editor

## Getting started
- Double click the "restServerKonMaFs.exe" in the icm_server folder to start the inconsistency checking server
- Double click the "start_KonMaFs.bat" to start the client module
- Load the models in the corresponding folders via GUI of the client module to test each function

## License
- The client modul (all source files in the folder client) have the MIT License
- The server modul has the EULA license
- The models (all files in the folder models) have the license Creative Commons BY (CC-BY)


## contact 
* Technische Universität München
  * Fan Ji  - [fan.ji@tum.de](mailto:fan.ji@tum.de)

* Technische Universität München
  * Maximilian Wünnenberg - [max.wuennenberg@tum.de](mailto:max.wuennenberg@tum.de)

* Universität Duisburg-Essen
  * Rafael Schypula  - [rafael.schypula@uni-due.de](mailto:rafael.schypula@uni-due.de)

## Acknowledgment: 
This work was funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) – project number 451550676.
